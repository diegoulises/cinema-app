import axios from "axios";
import {
    TICKETS,
    LOADING,
    ERROR,
    SEATS,
    LOADING_SEATS,
    ERROR_SEATS,
    READ_SEATS,
    READ_LOADING_SEATS,
    READ_ERROR_SEATS,
    PRICES,
    LOADING_PRICES,
    ERROR_PRICES,
    DESCUENTOS,
    LOADING_DESCUENTOS,
    ERROR_DESCUENTOS
} from "../constants/ticketsTypes";
import { PRODUCTION } from "../constants/StringsAPI";

export const enviarTickets = (state) => async dispatch => {
    dispatch({
        type: LOADING
    });

    try {
        const data = {
            general: state.general,
            preferencial: state.preferencial,
            vip: state.vip,
            priority: state.priority,
            priorityPlus: state.priorityPlus
        };
        //DATABASE
        dispatch({
            type: TICKETS,
            payload: data
        });
    } catch (error) {
        //console.log(error.message);
        dispatch({
            type: ERROR,
            payload: "Ocurrio un error con tus boletos, reintenta de nuevo"
        });
    }
};

export const vaciarTickets = () => dispatch => {
    dispatch({
        type: TICKETS,
        payload: {}
    });
    dispatch({
        type: LOADING,
        payload: false
    });
    dispatch({
        type: ERROR,
        payload: ""
    });
};

export const reservarTemp = (seatNumber, idType, free, state, seats) => dispatch => {
    if (state.isPlus) {
        //console.log("IS PLUS");
        //console.log(state);
        dispatch({
            type: LOADING_SEATS
        });
        dispatch({
            type: SEATS,
            payload: state
        });
        try {
            if (free) {
                let formData = new FormData();
                let success = false
                formData.append('seatNumber', seatNumber)
                formData.append('idType', idType)
                formData.append('free', free)
                axios({
                    method: 'post',
                    url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
                    data: formData,
                    config: { headers: { 'Content-Type': 'multipart/form-data' } }
                })
                    .then(function (response) {
                        let newData = {
                            seatNumber: seatNumber,
                            idType: idType,
                            free: free,
                        };
                        let globalData = [];
                        if (seats.length === 0) {
                            globalData = seats
                        } else {
                            globalData = seats;
                        }
                        for (var index in globalData) {
                            const item = globalData[index]
                            if (item.seatNumber === seatNumber && item.idType === idType) {
                                globalData.splice(index, 1)
                                break
                            }
                        }
                        //console.log("REMOVE");
                        for (var k in globalData) {
                            //console.log(k, globalData[k]);
                        }
                        dispatch({
                            type: SEATS,
                            payload: globalData
                        });

                    })
                    .catch(function (response) {
                        //console.log('error')
                    });
            } else {
                let newData = {
                    seatNumber: seatNumber,
                    idType: idType,
                    free: free,
                    isPlus: true
                };
                let globalData = [];
                if (seats.length === 0) {

                } else {
                    globalData = seats;
                }

                globalData.push(newData)
                for (var k in globalData) {
                    //console.log(k, globalData[k]);
                }
                dispatch({
                    type: SEATS,
                    payload: globalData
                });
            }
        } catch (error) {
            //console.log(error.message);
            dispatch({
                type: ERROR_SEATS,
                payload: "No se pudieron enviar tus datos"
            });
            return state
        }
    }else{
        //console.log("IS NOT PLUS");
        //console.log(state);
        dispatch({
            type: LOADING_SEATS
        });
        dispatch({
            type: SEATS,
            payload: state
        });
        try {
            if (free) {
                let formData = new FormData();
                let success = false
                formData.append('seatNumber', seatNumber)
                formData.append('idType', idType)
                formData.append('free', free)
                axios({
                    method: 'post',
                    url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
                    data: formData,
                    config: { headers: { 'Content-Type': 'multipart/form-data' } }
                })
                    .then(function (response) {
                        let newData = {
                            seatNumber: seatNumber,
                            idType: idType,
                            free: free,
                            isPlus: false
                        };
                        let globalData = [];
                        if (seats.length === 0) {
                            globalData = seats
                        } else {
                            globalData = seats;
                        }
                        for (var index in globalData) {
                            const item = globalData[index]
                            if (item.seatNumber === seatNumber && item.idType === idType) {
                                globalData.splice(index, 1)
                                break
                            }
                        }
                        //console.log("REMOVE");
                        for (var k in globalData) {
                            //console.log(k, globalData[k]);
                        }
                        dispatch({
                            type: SEATS,
                            payload: globalData
                        });

                    })
                    .catch(function (response) {
                        //console.log('error')
                    });
            } else {
                let newData = {
                    seatNumber: seatNumber,
                    idType: idType,
                    free: free
                };
                let globalData = [];
                if (seats.length === 0) {

                } else {
                    globalData = seats;
                }

                globalData.push(newData)
                for (var k in globalData) {
                    //console.log(k, globalData[k]);
                }
                dispatch({
                    type: SEATS,
                    payload: globalData
                });
            }
        } catch (error) {
            //console.log(error.message);
            dispatch({
                type: ERROR_SEATS,
                payload: "No se pudieron enviar tus datos"
            });
            return state
        }
    }
}
var seleccionadosGeneral = []
var seleccionadosPreferente = []
var seleccionadosPriority = []
var seleccionadosVip = []
export const vaciarSeats = () => dispatch => {
    dispatch({
        type: SEATS,
        payload: []
    });
};

export const readSeats = () => async dispatch => {
    dispatch({
        type: READ_LOADING_SEATS
    });

    try {
        axios({
            method: 'get',
            url: `${PRODUCTION}/api/asientos/read.php`,
        })
            .then(function (response) {
                //handle success
                
                dispatch({
                    type: READ_SEATS,
                    payload: response.data
                });
            })
            .catch(function (response) {
                //handle error
                //console.log(response);
            });


    } catch (error) {
        //console.log(error.message);
        dispatch({
            type: READ_ERROR_SEATS,
            payload: "No se pusieron traer los datos"
        });
    }
};

export const getPrices = () => async dispatch => {
    dispatch({
        type: LOADING_PRICES
    });

    try {
        axios({
            method: 'get',
            url: `${PRODUCTION}/api/boletos/getPrices.php`,
        })
            .then(function (response) {
                dispatch({
                    type: PRICES,
                    payload: response.data
                });
                
            })
            .catch(function (response) {
                //handle error
            });
    } catch (error) {
        dispatch({
            type: ERROR_PRICES,
            payload: "Ah ocurrido algun error, recarga la página"
        });
    }
};

export const getDescuentos = () => async dispatch => {
    dispatch({
        type: LOADING_DESCUENTOS
    });

    try {
        axios({
            method: 'get',
            url: `${PRODUCTION}/api/descuentos/getDescuentos.php`,
        })
            .then(function (response) {
                
                dispatch({
                    type: DESCUENTOS,
                    payload: response.data
                });

            })
            .catch(function (response) {
                //handle error
            });
    } catch (error) {
        dispatch({
            type: ERROR_DESCUENTOS,
            payload: "Ah ocurrido algun error, recarga la página"
        });
    }
};