import { SELECT_MOVIE } from "../constants/ticketsTypes";
export const onSelectMovie = movie => ({
  type: SELECT_MOVIE,
  payload: movie
});
