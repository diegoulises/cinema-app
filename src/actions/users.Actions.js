import axios from "axios";
import {
  USER,
  LOADING,
  ERROR,
} from "../constants/usersTypes";
import { PRODUCTION } from "../constants/StringsAPI";

export const enviar = (state) => async dispatch => {
  dispatch({
    type: LOADING
  });

  try {
    let data = {
      name: state.name,
      middleName: state.middleName,
      lastName: state.lastName,
      email: state.email,
      phone: state.phone,
      calle: state.calle,
      localidad: state.localidad,
      estado: state.estado,
      cp: state.cp,
      colonia: state.colonia,
      id: state.id
    };
    //DATABASE (MySQL - PHP)

    let formData = new FormData();
    formData.append('name', state.name)
    formData.append('middleName', "")
    formData.append('lastName', state.lastName)
    formData.append('email', state.email)
    formData.append('mobile', state.phone)
    formData.append('address', "")
    formData.append('city', "")
    formData.append('state', "")
    formData.append('zipCode', 0)
    if(data.id!==""){
      formData.append('id', state.id)
    }

    // axios({
    //   method: 'post',
    //   url: `${PRODUCTION}/api/customers/insert.php`,
    //   data: formData,
    //   config: { headers: { 'Content-Type': 'multipart/form-data' } }
    // })
    //   .then(function (response) {
    //     //handle success
    //     data.id=response.data.id;
    //   })
    //   .catch(function (response) {
    //     //handle error
    //   });

    dispatch({
      type: USER,
      payload: data
    });
  } catch (error) {
    //console.log(error.message);
    dispatch({
      type: ERROR,
      payload: "No se pusieron enviar tus datos"
    });
  }
};

export const limpiar = () => dispatch => {
  dispatch({
    type: USER,
    payload: {}
  });
  dispatch({
    type: LOADING,
    payload: false
  });
  dispatch({
    type: ERROR,
    payload: ""
  });
};