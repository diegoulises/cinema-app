import axios from "axios";
import {
    LOGIN,
    CARGANDO,
    ERROR,
} from "../constants/sessionTypes";
import { PRODUCTION } from "../constants/StringsAPI";

export const login = (email, password) => async dispatch => {
    dispatch({
        type: CARGANDO
    });

    let formData = new FormData();
    formData.append('username', email)
    formData.append('password', password)

    axios({
        method: 'get',
        url: `${PRODUCTION}/api/usuarios/authenticate.php?username=${email}&password=${password}`,
        
    })
        .then(function (response) {
            //handle success
            if (response.data.success) {
                dispatch({
                    type: LOGIN,
                    payload: response.data
                });
                localStorage.setItem("login", true);
                localStorage.setItem("username", email);
            }
            dispatch({
                type: ERROR,
                payload: "Credenciales no válidas"
            });
        })
        .catch(function (response) {
            //handle error
            dispatch({
                type: ERROR,
                payload: "Credenciales no válidas"
            });
        });
};

export const logOut = () => dispatch => {
    localStorage.removeItem("login");
    localStorage.removeItem("username");
    var response = {
        success: false
    }
    dispatch({
        type: LOGIN,
        payload: response
    });
    dispatch({
        type: CARGANDO,
        payload: false
    });
    dispatch({
        type: ERROR,
        payload: ""
    });
};