//BOLETOS
export const TICKETS = "tickets_information";
export const LOADING = "tickets_loading";
export const ERROR = "tickets_error";
export const SEATS = "seats_information";
export const LOADING_SEATS = "seats_loading";
export const ERROR_SEATS = "seats_error";
export const READ_SEATS = "read_seats";
export const READ_LOADING_SEATS = "read_seats_loading";
export const READ_ERROR_SEATS = "read_seats_error";
export const SEATS_VIP = "seats_vip"
export const LOADING_PRICES = "prices_loading";
export const ERROR_PRICES = "prices_error";
export const PRICES = "prices";
export const LOADING_DESCUENTOS = "descuentos_loading";
export const ERROR_DESCUENTOS = "descuentos_error";
export const DESCUENTOS = "descuentos";
export const SELECT_MOVIE = "movies";