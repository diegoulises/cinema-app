import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import blueGrey from "@material-ui/core/colors/blueGrey";
import red from "@material-ui/core/colors/red";
import "./App.css";
import "./css/fonts.css"
import axios from 'axios'

import Page404 from "./components/404";
import UserForm from "./components/UserForm";
import Review from "./components/Review";
import Tickets from "./components/Tickets";
import Booking from "./components/Booking";
import Success from "./components/Success";
import Home from "./components/Movies";
import Movie from "./components/MovieBanner";
import Login from "./components/Login";
import LoginClient from "./components/Login/UserForm";
import LoginTickets from "./components/Login/Tickets";
import LoginBooking from "./components/Login/Booking";
import LoginReview from "./components/Login/Review";
import LoginSuccess from "./components/Login/Success";

import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import { useBeforeUnload, Beforeunload } from 'react-beforeunload'
import { useSelector } from "react-redux";
import { PRODUCTION } from "./constants/StringsAPI";

var seleccionadosGeneral = []
var seleccionadosPreferente = []
var seleccionadosPriority = []
var seleccionadosVip = []

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#01579b",
      main: "#01579b",
      dark: "#880e4f"
    },
    secondary: {
      light: "#2196f3",
      main: "#2196f3",
      dark: "#2196f3"
    },
    type: "light",
    background: {
      default: "#101010",
    }
  },
  typography: {
    fontFamily: [
      "Gotham-Light",
      "Gotham-Thin",
      "Gotham-Book",
      "Gotham-Medium",
      "Gotham-Ultra",
    ].join(','),
  },
});

function freeUp(event, seats) {
    //Agregar elementos al array de seleccionados para el pago con paypal
    for (var k in seats) {
      if (seats[k].idType===1) {
        seleccionadosGeneral.push(seats[k].seatNumber)
      }
      if (seats[k].idType===2) {
        seleccionadosPreferente.push(seats[k].seatNumber)
      }
      if (seats[k].idType===3) {
        seleccionadosVip.push(seats[k].seatNumber)
      }
      if (seats[k].idType===4) {
        seleccionadosPriority.push(seats[k].seatNumber)
      }
    }
  for (var key in seleccionadosGeneral) {
    window.onbeforeunload = (e) => {
      alert('Adios')
    };
    var formData = new FormData()
    formData.append('seatNumber', seleccionadosGeneral[key])
    formData.append('idType', 1)
    formData.append('free', true)
    axios({
      method: 'post',
      url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
      data: formData,
      config: { headers: { 'Content-Type': 'multipart/form-data' } }
    })
      .then(function (response) {
        if (response.data.success) {
          //console.log(`asiento ${seleccionadosGeneral[key]} liberado de general`)
        } else {
          //console.log(`asiento ${seleccionadosGeneral[key]} no se pudo liberar de general`)
        }
      })
      .catch(function (response) {
        //console.log('error')
      });
  }
  for (var key in seleccionadosPreferente) {
    window.onbeforeunload = (e) => {
      alert('Adios')
    };
    var formData = new FormData()
    formData.append('seatNumber', seleccionadosPreferente[key])
    formData.append('idType', 2)
    formData.append('free', true)
    axios({
      method: 'post',
      url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
      data: formData,
      config: { headers: { 'Content-Type': 'multipart/form-data' } }
    })
      .then(function (response) {
        if (response.data.success) {
          //console.log(`asiento ${seleccionadosPreferente[key]} liberado de preferente`)
        } else {
          //console.log(`asiento ${seleccionadosPreferente[key]} no se pudo liberar de preferente`)
        }
      })
      .catch(function (response) {
        //console.log('error')
      });
  }
  for (var key in seleccionadosVip) {
    window.onbeforeunload = (e) => {
      alert('Adios')
    };
    var formData = new FormData()
    formData.append('seatNumber', seleccionadosVip[key])
    formData.append('idType', 3)
    formData.append('free', true)
    axios({
      method: 'post',
      url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
      data: formData,
      config: { headers: { 'Content-Type': 'multipart/form-data' } }
    })
      .then(function (response) {
        if (response.data.success) {
          //console.log(`asiento ${seleccionadosVip[key]} liberado de vip`)
        } else {
          //console.log(`asiento ${seleccionadosVip[key]} no se pudo liberar de vip`)
        }
      })
      .catch(function (response) {
        //console.log('error')
      });
  }
  for (var key in seleccionadosPriority) {
    window.onbeforeunload = (e) => {
      alert('Adios')
    };
    var formData = new FormData()
    formData.append('seatNumber', seleccionadosPriority[key])
    formData.append('idType', 4)
    formData.append('free', true)
    axios({
      method: 'post',
      url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
      data: formData,
      config: { headers: { 'Content-Type': 'multipart/form-data' } }
    })
      .then(function (response) {
        if (response.data.success) {
          //console.log(`asiento ${seleccionadosPriority[key]} liberado de priority`)
        } else {
          //console.log(`asiento ${seleccionadosPriority[key]} no se pudo liberar de priority`)
        }
      })
      .catch(function (response) {
        //console.log('error')
      });
  }
}

export default function Router() {
  const ticketsSelector = useSelector(store => store.ticketsReducer);
  const {
    seats,
  } = ticketsSelector;
  localStorage.removeItem("stripe")
  return (
    <MuiThemeProvider theme={theme}>
      <BrowserRouter>
        <Beforeunload onBeforeunload={event => {
          const stripe = localStorage.getItem("stripe");
          if (!stripe) {
            event.preventDefault()
            freeUp(event, seats);
          }
          
        }}>
          <ReactNotification />
          <div className="App">
            <Switch className="wid">
              <Route exact path="/" component={Home} />
              <Route exact path="/movie" component={Movie} />
              <Route exact path="/tickets" component={Tickets} />
              <Route exact path="/client" component={UserForm} />
              <Route exact path="/booking" component={Booking} />
              <Route exact path="/review" component={Review} />
              <Route exact path="/success" component={Success} />
              {/* <Route exact path="/login" component={Login} />
              <Route exact path="/login/client" component={LoginClient} />
              <Route exact path="/login/tickets" component={LoginTickets} />
              <Route exact path="/login/booking" component={LoginBooking} />
              <Route exact path="/login/review" component={LoginReview} />
              <Route exact path="/login/success" component={LoginSuccess} /> */}
              <Route component={Page404} />
            </Switch>
          </div>
        </Beforeunload>

      </BrowserRouter>
    </MuiThemeProvider>
  )

}
