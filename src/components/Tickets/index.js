import React from "react";
import {
  IconButton,
  CssBaseline,
  Grid,
  Typography,
  AppBar,
  Toolbar,
  Stepper,
  Step,
  StepLabel,
  Paper,
  Button,
  makeStyles,
  Hidden
} from "@material-ui/core";

import useWindowSize from "../WindowSize";
import Add from "@material-ui/icons/AddRounded";
import Remove from "@material-ui/icons/RemoveRounded";
import Spinner from "../General/Spinner";
import Fatal from "../General/Fatal";
import Box from "./Box";
import { ReactComponent as SvgIcon } from "../../icons/ticket.svg";
import { useSelector, useDispatch } from "react-redux";
import * as ticketsActions from "../../actions/tickets.Actions";
import { useHistory } from "react-router-dom";
import { store } from "react-notifications-component";
import RN from "../Notification";
// import ReactPixel from "react-facebook-pixel";

// ReactPixel.init("496979157870426", {}, { debug: true, autoConfig: false });
// ReactPixel.pageView();
//ReactPixel.fbq("track", "PageView");

function Copyright({ classes }) {
  return (
    <Typography
      variant="body2"
      color="textSecondary"
      align="center"
      className={classes.copy}
    >
      {"Copyright © "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  general: {
    backgroundColor: "#757575",
    [theme.breakpoints.up("sm")]: {
      width: 150
    },
    [theme.breakpoints.down("xs")]: {
      width: 150
    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  preferencial: {
    backgroundColor: "#9e9e9e",
    [theme.breakpoints.up("sm")]: {
      width: 150
    },
    [theme.breakpoints.down("xs")]: {
      width: 150
    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  vip: {
    backgroundColor: "#424242",
    [theme.breakpoints.up("sm")]: {
      width: 150
    },
    [theme.breakpoints.down("xs")]: {
      width: 150
    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  priority: {
    backgroundColor: "#bdbdbd",
    [theme.breakpoints.up("sm")]: {
      width: 150
    },
    [theme.breakpoints.down("xs")]: {
      width: 150
    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  icon: {
    color: "#212121"
  },
  appBar: {
    position: "relative",
  },
  formControl: {
    width: "100%"
  },
  layout: {
    //width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      //width: 600,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3)
    }
  },
  stepper: {
    padding: theme.spacing(3, 0, 2)
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  buttonsDown: {
    display: "flex",
    justifyContent: "center",
    marginBottom: theme.spacing(0)
  },
  button: {
    fontFamily: "Gotham-Book",
    marginTop: theme.spacing(0),
    marginBottom: theme.spacing(2)
  },
  marginQr: {
    margin: theme.spacing(3)
  },
  copy: {
    color: "#fff",
    margin: theme.spacing(3)
  },
  layoutContent: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontFamily: "Gotham-Medium",
    fontSize: 15
  },
  number: {
    fontFamily: "Gotham-Medium"
  },
  numberBlack: {
    fontFamily: "Gotham-Medium",
    color: "#212121"
  },
  info: {
    fontFamily: "Gotham-Book"
  },
  price: {
    fontFamily: "Gotham-Book",
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    textDecoration: "underline"
  },
  ajl: {
    fontFamily: "Gotham-Ultra",
    marginLeft: theme.spacing(2)
  }
}));

const steps = [
  "Selecciona tus boletos",
  "Datos del cliente",
  "Reserva",
  "Revise su compra"
];

export default function Tickets() {
  const classes = useStyles();

  const usersSelector = useSelector(store => store.usersReducer);
  const ticketsSelector = useSelector(store => store.ticketsReducer);
  const dispatch = useDispatch();

  const history = useHistory();

  const size = useWindowSize();

  React.useEffect(() => {
    const { tickets } = ticketsSelector;
    if (Object.keys(tickets).length !== 0) {
      setValues({
        ...values,
        priority: tickets.priority,
        vip: tickets.vip,
        preferencial: tickets.preferencial,
        general: tickets.general
      });
    }
  }, []);

  const [values, setValues] = React.useState({
    activeStep: 0,
    general: 0,
    preferencial: 0,
    vip: 0,
    priority: 0
  });
  const handleNext = () => {
    dispatch(ticketsActions.enviarTickets(values));
    dispatch(ticketsActions.getPrices());
    if (
      values.general === 0 &&
      values.vip === 0 &&
      values.preferencial === 0 &&
      values.priority === 0
    ) {
      store.addNotification({
        content: (
          <RN
            title="Error"
            message="Selecciona tu o tus boletos según la zona de tu prefencia para continuar."
            type="Error"
          />
        ),
        insert: "top",
        container: "top-right",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 1000,
          onScreen: false
        }
      });
    }
    dispatch(ticketsActions.vaciarSeats());
    history.push("/client");
  };
  const handleBack = () => {
    history.push("/");
  };
  const sizeEscenario = () => {
    if (size.height > size.width) {
      return "auto";
    }
    return "80%";
  };
  const activeStepClass = () => {
    return sizeEscenario();
  };
  const cargarEscenario = () => {
    const { user, loading, error } = usersSelector;
    if (loading) {
      return <Spinner />;
    }
    if (error) {
      return <Fatal mensaje={error} />;
    }
    return (
      <Grid container justify="center">
        <Grid item xs={12} sm={10} container justify="center">
          <Grid item xs={12}>
            <Box
              enableColor={values.general === 0}
              color="#880e4f"
              largo={8}
              text="Pantalla."
            />
          </Grid>
          {/**ESCENARIO */}
          <Grid item xs={12} sm={8} container justify="center">
            <Grid item xs={2} container justify="center">
              <Grid item xs={5}>
                <Box
                  enableColor={values.general === 0}
                  color="#757575"
                  largo={100}
                />
              </Grid>
              <Grid item xs={7}>
                <Box
                  enableColor={values.preferencial === 0}
                  color="#9e9e9e"
                  largo={100}
                />
              </Grid>
            </Grid>

            <Grid item xs={8} container>
              <Grid item xs={12} container>
                <Grid item xs={2}>
                  <Box
                    enableColor={values.vip === 0}
                    color="#424242"
                    largo={70}
                  />
                </Grid>

                <Grid item xs={8} container justify="center">
                  <Grid item xs={12} container justify="center">
                    <Grid item xs={4}>
                      <Box
                        enableColor={values.priority === 0}
                        color="#bdbdbd"
                        largo={50}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Box
                        enableColor={values.general === 0}
                        color="#880e4f"
                        largo={50}
                        text="Escaleras."
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Box
                        enableColor={values.priority === 0}
                        color="#bdbdbd"
                        largo={50}
                      />
                    </Grid>
                  </Grid>
                  <Grid item xs={12}>
                    <Box
                      enableColor={values.priority === 0}
                      color="#bdbdbd"
                      largo={20}
                    />
                  </Grid>
                </Grid>

                <Grid item xs={2}>
                  <Box
                    enableColor={values.vip === 0}
                    color="#424242"
                    largo={70}
                  />
                </Grid>
              </Grid>

              <Grid item xs={12} container justify="center">
                <Grid item xs={6}>
                  <Box
                    enableColor={values.vip === 0}
                    color="#424242"
                    largo={30}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Box
                    enableColor={values.vip === 0}
                    color="#424242"
                    largo={30}
                  />
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={2} container justify="center">
              <Grid item xs={7}>
                <Box
                  enableColor={values.preferencial === 0}
                  color="#9e9e9e"
                  largo={100}
                />
              </Grid>
              <Grid item xs={5}>
                <Box
                  enableColor={values.general === 0}
                  color="#757575"
                  largo={100}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Typography variant="h6" gutterBottom>
            Selecciona tus boletos
          </Typography>
          {/** BOLETOS */}

          <Grid container spacing={3} justify="center">
            <Grid item xs={12} sm={3}>
              <Grid item container justify="center" alignItems="center">
                <Grid item xs={12}>
                  <Typography align="center" className={classes.text}>
                    PRIORITY
                  </Typography>
                </Grid>
                <Grid item>
                  <IconButton
                    size="small"
                    className={classes.icon}
                    aria-label="Eliminar 1 boleto"
                    onClick={() =>
                      setValues({
                        ...values,
                        priority: values.priority - 1
                      })
                    }
                    disabled={values.priority === 0}
                  >
                    <Remove />
                  </IconButton>
                </Grid>
                <Grid item>
                  <Paper elevation={3} className={classes.priority}>
                    <Grid item xs={12}>
                      <Typography
                        align="center"
                        variant="body2"
                        className={classes.numberBlack}
                      >
                        {values.priority}
                      </Typography>
                    </Grid>
                  </Paper>
                </Grid>
                <Grid item>
                  <IconButton
                    size="small"
                    className={classes.icon}
                    aria-label="Añadir 1 boleto"
                    onClick={() => {
                      setValues({
                        ...values,
                        priority: values.priority + 1
                      });
                      // ReactPixel.track("AddToCart", {
                      //   type: "Priority",
                      //   amount: values.priority + 1
                      // });
                    }}
                  >
                    <Add />
                  </IconButton>
                </Grid>
                <Grid item xs={12}>
                  <Typography
                    align="center"
                    variant="body1"
                    className={classes.price}
                  >
                    $180.0 MXN
                  </Typography>
                  <Typography align="center" variant="body2">
                    Boleto Zona 1, acceso prioritario, barra libre, premier, comida & snacks.
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Grid item container justify="center" alignItems="center">
                <Grid item xs={12}>
                  <Typography align="center" className={classes.text}>
                    VIP
                  </Typography>
                </Grid>
                <Grid item>
                  <IconButton
                    size="small"
                    className={classes.icon}
                    aria-label="Eliminar 1 boleto"
                    onClick={() =>
                      setValues({ ...values, vip: values.vip - 1 })
                    }
                    disabled={values.vip === 0}
                  >
                    <Remove />
                  </IconButton>
                </Grid>
                <Grid item>
                  <Paper elevation={3} className={classes.vip}>
                    <Grid item xs={12}>
                      <Typography
                        align="center"
                        variant="body2"
                        className={classes.number}
                      >
                        {values.vip}
                      </Typography>
                    </Grid>
                  </Paper>
                </Grid>
                <Grid item>
                  <IconButton
                    size="small"
                    className={classes.icon}
                    aria-label="Añadir 1 boleto"
                    onClick={() => {
                      setValues({ ...values, vip: values.vip + 1 });

                      // ReactPixel.track("AddToCart", {
                      //   type: "VIP",
                      //   amount: values.vip + 1
                      // });
                    }}
                  >
                    <Add />
                  </IconButton>
                </Grid>
                <Grid item xs={12}>
                  <Typography
                    align="center"
                    variant="body1"
                    className={classes.price}
                  >
                    $120.0 MXN
                  </Typography>
                  <Typography align="center" variant="body2">
                    Boleto Zona 2, acceso prioritario, comida & snacks.
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Grid item container justify="center" alignItems="center">
                <Grid item xs={12}>
                  <Typography align="center" className={classes.text}>
                    PREFERENCIAL
                  </Typography>
                </Grid>
                <Grid item>
                  <IconButton
                    size="small"
                    className={classes.icon}
                    aria-label="Eliminar 1 boleto"
                    onClick={() =>
                      setValues({
                        ...values,
                        preferencial: values.preferencial - 1
                      })
                    }
                    disabled={values.preferencial === 0}
                  >
                    <Remove />
                  </IconButton>
                </Grid>
                <Grid item>
                  <Paper elevation={3} className={classes.preferencial}>
                    <Grid>
                      <Typography
                        align="center"
                        variant="body2"
                        className={classes.number}
                      >
                        {values.preferencial}
                      </Typography>
                    </Grid>
                  </Paper>
                </Grid>
                <Grid item>
                  <IconButton
                    size="small"
                    className={classes.icon}
                    aria-label="Añadir 1 boleto"
                    onClick={() => {
                      setValues({
                        ...values,
                        preferencial: values.preferencial + 1
                      });

                      // ReactPixel.track("AddToCart", {
                      //   type: "Preferencial",
                      //   amount: values.preferencial + 1
                      // });
                    }}
                  >
                    <Add />
                  </IconButton>
                </Grid>
                <Grid item xs={12}>
                  <Typography
                    align="center"
                    variant="body1"
                    className={classes.price}
                  >
                    $80.0 MXN
                  </Typography>
                  <Typography align="center" variant="body2">
                    Boleto Zona 3 + Comida.
                  </Typography>
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={12} sm={3}>
              <Grid item xs={12} container justify="center" alignItems="center">
                <Grid item xs={12}>
                  <Typography align="center" className={classes.text}>
                    GENERAL
                  </Typography>
                </Grid>
                <Grid item>
                  <IconButton
                    size="small"
                    className={classes.icon}
                    aria-label="Eliminar 1 boleto"
                    onClick={() =>
                      setValues({
                        ...values,
                        general: values.general - 1
                      })
                    }
                    disabled={values.general === 0}
                  >
                    <Remove />
                  </IconButton>
                </Grid>
                <Grid item>
                  <Paper elevation={3} className={classes.general}>
                    <Typography
                      align="center"
                      variant="body2"
                      className={classes.number}
                    >
                      {values.general}
                    </Typography>
                  </Paper>
                </Grid>
                <Grid item>
                  <IconButton
                    size="small"
                    className={classes.icon}
                    aria-label="Añadir 1 boleto"
                    onClick={() => {
                      setValues({
                        ...values,
                        general: values.general + 1
                      });

                      // ReactPixel.track("AddToCart", {
                      //   type: "General",
                      //   amount: values.general + 1
                      // });
                    }}
                  >
                    <Add />
                  </IconButton>
                </Grid>
                <Grid item xs={12}>
                  <Typography
                    align="center"
                    variant="body1"
                    className={classes.price}
                  >
                    $50 MXN
                  </Typography>
                  <Typography align="center" variant="body2">
                    Boleto Zona 4.
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  };
  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="absolute" color="default" className={classes.appBar}>
        <Toolbar>
          <SvgIcon style={{ fill: "#d50000", width: 48, height: 48 }} />
          <Typography
            variant="h6"
            color="inherit"
            noWrap
            className={classes.ajl}
          >
            CINEMA APP
          </Typography>
        </Toolbar>
      </AppBar>
      <main className={classes.layout} style={{ maxWidth: activeStepClass() }}>
        <Grid
          container
          align="center"
          justify="center"
          direction="column"
          className={classes.layoutContent}
        >
          <Paper className={classes.paper}>
            <Typography component="h1" variant="h4" align="center"></Typography>
            <Stepper
              activeStep={values.activeStep}
              className={classes.stepper}
              alternativeLabel
            >
              {steps.map(label => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
            <React.Fragment>
            <Hidden xsDown>
                    <div className={classes.buttons}>
                      <Button onClick={handleBack} className={classes.button}>
                        REGRESAR
                      </Button>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={handleNext}
                        className={classes.button}
                      >
                        SIGUIENTE
                      </Button>
                    </div>
                  </Hidden>
                  <Hidden smUp>
                    <div className={classes.buttonsDown}>
                      <Button onClick={handleBack} className={classes.button}>
                        REGRESAR
                      </Button>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={handleNext}
                        className={classes.button}
                      >
                        SIGUIENTE
                      </Button>
                    </div>
                  </Hidden>
                

              {cargarEscenario()}
            </React.Fragment>
          </Paper>
        </Grid>
        <Copyright classes={classes} />
      </main>
    </React.Fragment>
  );
}
