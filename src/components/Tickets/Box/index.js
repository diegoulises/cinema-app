import React from "react";
import posed from "react-pose";
import { makeStyles, Typography, Grid } from "@material-ui/core";
import useWindowSize from "../../WindowSize";

const useStyles = makeStyles(theme => ({
  general: {
    backgroundColor: "#757575",
    width: 150,
    height: 150,
    padding: theme.spacing(3, 2),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  preferencial: {
    backgroundColor: "#9e9e9e",
    width: 150,
    height: 150,
    padding: theme.spacing(3, 2),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  vip: {
    backgroundColor: "#545454",
    width: 150,
    height: 150,
    padding: theme.spacing(3, 2),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  priority: {
    backgroundColor: "#bdbdbd",
    width: 150,
    height: 150,
    padding: theme.spacing(3, 2),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  box: {
    background: "#880e4f",
    width: "100%"
  },
  text: {
    color: "#fff",
    fontFamily: "Gotham-Book",
    [theme.breakpoints.up("lg")]: {
      fontSize: 16
    },
    [theme.breakpoints.up("md")]: {
      fontSize: 10
    },
    [theme.breakpoints.up("sm")]: {
      fontSize: 8
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: 6
    }
  },
  layout: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
}));

const Box = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 1
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});

const BoxGeneral = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 1,
    background: "#010101"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#757575"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});
const BoxGeneralEnable = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 1,
    background: "#757575"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#757575"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});

const BoxPreferencial = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 1,
    background: "#010101"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#9e9e9e"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});
const BoxPreferencialEnable = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 1,
    background: "#9e9e9e"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#9e9e9e"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});

const BoxVip = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 1,
    background: "#010101"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#424242"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});
const BoxVipEnable = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 1,
    background: "#424242"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#424242"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});

const BoxPriority = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 1,
    background: "#010101"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#bdbdbd"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});
const BoxPriorityEnable = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 1,
    background: "#bdbdbd"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#bdbdbd"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});

const BoxP = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    opacity: 1
  },
  hover: {
    scale: 0.9,
    opacity: 1
  },
  press: {
    scale: 0.9
  }
});

export default function Zona({ color, largo, enableColor, text }) {
  const classes = useStyles();
  const size = useWindowSize();

  const sizeEscenario = () => {
    if (size.height > size.width) {
      return (size.height / 300) * largo;
    }
    return (size.height / 300) * largo;
  };
  if (color === "#880e4f") {
    return (
      <BoxP
        className={classes.box}
        style={{ background: color, height: sizeEscenario() }}
      >
        <Grid
          container
          align="center"
          justify="center"
          direction="column"
          className={classes.layout}
        >
          <Typography className={classes.text} align="center">
            {text}
          </Typography>
        </Grid>
      </BoxP>
    );
  }

  if (color === "#757575") {
    if (!enableColor) {
      return (
        <BoxGeneralEnable
          className={classes.box}
          style={{ background: color, height: sizeEscenario() }}
        />
      );
    }
    return (
      <BoxGeneral
        className={classes.box}
        style={{ background: color, height: sizeEscenario() }}
      />
    );
  }

  if (color === "#9e9e9e") {
    if (!enableColor) {
      return (
        <BoxPreferencialEnable
          className={classes.box}
          style={{ background: color, height: sizeEscenario() }}
        />
      );
    }
    return (
      <BoxPreferencial
        className={classes.box}
        style={{ background: color, height: sizeEscenario() }}
      />
    );
  }

  if (color === "#424242") {
    if (!enableColor) {
      return (
        <BoxVipEnable
          className={classes.box}
          style={{ background: color, height: sizeEscenario() }}
        />
      );
    }
    return (
      <BoxVip
        className={classes.box}
        style={{ background: color, height: sizeEscenario() }}
      />
    );
  }

  if (color === "#bdbdbd") {
    if (!enableColor) {
      return (
        <BoxPriorityEnable
          className={classes.box}
          style={{ background: color, height: sizeEscenario() }}
        />
      );
    }
    return (
      <BoxPriority
        className={classes.box}
        style={{ background: color, height: sizeEscenario() }}
      />
    );
  }

  return (
    <Box
      className={classes.box}
      style={{
        background: enableColor ? "#000000" : color,
        height: sizeEscenario()
      }}
    />
  );
}
