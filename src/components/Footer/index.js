import React from "react";
import {
    makeStyles, Typography, Grid,
} from "@material-ui/core";
import blueGrey from "@material-ui/core/colors/blueGrey";

const useStyles = makeStyles(theme => ({
    footerLogos: {
        height: 18,
        margin: theme.spacing(1),
    },
    footerLogoOpenpay: {
        height: 48,
        margin: theme.spacing(1)
    },
    footer: {
        backgroundColor: "#f9f9f9", //#212121
        overflowX: 'hidden',
        overflowY: 'hidden',
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1)
    },
    fiscal: {
        fontFamily: "Gotham-Medium",
        color: "#212121"
    },
    typography: {
        color: "#212121"
    }
}));

function Footer() {
    const classes = useStyles();
    return (
        <footer className={classes.footer}>
            <Grid container direction="row" justify="space-evenly" alignItems="center" spacing={1}>
                <Grid item>
                    <Typography className={classes.fiscal} variant="caption" display="block">
                        Términos y Condiciones
                    </Typography>          
                </Grid>
                <Grid item>
                    <img className={classes.footerLogoOpenpay} src="/assets/images/stripe.png" alt="Stripe" />
                </Grid>
                <Grid item>
                    <Typography className={classes.typography} variant="caption" display="block" className={classes.fiscal}>Direccción fiscal</Typography>
                    <Typography className={classes.typography} variant="caption" display="block">
                        Blvd. García De León 1685, C.P. 58260, Col. Chapultepec Oriente,
                        Morelia Michoacán
                    </Typography>
                    <Typography className={classes.typography} variant="caption" display="block">
                    </Typography>
                </Grid>
                <Grid item>
                    <Typography className={classes.fiscal} variant="caption" display="block">
                        Tarjetas de crédito
                    </Typography>
                    <img className={classes.footerLogos} src="/assets/images/visa.png" alt="visa" />
                    <img className={classes.footerLogos} src="/assets/images/masterCard.png" alt="masterCard" />
                    <img className={classes.footerLogos} src="/assets/images/americanExpress.png" alt="americanExpress" />
                    <img className={classes.footerLogos} src="/assets/images/carnet.png" alt="carnet" />
                    <Typography className={classes.fiscal} variant="caption" display="block">
                        Tarjetas de débito
                    </Typography>
                    <img className={classes.footerLogos} src="/assets/images/tarjetasDebito/citibanamex.png" alt="citibanamex" />
                    <img className={classes.footerLogos} src="/assets/images/tarjetasDebito/santander.png" alt="santander" />
                    <img className={classes.footerLogos} src="/assets/images/tarjetasDebito/hsbc.png" alt="hsbc" />
                    <img className={classes.footerLogos} src="/assets/images/tarjetasDebito/scotiabank.png" alt="scotiabank" />
                    <img className={classes.footerLogos} src="/assets/images/tarjetasDebito/inbursa.png" alt="inbursa" />
                    <img className={classes.footerLogos} src="/assets/images/tarjetasDebito/ixe.png" alt="ixe" />

                    <Typography className={classes.typography} variant="caption" display="block">
                    </Typography>
                    <img className={classes.footerLogos} src="/assets/images/tarjetasDebito/bbva.png" alt="bbva" />
                </Grid>
                <Grid item>
                    <Typography className={classes.fiscal} variant="caption" display="block">
                        Teléfono
                    </Typography>
                    <Typography className={classes.typography} variant="caption" display="block">
                        +52 (443) 335-1458
                    </Typography>
                </Grid>
                <Grid item>
                    <Typography className={classes.fiscal} variant="caption" display="block">
                        Correo de soporte
                    </Typography>
                    <Typography className={classes.typography} variant="caption" display="block">soporte@ajlboletos.com</Typography>
                </Grid>
            </Grid>
        </footer>
    );
}

export default Footer;