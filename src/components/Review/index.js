import React, { useRef } from "react";
import {
  CssBaseline,
  Grid,
  Typography,
  AppBar,
  Toolbar,
  Stepper,
  Step,
  StepLabel,
  Paper,
  Button,
  makeStyles,
  ListItemText,
  List,
  ListItem,
  TextField,
  Checkbox,
  Hidden,
  FormControlLabel,
} from "@material-ui/core";
import useWindowSize from "../WindowSize";
import { Image } from 'react'
import PaypalBtn from "../PaypalBtn";
import Spinner from "../General/Spinner";
import Fatal from "../General/Fatal";
import { ReactComponent as SvgIcon } from "../../icons/ticket.svg";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import * as ticketsActions from "../../actions/tickets.Actions";
import axios from 'axios'
import { CountdownCircleTimer } from 'react-countdown-circle-timer';
import { PRODUCTION } from "../../constants/StringsAPI";
import Footer from "../Footer";
//import ReactPixel from 'react-facebook-pixel';
import { PK } from "../../constants/StringsAPI"

//ReactPixel.init('496979157870426', {}, { debug: true, autoConfig: false });
//ReactPixel.pageView();
//ReactPixel.fbq('track', 'PageView');

var seleccionadosGeneral = []
var seleccionadosPreferente = []
var seleccionadosPriority = []
var seleccionadosVip = []

function Copyright({ classes }) {
  return (
    <Typography
      variant="body2"
      color="textSecondary"
      align="center"
      className={classes.copy}
    >
      {"Copyright © "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const renderTime = value => {
  if (value === 0) {
    return <div style={{ fontSize: 12, color: '#101010', fontFamily: "Gotham-Book" }}>Asientos liberados</div>;
  }
  if (value % 60 < 10) {
    return (
      <div className="timer">
        <div style={{ fontSize: 22, color: '#101010', fontFamily: "Gotham-Ultra" }}>{Math.trunc(value / 60)}:0{value % 60}</div>
        <div style={{ fontSize: 10, color: '#101010', fontFamily: "Gotham-Book" }}>tiempo<br />restante</div>
      </div>
    );
  }
  return (
    <div className="timer">
      <div style={{ fontSize: 22, color: '#101010', fontFamily: "Gotham-Ultra" }}>{Math.trunc(value / 60)}:{value % 60}</div>
      <div style={{ fontSize: 10, color: '#101010', fontFamily: "Gotham-Book" }}>tiempo<br />restante</div>
    </div>
  );

};

const useStyles = makeStyles(theme => ({
  image: {
    width: 100
  },
  icon: {
    color: "#FFFFFF"
  },
  text: {
    fontSize: 15
  },
  appBar: {
    position: "relative"
  },
  formControl: {
    width: "100%"
  },
  layout: {
    //width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      //width: 600,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3)
    }
  },
  stepper: {
    padding: theme.spacing(3, 0, 5)
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  button: {
    fontFamily: "Gotham-Book",
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  },
  buttonOpenPay: {
    backgroundColor: 'white',
    width: 250,
    borderRadius: 100,
    color: 'black',
    '&:hover': {
      background: "#dddddd",
    },
  },
  logoOpenpay: {
    height: 24
  },
  logoStripe: {
    height: 20,
  },
  textOpenpay: {
    fontSize: 10
  },
  marginQr: {
    margin: theme.spacing(3)
  },
  copy: {
    color: "#fff",
    margin: theme.spacing(3)
  },
  layoutContent: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  listItem: {
    padding: theme.spacing(1, 0)
  },
  total: {
    fontWeight: "700"
  },
  title: {
    marginTop: theme.spacing(2)
  },
  add: {
    fontFamily: "Gotham-Book"
  },
  dataReview: {
    fontFamily: "Gotham-Book",
    textDecoration: "underline"
  },
  dataReview2: {
    fontFamily: "Gotham-Book",
  },
  ajl: {
    fontFamily: "Gotham-Ultra",
    marginLeft: theme.spacing(2)
  },
}));

const steps = [
  "Selecciona tus boletos",
  "Datos del cliente",
  "Reserva",
  "Revise su compra"
];

const addresses = [
  "Negrito Poeta #84",
  "col. Santa María de Guido",
  "58090",
  "Morelia",
  "México"
];
const checks = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
const init = ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""];

export default function Review() {
  const classes = useStyles();
  const history = useHistory();
  const size = useWindowSize();
  const ticketsSelector = useSelector(store => store.ticketsReducer);
  const userSelector = useSelector(store => store.usersReducer);
  const dispatch = useDispatch();
  const stripe = window.Stripe(PK);

  const [state, setState] = React.useState({
    checked: [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
    name: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
    email: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
    phone: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
    amountSeats: 0,
    invoice: false,
  });
  var openPayWindow = null
  // const freeUp = (seats) => {
  //   //Agregar elementos al array de seleccionados para el pago con paypal
  //   for (var k in seats) {
  //     if (seats[k].idType === 1) {
  //       seleccionadosGeneral.push(seats[k].seatNumber)
  //     }
  //     if (seats[k].idType === 2) {
  //       seleccionadosPreferente.push(seats[k].seatNumber)
  //     }
  //     if (seats[k].idType === 3) {
  //       seleccionadosVip.push(seats[k].seatNumber)
  //     }
  //     if (seats[k].idType === 4) {
  //       seleccionadosPriority.push(seats[k].seatNumber)
  //     }
  //   }
  //   for (var key in seleccionadosGeneral) {
  //     window.onbeforeunload = (e) => {
  //       alert('Adios')
  //     };
  //     var formData = new FormData()
  //     formData.append('seatNumber', seleccionadosGeneral[key])
  //     formData.append('idType', 1)
  //     formData.append('free', true)
  //     axios({
  //       method: 'post',
  //       url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
  //       data: formData,
  //       config: { headers: { 'Content-Type': 'multipart/form-data' } }
  //     })
  //       .then(function (response) {
  //         if (response.data.success) {
  //           //console.log(`asiento ${seleccionadosGeneral[key]} liberado de general`)
  //         } else {
  //           //console.log(`asiento ${seleccionadosGeneral[key]} no se pudo liberar de general`)
  //         }
  //       })
  //       .catch(function (response) {
  //         //console.log('error')
  //       });
  //   }
  //   for (var key in seleccionadosPreferente) {
  //     window.onbeforeunload = (e) => {
  //       alert('Adios')
  //     };
  //     var formData = new FormData()
  //     formData.append('seatNumber', seleccionadosPreferente[key])
  //     formData.append('idType', 2)
  //     formData.append('free', true)
  //     axios({
  //       method: 'post',
  //       url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
  //       data: formData,
  //       config: { headers: { 'Content-Type': 'multipart/form-data' } }
  //     })
  //       .then(function (response) {
  //         if (response.data.success) {
  //           //console.log(`asiento ${seleccionadosPreferente[key]} liberado de preferente`)
  //         } else {
  //           //console.log(`asiento ${seleccionadosPreferente[key]} no se pudo liberar de preferente`)
  //         }
  //       })
  //       .catch(function (response) {
  //         //console.log('error')
  //       });
  //   }
  //   for (var key in seleccionadosVip) {
  //     window.onbeforeunload = (e) => {
  //       alert('Adios')
  //     };
  //     var formData = new FormData()
  //     formData.append('seatNumber', seleccionadosVip[key])
  //     formData.append('idType', 3)
  //     formData.append('free', true)
  //     axios({
  //       method: 'post',
  //       url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
  //       data: formData,
  //       config: { headers: { 'Content-Type': 'multipart/form-data' } }
  //     })
  //       .then(function (response) {
  //         if (response.data.success) {
  //           //console.log(`asiento ${seleccionadosVip[key]} liberado de vip`)
  //         } else {
  //           //console.log(`asiento ${seleccionadosVip[key]} no se pudo liberar de vip`)
  //         }
  //       })
  //       .catch(function (response) {
  //         //console.log('error')
  //       });
  //   }
  //   for (var key in seleccionadosPriority) {
  //     window.onbeforeunload = (e) => {
  //       alert('Adios')
  //     };
  //     var formData = new FormData()
  //     formData.append('seatNumber', seleccionadosPriority[key])
  //     formData.append('idType', 4)
  //     formData.append('free', true)
  //     axios({
  //       method: 'post',
  //       url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
  //       data: formData,
  //       config: { headers: { 'Content-Type': 'multipart/form-data' } }
  //     })
  //       .then(function (response) {
  //         if (response.data.success) {
  //           //console.log(`asiento ${seleccionadosPriority[key]} liberado de priority`)
  //         } else {
  //           //console.log(`asiento ${seleccionadosPriority[key]} no se pudo liberar de priority`)
  //         }
  //       })
  //       .catch(function (response) {
  //         //console.log('error')
  //       });
  //   }
  // }
  const handleChangeName = index => event => {
    const { name } = state;
    const newState = name.slice(0); // Create a shallow copy of the name
    newState[index] = event.target.value; // Set the new value
    setState({ ...state, "name": newState });
  };
  const handleChangeEmail = index => event => {
    const { email } = state;
    const newState = email.slice(0); // Create a shallow copy of the name
    newState[index] = event.target.value; // Set the new value
    setState({ ...state, "email": newState });
  };
  const handleChangePhone = index => event => {
    const { phone } = state;
    const newState = phone.slice(0); // Create a shallow copy of the name
    newState[index] = event.target.value; // Set the new value
    setState({ ...state, "phone": newState });
  };
  const handleChangeC = index => event => {
    const { user } = userSelector;
    const { checked } = state;
    const newState = checked.slice(0); // Create a shallow copy of the name
    newState.map((item, j) => {
      if (j === index) {
        newState[j] = event.target.checked; // Set the new value
        if (event.target.checked) {

          let helloName = Object.assign({}, state);
          helloName.name[j] = user.name;
          let helloEmail = Object.assign({}, state);
          helloEmail.email[j] = user.email;
          let helloPhone = Object.assign({}, state);
          helloPhone.phone[j] = user.phone;

          setState({ ...state, "name": helloName });
          setState({ ...state, "email": helloEmail });
          setState({ ...state, "phone": helloPhone });

        }
      } else {
        newState[j] = !event.target.checked;
        let helloName = Object.assign({}, state);
        helloName.name[j] = "";
        let helloEmail = Object.assign({}, state);
        helloEmail.email[j] = "";
        let helloPhone = Object.assign({}, state);
        helloPhone.phone[j] = "";

        setState({ ...state, "name": helloName });
        setState({ ...state, "email": helloEmail });
        setState({ ...state, "phone": helloPhone });
      }
    });
    setState({ ...state, "checked": newState });
    
  };

  const handleBack = () => {
    dispatch(ticketsActions.vaciarSeats());
    const {
      tickets,
      loading,
      error,
      seats,
      loading_seats,
      error_seats
    } = ticketsSelector;
    //freeUp(seats)
    history.push("/booking");
  };
  const handleOpenPay = (amount) => {
    //window.open('http://www.facebook.com/sharer.php?s=100&p[title]=Fb Share&p[summary]=Facebook share popup&p[url]=javascript:fbShare("http://jsfiddle.net/stichoza/EYxTJ/")&p[images][0]="http://goo.gl/dS52U"', 'sharer', 'toolbar=0,status=0,width=548,height=325');
    const { user } = userSelector;
    const name = user.name
    const last_name = user.last_name
    const phone_number = user.phone
    const email = user.email
    var formData = new FormData()
    formData.append('name', name)
    formData.append('last_name', last_name)
    formData.append('phone_number', phone_number)
    formData.append('email', email)
    formData.append('amount', amount)
    axios({
      method: 'post',
      url: `${PRODUCTION}/api/openpay/createCharge.php`,
      data: formData,
      config: { headers: { 'Content-Type': 'multipart/form-data' } }
    })
      .then(function (response) {
        if (response.data.success) {
          const url = response.data.redirect_url
          var win = window.open(url, 'sharer', 'toolbar=0,status=0,width=800,height=600');
          openPayWindow = win
        } else {
          alert('error')
        }
      })
      .catch(function (response) {
      });
  }
  const handleStripe = (cargo, cargoMasIva, invoice) => {
    //window.open('http://www.facebook.com/sharer.php?s=100&p[title]=Fb Share&p[summary]=Facebook share popup&p[url]=javascript:fbShare("http://jsfiddle.net/stichoza/EYxTJ/")&p[images][0]="http://goo.gl/dS52U"', 'sharer', 'toolbar=0,status=0,width=548,height=325');
    const { user } = userSelector;
    const { seats } = ticketsSelector;
    const name = user.name
    const last_name = user.last_name
    const phone_number = user.phone
    const email = user.email
    console.log("handleStripe");

    var formData = new FormData()
    formData.append('name', name)
    formData.append('last_name', last_name)
    formData.append('phone_number', phone_number)
    formData.append('email', email)
    if (invoice) {
      formData.append('amount', cargoMasIva)
      formData.append('requiresInvoice', true)
    } else {
      formData.append('amount', cargo)
    }

    axios({
      method: 'post',
      url: `${PRODUCTION}/api/stripe/createCharge.php`,
      data: formData,
      config: { headers: { 'Content-Type': 'multipart/form-data' } }
    })
      .then(function (response) {
        const id = response.data.id
        console.log(response);
        if (response.data.success) {
          /*const url = response.data.redirect_url
          var win = window.open(url, 'sharer', 'toolbar=0,status=0,width=800,height=600');
          openPayWindow = win*/

          // Crear bookings en pendiente
          let seleccionadosGeneral = []
          let seleccionadosPreferente = []
          let seleccionadosVip = []
          let seleccionadosPriority = []
          for (var k in seats) {
            if (seats[k].idType === 1) {
              seleccionadosGeneral.push(seats[k].seatNumber)
            }
            if (seats[k].idType === 2) {
              seleccionadosPreferente.push(seats[k].seatNumber)
            }
            if (seats[k].idType === 3) {
              seleccionadosVip.push(seats[k].seatNumber)
            }
            if (seats[k].idType === 4) {
              seleccionadosPriority.push(seats[k].seatNumber)
            }
          }
          var indexCustomUser = 0;
          for (var index in seleccionadosPriority) {

            let seatNumber = seleccionadosPriority[index]
            let seatType = 4
            let customName = state.name[indexCustomUser] ? state.name[indexCustomUser] : user.name
            let customEmail = state.email[indexCustomUser] ? state.email[indexCustomUser] : user.email
            let customPhone = state.phone[indexCustomUser] ? state.phone[indexCustomUser] : user.phone
            var formData = new FormData()
            formData.append('idTransaction', id)
            formData.append('seatNumber', seatNumber)
            formData.append('seatType', seatType)
            formData.append('ownerName', customName)
            formData.append('ownerEmail', customEmail)
            formData.append('ownerMobile', customPhone)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/booking/insertPending.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {

                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            indexCustomUser = indexCustomUser + 1
          }

          for (var index in seleccionadosVip) {

            let seatNumber = seleccionadosVip[index]
            let seatType = 3
            let customName = state.name[indexCustomUser] ? state.name[indexCustomUser] : user.name
            let customEmail = state.email[indexCustomUser] ? state.email[indexCustomUser] : user.email
            let customPhone = state.phone[indexCustomUser] ? state.phone[indexCustomUser] : user.phone
            formData = new FormData()
            formData.append('idTransaction', id)
            formData.append('seatNumber', seatNumber)
            formData.append('seatType', seatType)
            formData.append('ownerName', customName)
            formData.append('ownerEmail', customEmail)
            formData.append('ownerMobile', customPhone)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/booking/insertPending.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {

                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            indexCustomUser = indexCustomUser + 1
          }

          for (var index in seleccionadosPreferente) {

            let seatNumber = seleccionadosPreferente[index]
            let seatType = 2
            let customName = state.name[indexCustomUser] ? state.name[indexCustomUser] : user.name
            let customEmail = state.email[indexCustomUser] ? state.email[indexCustomUser] : user.email
            let customPhone = state.phone[indexCustomUser] ? state.phone[indexCustomUser] : user.phone
            formData = new FormData()
            formData.append('idTransaction', id)
            formData.append('seatNumber', seatNumber)
            formData.append('seatType', seatType)
            formData.append('ownerName', customName)
            formData.append('ownerEmail', customEmail)
            formData.append('ownerMobile', customPhone)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/booking/insertPending.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {

                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            indexCustomUser = indexCustomUser + 1
          }

          for (var index in seleccionadosGeneral) {

            let seatNumber = seleccionadosGeneral[index]
            let seatType = 1
            let customName = state.name[indexCustomUser] ? state.name[indexCustomUser] : user.name
            let customEmail = state.email[indexCustomUser] ? state.email[indexCustomUser] : user.email
            let customPhone = state.phone[indexCustomUser] ? state.phone[indexCustomUser] : user.phone
            formData = new FormData()
            formData.append('idTransaction', id)
            formData.append('seatNumber', seatNumber)
            formData.append('seatType', seatType)
            formData.append('ownerName', customName)
            formData.append('ownerEmail', customEmail)
            formData.append('ownerMobile', customPhone)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/booking/insertPending.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {

                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            indexCustomUser = indexCustomUser + 1
          }
          console.log('bookings created')

          localStorage.setItem("stripe", true)

          // Redirecting to payment form page
          stripe.redirectToCheckout({
            sessionId: response.data.session_id
          }).then(function (result) {
            alert('error stripe')
          });

        } else {
          alert('error axios')
        }
      })
      .catch(function (response) {
      });
  }
  const sizeEscenario = () => {
    if (size.height > size.width) {
      return "auto";
    }
    return 800;
  };

  const activeStepClass = () => {
    return sizeEscenario();
  };

  const getInfoUser = () => {
    const { user, loading, error } = userSelector;
    if (loading) {
      return <Spinner />;
    }
    if (error) {
      return <Fatal mensaje={error} />;
    }
    return (
      <Grid item xs={12}>
        <Typography variant="caption" gutterBottom>
          Hola <Typography variant="caption" display="inline" className={classes.dataReview}>{user.name}</Typography>!, verifica que tu email <Typography variant="caption" display="inline" className={classes.dataReview}>{user.email}</Typography> sea correcto, recuerda que tus entradas serán enviadas a ésta dirección.
        </Typography>
      </Grid>
    );
  };

  const handleCancel = (seats) => {
    //freeUp(seats)
    if (openPayWindow != null) {
      openPayWindow.close()
    }
    history.push("/");
    alert('Se acabó tu tiempo. Hemos cancelado tu compra y liberado tus asientos')
  }

  const handleChange = name => event => {
    setState({ ...state, [name]: event.target.checked });
    if (event.target.checked) {
      // ReactPixel.track("AddPaymentInfo", {
      //   requireInvoice: event.target.checked,
      // });
    }
  };

  const getSummary = () => {

    const { user } = userSelector;
    const {
      tickets,
      loading,
      error,
      seats,
      loading_seats,
      error_seats,
    } = ticketsSelector;


    //Agregar elementos al array de seleccionados para el pago con paypal
    for (let k in seats) {
      if (seats[k].idType === 1) {
        seleccionadosGeneral.push(seats[k].seatNumber)
      }
      if (seats[k].idType === 2) {
        seleccionadosPreferente.push(seats[k].seatNumber)
      }
      if (seats[k].idType === 3) {
        seleccionadosVip.push(seats[k].seatNumber)
      }
      if (seats[k].idType === 4) {
        seleccionadosPriority.push(seats[k].seatNumber)
      }
    }

    if (loading || loading_seats) {
      return <Spinner />;
    }
    if (error || error_seats) {
      return <Fatal mensaje={error} />;
    }
    if (error_seats) {
      return <Fatal mensaje={error_seats} />;
    }
    if ((tickets.general === 0 && tickets.vip === 0 && tickets.preferencial === 0 && tickets.priority === 0) || Object.keys(tickets).length === 0) {
      history.push("/")
    }

    var totalTickets = tickets.general + tickets.vip + tickets.preferencial + tickets.priority;
    //let p = prices.prices;

    var priceGeneralAPI;
    var priceVipAPI;
    var pricePriorityAPI;
    var pricePreferencialAPI;
    var commissionGeneralAPI;
    var commissionVipAPI;
    var commissionPriorityAPI;
    var commissionPreferencialAPI;

    //for (let i in p) {
      // if (p[i].name === "General") {
      //   priceGeneralAPI = 50;
      //   commissionGeneralAPI = 0.03;
      // }
      // if (p[i].name === "VIP") {
      //   priceVipAPI = 120;
      //   commissionVipAPI = 0.03;
      // }
      // if (p[i].name === "Priority") {
      //   pricePriorityAPI = 180;
      //   commissionPriorityAPI = 0.03;
      // }
      // if (p[i].name === "Preferencial") {
      //   pricePreferencialAPI = 80;
      //   commissionPreferencialAPI = 0.03;
      // }
    //}
    priceGeneralAPI = 50;
    commissionGeneralAPI = 0.035;
    priceVipAPI = 120;
    commissionVipAPI = 0.035;
    pricePriorityAPI = 180;
    commissionPriorityAPI = 0.08;
    pricePreferencialAPI = 80;
    commissionPreferencialAPI = 0.035;

    let ticketsData = [];
    let priceGeneral = 0;
    let priceVip = 0;
    let pricePriority = 0;
    let pricePreferencial = 0;
    let sum = 0;
    if (tickets.priority > 0) {
      pricePriority = tickets.priority * pricePriorityAPI;
      let seatsPriority = "";
      for (let k in seats) {
        if (seats[k].idType === 4) {
          seatsPriority += seats[k].seatNumber + " ";
        }
      }
      ticketsData.push({
        amount: tickets.priority,
        name: "Priority",
        desc:
          "Boleto Zona 1, acceso prioritario, barra libre, premier, comida & snacks.",
        price: `$${pricePriority} MXN`,
        seats: seatsPriority,
        commission: commissionPriorityAPI
      });
    }
    if (tickets.vip > 0) {
      priceVip = tickets.vip * priceVipAPI;
      let seatsVip = "";
      for (let k in seats) {
        if (seats[k].idType === 3) {
          seatsVip += seats[k].seatNumber + " ";
        }
      }
      ticketsData.push({
        amount: tickets.vip,
        name: "VIP",
        desc:
          "Boleto Zona 2, acceso prioritario, comida & snacks.",
        price: `$${priceVip} MXN`,
        seats: seatsVip,
        commission: commissionVipAPI
      });
    }
    if (tickets.preferencial > 0) {
      pricePreferencial = tickets.preferencial * pricePreferencialAPI;
      let seatsPreferencial = "";
      for (let k in seats) {
        if (seats[k].idType === 2) {
          seatsPreferencial += seats[k].seatNumber + " ";
        }
      }
      ticketsData.push({
        amount: tickets.preferencial,
        name: "Preferencial",
        desc: "Boleto Zona 3 + Comida.",
        price: `$${pricePreferencial} MXN`,
        seats: seatsPreferencial,
        commission: commissionPreferencialAPI
      });
    }
    if (tickets.general > 0) {
      priceGeneral = tickets.general * priceGeneralAPI;
      let seatsGeneral = "";
      for (let k in seats) {
        if (seats[k].idType === 1) {
          seatsGeneral += seats[k].seatNumber + " ";
        }
      }
      ticketsData.push({
        amount: tickets.general,
        name: "General",
        desc: "Boleto Zona 4",
        price: `$${priceGeneral} MXN`,
        seats: seatsGeneral,
        commission: commissionGeneralAPI
      });
    }

    sum = priceGeneral + pricePreferencial + pricePriority + priceVip;
    const comision = priceGeneral * commissionGeneralAPI +
      pricePreferencial * commissionPreferencialAPI +
      pricePriority * commissionVipAPI +
      priceVip * commissionPriorityAPI;
    const sumCom =
      comision +
      priceGeneral +
      pricePreferencial +
      pricePriority +
      priceVip;
    var iva = (sumCom * 0.16).toFixed(2);
    const cargo = sumCom.toFixed(2);
    const cargoMasIva = (sumCom * 0.16 + sumCom).toFixed(2);
    return (
      <div>
        <ListItem className={classes.listItem}>
          <FormControlLabel
            control={
              <Checkbox
                checked={state.invoice}
                onChange={handleChange("invoice")}
                value="invoice"
                color="primary"
              />
            }
            label="Requiere factura"
          />
        </ListItem>
        <List disablePadding>
          {ticketsData.map((product, indexProduct) => (
            <div key={indexProduct}>
              <ListItem className={classes.listItem} key={product.name}>
                <ListItemText
                  primary={
                    <Typography
                      variant="body1"
                      style={{ fontFamily: "Gotham-Book" }}
                    >{`${product.name} (x${product.amount})`}</Typography>
                  }
                  secondary={
                    <Typography
                      variant="body2"
                      style={{ fontFamily: "Gotham-Book" }}
                    >
                      {product.desc}
                      <br />
                      Cargo por comisión de boleto{" "}
                      {(product.commission * 100).toFixed(1)}%
                    </Typography>
                  }
                />
                <Typography
                  variant="body2"
                  align="right"
                  style={{ fontFamily: "Gotham-Book" }}
                >
                  {product.price}
                </Typography>
              </ListItem>
              <Grid container>
                {seats.map((seat, index) => boletos(seat, product.name, index))}
              </Grid>
            </div>
          ))}
          <ListItem className={classes.listItem}>
            <ListItemText primary="Subtotal" />
            <Typography variant="subtitle1" className={classes.total}>
              ${sum.toFixed(2)}
            </Typography>
          </ListItem>
          <ListItem className={classes.listItem}>
            <ListItemText primary="Comisión" />
            <Typography variant="subtitle1" className={classes.total}>
              ${comision.toFixed(2)}
            </Typography>
          </ListItem>
          <ListItem className={classes.listItem}>
            <ListItemText primary="IVA" />
            <Typography variant="subtitle1" className={classes.total}>
              ${state.invoice ? iva : 0}
            </Typography>
          </ListItem>
          <ListItem className={classes.listItem}>
            <ListItemText primary="Total" />
            <Typography variant="subtitle1" className={classes.total}>
              ${state.invoice ? cargoMasIva : cargo}
            </Typography>
          </ListItem>
        </List>
        <Grid container spacing={2} alignContent="center">
          <Grid item xs={12}>
            <Typography
              style={{
                width: "100%",
                marginTop: 0,
                marginBottom: 0,
                fontSize: 12,
                fontFamily: "Gotham-Ultra"
              }}
            >
              NOTA:
            </Typography>
            <Typography
              style={{
                width: "100%",
                marginTop: 10,
                marginBottom: 20,
                fontSize: 12
              }}
            >
              Tu compra será cancelada y tus asientos serán liberados al
              acabarse el tiempo
            </Typography>
          </Grid>
          <CountdownCircleTimer
            isPlaying
            size={100}
            renderTime={renderTime}
            strokeWidth={5}
            trailColor="#212121"
            ariaLabel={"2"}
            durationSeconds={600}
            onComplete={() => handleCancel(seats)}
            colors={[["#388e3c", 0.33], ["#FBDF23", 0.33], ["#880000"]]}
          />
          <Grid item xs={12}>
            <Typography variant="h6" gutterBottom className={classes.title}>
              Dirección del evento
            </Typography>
            <Typography variant="body2">Morelia, Michoacán</Typography>
            <Typography gutterBottom variant="body2">
              {addresses.join(", ")}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <PaypalBtn
              state={state}
              seats={seats}
              amount={cargo}
              amountIva={cargoMasIva}
              user={user}
              invoice={state.invoice}
              asientosGeneral={seleccionadosGeneral}
              asientosPreferencial={seleccionadosPreferente}
              asientosVip={seleccionadosVip}
              asientosPriority={seleccionadosPriority}
            />
          </Grid>
        </Grid>
      </div>
    );
  };

  const boletos = (seat, name, index) => {
    //GENERAL
    if (seat.idType === 1) {
      if (name === "General") {
        return (
          <Grid container item spacing={2} alignItems="center" justify="center" key={index}>
            <Grid item xs={1}>
              <Checkbox
                checked={state.checked[index]}
                onChange={handleChangeC(index)}
                value="secondary"
                color="primary"
                inputProps={{ 'aria-label': 'secondary checkbox' }}
              />
            </Grid>
            <Grid item xs={2}>
              <Typography>
                {seat.seatNumber}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="name"
                label="Nombre"
                value={state.name[index]}
                key={index}
                onChange={handleChangeName(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="email"
                label="Email"
                value={state.email[index]}
                key={index}
                onChange={handleChangeEmail(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="phone"
                label="Telefono"
                value={state.phone[index]}
                key={index}
                onChange={handleChangePhone(index)}
                fullWidth
              />
            </Grid>
          </Grid>
        );
      }
    }
    //Preferencial
    if (seat.idType === 2) {
      if (name === "Preferencial") {
        return (
          <Grid container item spacing={2} alignItems="center" justify="center" key={index}>
            <Grid item xs={1}>
              <Checkbox
                checked={state.checked[index]}
                onChange={handleChangeC(index)}
                value="secondary"
                color="primary"
                inputProps={{ 'aria-label': 'secondary checkbox' }}
              />
            </Grid>
            <Grid item xs={2}>
              <Typography>
                {seat.seatNumber}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="name"
                label="Nombre"
                value={state.name[index]}
                key={index}
                onChange={handleChangeName(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="email"
                label="Email"
                value={state.email[index]}
                key={index}
                onChange={handleChangeEmail(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="phone"
                label="Telefono"
                value={state.phone[index]}
                key={index}
                onChange={handleChangePhone(index)}
                fullWidth
              />
            </Grid>
          </Grid>
        );
      }
    }
    //Vip
    if (seat.idType === 3) {
      if (name === "VIP") {
        return (
          <Grid container item spacing={2} alignItems="center" justify="center" key={index}>
            <Grid item xs={1}>
              <Checkbox
                checked={state.checked[index]}
                onChange={handleChangeC(index)}
                value="secondary"
                color="primary"
                inputProps={{ 'aria-label': 'secondary checkbox' }}
              />
            </Grid>
            <Grid item xs={2}>
              <Typography>
                {seat.seatNumber}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="name"
                label="Nombre"
                value={state.name[index]}
                key={index}
                onChange={handleChangeName(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="email"
                label="Email"
                value={state.email[index]}
                key={index}
                onChange={handleChangeEmail(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="phone"
                label="Telefono"
                value={state.phone[index]}
                key={index}
                onChange={handleChangePhone(index)}
                fullWidth
              />
            </Grid>
          </Grid>
        );
      }
    }
    //Priority
    if (seat.idType === 4) {
      if (name === "Priority") {
        return (
          <Grid container item spacing={2} alignItems="center" justify="center" key={index}>
            <Grid item xs={1}>
              <Checkbox
                checked={state.checked[index]}
                onChange={handleChangeC(index)}
                value="secondary"
                color="primary"
                inputProps={{ 'aria-label': 'secondary checkbox' }}
              />
            </Grid>
            <Grid item xs={2}>
              <Typography>
                {seat.seatNumber}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="name"
                label="Nombre"
                value={state.name[index]}
                key={index}
                onChange={handleChangeName(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="email"
                label="Email"
                value={state.email[index]}
                key={index}
                onChange={handleChangeEmail(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="phone"
                label="Telefono"
                value={state.phone[index]}
                key={index}
                onChange={handleChangePhone(index)}
                fullWidth
              />
            </Grid>
          </Grid>
        );
      }
    }
  }

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="absolute" color="default" className={classes.appBar}>
        <Toolbar>
          <SvgIcon style={{ fill: "#d50000", width: 48, height: 48 }} />
          <Typography variant="h6" color="inherit" noWrap className={classes.ajl}>
            CINEMA APP
          </Typography>
        </Toolbar>
      </AppBar>
      <Grid
        container
        alignItems="center"
        justify="space-evenly"
        direction="row"
      >
        <Grid item>
          <Hidden mdDown>
            <img className={classes.image} src="/palomitas-de-maiz.png" alt="CINEMA" />
          </Hidden>
        </Grid>
        <Grid item>
          <main className={classes.layout} style={{ maxWidth: activeStepClass() }}>
            <Grid
              container
              align="center"
              justify="center"
              direction="column"
              className={classes.layoutContent}
            >
              <Paper className={classes.paper}>
                <Typography component="h1" variant="h4" align="center"></Typography>
                <Stepper
                  activeStep={4}
                  className={classes.stepper}
                  alternativeLabel
                >
                  {steps.map(label => (
                    <Step key={label}>
                      <StepLabel>{label}</StepLabel>
                    </Step>
                  ))}
                </Stepper>
                <React.Fragment>
                  <Typography variant="h6" gutterBottom>
                    Resumen de compra
              </Typography>
                  {getInfoUser()}
                  {getSummary()}

                  <div className={classes.buttons}>
                    <Button onClick={handleBack} className={classes.button}>
                      REGRESAR
                </Button>
                  </div>
                </React.Fragment>
              </Paper>
            </Grid>
            <Copyright classes={classes} />
          </main>

        </Grid>

        <Grid item>
          <Hidden mdDown>
            <img className={classes.image} src="/boleto.png" alt="APP" />
          </Hidden>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
