import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { Grid } from "@material-ui/core";
import red from "@material-ui/core/colors/red";
import { ReactComponent as SvgIcon } from "../../icons/ticket.svg";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center" style={{color: "#fff"}}>
      {"CINEMA APP, Copyright © "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  layout: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  titulo: {
    color: "#fff",
    marginTop: theme.spacing(10),
    marginBottom: theme.spacing(10),
    [theme.breakpoints.down("xs")]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
    }
  },
  link: {
    textDecoration: "none",
    color: "#880e4f"
  }
}));

export default function Home() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <Grid
        container
        align="center"
        justify="center"
        direction="column"
        className={classes.layout}
      >
        <Grid item>
          <SvgIcon style={{ fill: "#d50000", height: 144 }} />
          <Typography variant="h5" gutterBottom style={{color: "#fff"}}>CINEMA APP</Typography>
        </Grid>
        <Typography variant="h3" gutterBottom className={classes.titulo}>
          Página no encontrada,{" "}
          <Link to="/" className={classes.link}>
            volver a inicio
          </Link>
          .
        </Typography>
        <Copyright />
      </Grid>
    </React.Fragment>
  );
}
