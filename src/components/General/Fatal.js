import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Alert from "@material-ui/lab/Alert";
import IconButton from "@material-ui/core/IconButton";
import Collapse from "@material-ui/core/Collapse";
import CloseIcon from "@material-ui/icons/Close";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  text: {
    fontFamily: "GothamNarrow-Black"
  }
}));

const Fatal = props => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  return (
    <div className={classes.root}>
      <Collapse in={open}>
        <Alert
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpen(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          severity="error"
        >
          <Typography className={classes.text}>{props.mensaje}</Typography>
        </Alert>
      </Collapse>
    </div>
  );
};

export default Fatal;
