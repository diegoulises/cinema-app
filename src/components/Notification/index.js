import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Avatar from "@material-ui/core/Avatar";
import ErrorRoundedIcon from "@material-ui/icons/ErrorRounded";
import SuccessRoundedIcon from '@material-ui/icons/CheckCircleRounded';
import WarningRoundedIcon from '@material-ui/icons/WarningRounded';
import { Typography, Box } from "@material-ui/core";
const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    backgroundColor: "#e0e0e0"
  },
  rootSuccess: {
    width: "100%",
    backgroundColor: "#e0e0e0"
  }, 
  rootWarning: {
    width: "100%",
    backgroundColor: "#e0e0e0",
    
    color: "#212121",
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: "transparent",
    color: "#212121"
  },
  conor: {
    color: "#212121",
    fontFamily: "Gotham-Ultra",
  },
  raven: {
    color: "#212121",
    fontFamily: "Gotham-Medium",
    fontSize: 12
  }
}));

export default function RecipeReviewCard({title, message, type}) {
  const classes = useStyles();
  if (type === "Success") {
    return(
      <Card className={classes.rootSuccess}>
        <Box borderLeft={10} borderColor="#212121">
          <CardHeader
            avatar={
              <Avatar aria-label="recipe" className={classes.avatar}>
                <SuccessRoundedIcon fontSize="large" />
              </Avatar>
            }
            title={
              <Typography variation="h1" className={classes.conor}>
                ¡{title}!
            </Typography>
            }
            subheader={
              <Typography variation="caption" className={classes.raven}>
                {message}
              </Typography>
            }
          />
        </Box>
      </Card>
    );
  }
  if (type === "Warning") {
    return (
      <Card className={classes.rootWarning}>
        <Box borderLeft={10} borderColor="#212121">
          <CardHeader
            avatar={
              <Avatar aria-label="recipe" className={classes.avatar}>
                <WarningRoundedIcon fontSize="large" />
              </Avatar>
            }
            title={
              <Typography variation="h1" className={classes.conor}>
                ¡{title}!
            </Typography>
            }
            subheader={
              <Typography variation="caption" className={classes.raven}>
                {message}
              </Typography>
            }
          />
        </Box>
      </Card>
    );
  }
  return (
    <Card className={classes.root}>
      <Box borderLeft={10} borderColor="#212121">
        <CardHeader
          avatar={
            <Avatar aria-label="recipe" className={classes.avatar}>
              <ErrorRoundedIcon fontSize="large" />
            </Avatar>
          }
          title={
            <Typography variation="h1" className={classes.conor}>
              ¡{title}!
            </Typography>
          }
          subheader={
            <Typography variation="caption" className={classes.raven}>
              {message}
            </Typography>
          }
        />
      </Box>
    </Card>
  );
}
