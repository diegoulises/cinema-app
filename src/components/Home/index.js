import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import AddressForm from "../UserForm";
import Booking from "../Booking";
import Review from "../Review";
import QRCode from "qrcode.react";
import { AppBar, Toolbar, Grid } from "@material-ui/core";
import useWindowSize from "../WindowSize";
import Spinner from "../General/Spinner";
import Fatal from "../General/Fatal";

import PayPalBtn from "../PaypalBtn";

import { useSelector, useDispatch } from "react-redux";
import * as usersActions from "../../actions/users.Actions";

function Copyright({ classes }) {
  return (
    <Typography
      variant="body2"
      color="textSecondary"
      align="center"
      className={classes.copy}
    >
      {"Copyright © "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  appBar: {
    position: "relative"
  },
  layout: {
    //width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      //width: 600,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3)
    }
  },
  stepper: {
    padding: theme.spacing(3, 0, 5)
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  },
  marginQr: {
    margin: theme.spacing(3)
  },
  copy: {
    margin: theme.spacing(3)
  },
  layoutContent: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
}));

const steps = [
  "Datos del cliente",
  "Selecciona tus boletos",
  "Revise su compra"
];

function getStepContent(step) {
  switch (step) {
    case 0:
      return <AddressForm />;
    case 1:
      return <Booking />;
    case 2:
      return <Review />;
    default:
      throw new Error("Unknown step");
  }
}

export default function Home() {
  const classes = useStyles();

  const user = useSelector(store => store.usersReducer);
  const dispatch = useDispatch();

  const [activeStep, setActiveStep] = React.useState(0);

  const handleNext = () => {
    const { response, loading, error } = user;

    if (activeStep === 0) {
      dispatch(usersActions.enviar("Ulises", "Martínez"))
    }
    if (loading) {
      return <Spinner/>
    }
    if (error) {
      return <Fatal mensaje={error} />;
    }
    if (response.length !== 0){
    setActiveStep(activeStep + 1);
    }
  };

  const handleBack = () => {
    if (activeStep === 1) {
      dispatch(usersActions.limpiar());
    }
    setActiveStep(activeStep - 1);
  };

  const size = useWindowSize();

  const sizeEscenario = () => {
    if (size.height > size.width) {
      return "auto";
    }
    return "75%";
  };

  const activeStepClass = () => {
    if (activeStep !== 1) {
      return 600;
    } else {
      return sizeEscenario();
    }
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="absolute" color="default" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" color="inherit" noWrap>
            AJL Boletos
          </Typography>
        </Toolbar>
      </AppBar>
      <main className={classes.layout} style={{ maxWidth: activeStepClass() }}>
        <Grid
          container
          align="center"
          justify="center"
          direction="column"
          className={classes.layoutContent}
        >
          <Paper className={classes.paper}>
            <Typography component="h1" variant="h4" align="center"></Typography>
            <Stepper activeStep={activeStep} className={classes.stepper}>
              {steps.map(label => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
            <React.Fragment>
              
              {activeStep === steps.length ? (
                <React.Fragment>
                  <Typography variant="h5" gutterBottom>
                    Su pago fue registrado
                  </Typography>
                  <QRCode
                    value="https://digitalignition.com.mx/es/home/"
                    className={classes.marginQr}
                  />
                  <Typography variant="subtitle1">
                    Hemos enviado su confirmación de compra y su entrada (código
                    QR) por correo electrónico.
                  </Typography>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  {getStepContent(activeStep)}
                  <div className={classes.buttons}>
                    {activeStep !== 0 && (
                      <Button onClick={handleBack} className={classes.button}>
                        Regresar
                      </Button>
                    )}

                    {activeStep === steps.length - 1 ? (
                      <PayPalBtn amount="12.03"/>
                    ) : (
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={handleNext}
                        className={classes.button}
                      >
                        SIGUIENTE
                      </Button>
                    )}
                  </div>
                </React.Fragment>
              )}
            </React.Fragment>
          </Paper>
        </Grid>
        <Copyright classes={classes} />
      </main>
    </React.Fragment>
  );
}
