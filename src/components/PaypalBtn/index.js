import React from "react";
import ReactDOM from "react-dom";
import paypal from "paypal-checkout";
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { PRODUCTION } from "../../constants/StringsAPI";
import ReactPixel from 'react-facebook-pixel';
import "./styles.css";
import { ENV } from "../../constants/StringsAPI";
class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      env: ENV,
      client: {
        sandbox: "AazX79pQdh7Sx_QXZjoETTRp3IGoWa43AUuOEronHIG6I5Y00MNzmY_7fQIQogvUBjNj5YtSbdfavK_a",
        production: "Af_gi5V-MhQ1GpbRFffmq5tk3kscqxwrshbVyldU6lNeypGL1QlXqO6rx_55tgKt89Nvpom57SdOL6om"
      },
      commit: true,
      open: false,
      amount: this.props.amount,
      state: this.props.state,
      withInvoivceR: this.props.state.invoice,
      amountIva: this.props.amountIva,
    };
  }

  payment(data, actions) {
    /*ReactPixel.track("InitiateCheckout", {
      method: "PayPal",
    });*/

    const self = this.props.state
    
    if (self.invoice) {
      return actions.payment.create({
        transactions: [
          {
            amount: { total: this.state.amountIva, currency: "MXN" }
          }
        ]
      });
    } else {
      return actions.payment.create({
        transactions: [
          {
            amount: { total: this.state.amount, currency: "MXN" }
          }
        ]
      });
    }
    
  }

  getLenght(type) {
    let size = 0
    for (var index in this.props.seats) {
      const item = this.props.seats[index]
      if (item.idType == type) {
        size = size + 1
      }
    }
    return size
  }

  onAuthorize(data, actions) {
    const general = this.props.asientosGeneral
    const generalSize = this.getLenght(1)
    const preferencial = this.props.asientosPreferencial
    const preferencialSize = this.getLenght(2)
    const vip = this.props.asientosVip
    const vipSize = this.getLenght(3)
    const priority = this.props.asientosPriority
    const prioritySize = this.getLenght(4)
    const amount = this.props.amount
    const amountIva = this.props.amountIva;
    const user = this.props.user
    const email = user.email
    const { history } = this.props;
    const self = this.props.state
    //console.log(self)
    //console.log(general)
    return actions.payment.execute().then(function (paymentData) {
      // // Show a success page to the buyer
      const id = paymentData.id
      const state = paymentData.state
      const withInvoice = self.invoice
      if (state === "approved") {
        var formData = new FormData()
        formData.append('idTransaction', id)
        
        formData.append('email', email)
        if (withInvoice) {
          formData.append('requiresInvoice', true)
          formData.append('paymentAmount', amountIva)
        }else{
          formData.append('paymentAmount', amount)
        }
        axios({
          method: 'post',
          url: `${PRODUCTION}/api/transactions/insert.php`,
          data: formData,
          config: { headers: { 'Content-Type': 'multipart/form-data' } }
        })
          .then(function (response) {
            if (response.data.success) {
            }
          })
          .catch(function (response) {
          });
        setTimeout(() => {
          var indexCustomUser = 0;
          if (typeof priority !== 'undefined') {
            for (var index = 0; index < prioritySize; index++) {
              let customUser = self
              let customName = customUser.name[indexCustomUser]
              let customEmail = customUser.email[indexCustomUser]
              let customPhone = customUser.phone[indexCustomUser]
              var formData = new FormData()
              formData.append('seatNumber', priority[index])
              formData.append('idType', 4)
              axios({
                method: 'post',
                url: `${PRODUCTION}/api/asientos/ocupar.php`,
                data: formData,
                config: { headers: { 'Content-Type': 'multipart/form-data' } }
              })
                .then(function (response) {
                  if (response.data.success) {

                  }
                })
                .catch(function (response) {
                  //console.log('error')
                });
              formData = new FormData()
              formData.append('idTransaction', id)
              formData.append('seatNumber', priority[index])
              formData.append('seatType', 4)
              formData.append('ownerName', customName != '' && customName != '1' && customName != '2' && customName != '3' ? customName : `${user.name} ${user.lastName}`)
              formData.append('ownerEmail', customEmail != '' ? customEmail : user.email)
              formData.append('ownerMobile', customPhone != '' ? customPhone : user.phone)
              axios({
                method: 'post',
                url: `${PRODUCTION}/api/booking/insert.php`,
                data: formData,
                config: { headers: { 'Content-Type': 'multipart/form-data' } }
              })
                .then(function (response) {
                  if (response.data.success) {
                  }
                })
                .catch(function (response) {
                  //console.log('error')
                });
              indexCustomUser = indexCustomUser + 1
            }
          }

          if (typeof vip !== 'undefined') {
            for (var index = 0; index < vipSize; index++) {
              let customUser = self
              let customName = customUser.name[indexCustomUser]
              let customEmail = customUser.email[indexCustomUser]
              let customPhone = customUser.phone[indexCustomUser]
              var formData = new FormData()
              formData.append('seatNumber', vip[index])
              formData.append('idType', 3)
              axios({
                method: 'post',
                url: `${PRODUCTION}/api/asientos/ocupar.php`,
                data: formData,
                config: { headers: { 'Content-Type': 'multipart/form-data' } }
              })
                .then(function (response) {
                  if (response.data.success) {
                  }
                })
                .catch(function (response) {
                  //console.log('error')
                });
              formData = new FormData()
              formData.append('idTransaction', id)
              formData.append('seatNumber', vip[index])
              formData.append('seatType', 3)
              formData.append('ownerName', customName != '' && customName != '1' && customName != '2' && customName != '3' ? customName : `${user.name} ${user.lastName}`)
              formData.append('ownerEmail', customEmail != '' ? customEmail : user.email)
              formData.append('ownerMobile', customPhone != '' ? customPhone : user.phone)
              axios({
                method: 'post',
                url: `${PRODUCTION}/api/booking/insert.php`,
                data: formData,
                config: { headers: { 'Content-Type': 'multipart/form-data' } }
              })
                .then(function (response) {
                  if (response.data.success) {
                  }
                })
                .catch(function (response) {
                  //console.log('error')
                });
              indexCustomUser = indexCustomUser + 1
            }
          }

          if (typeof preferencial !== 'undefined') {
            for (var index = 0; index < preferencialSize; index++) {
              let customUser = self
              let customName = customUser.name[indexCustomUser]
              let customEmail = customUser.email[indexCustomUser]
              let customPhone = customUser.phone[indexCustomUser]
              var formData = new FormData()
              formData.append('seatNumber', preferencial[index])
              formData.append('idType', 2)
              axios({
                method: 'post',
                url: `${PRODUCTION}/api/asientos/ocupar.php`,
                data: formData,
                config: { headers: { 'Content-Type': 'multipart/form-data' } }
              })
                .then(function (response) {
                  if (response.data.success) {
                  }
                })
                .catch(function (response) {
                  //console.log('error')
                });
              formData = new FormData()
              formData.append('idTransaction', id)
              formData.append('seatNumber', preferencial[index])
              formData.append('seatType', 2)
              formData.append('ownerName', customName != '' && customName != '1' && customName != '2' && customName != '3' ? customName : `${user.name} ${user.lastName}`)
              formData.append('ownerEmail', customEmail != '' ? customEmail : user.email)
              formData.append('ownerMobile', customPhone != '' ? customPhone : user.phone)
              axios({
                method: 'post',
                url: `${PRODUCTION}/api/booking/insert.php`,
                data: formData,
                config: { headers: { 'Content-Type': 'multipart/form-data' } }
              })
                .then(function (response) {
                  if (response.data.success) {
                  }
                })
                .catch(function (response) {
                  //console.log('error')
                });
              indexCustomUser = indexCustomUser + 1
            }
          }


          if (typeof general !== 'undefined') {
            for (var index = 0; index < generalSize; index++) {
              let customUser = self
              let customName = customUser.name[indexCustomUser]
              let customEmail = customUser.email[indexCustomUser]
              let customPhone = customUser.phone[indexCustomUser]
              var formData = new FormData()
              formData.append('seatNumber', general[index])
              formData.append('idType', 1)
              axios({
                method: 'post',
                url: `${PRODUCTION}/api/asientos/ocupar.php`,
                data: formData,
                config: { headers: { 'Content-Type': 'multipart/form-data' } }
              })
                .then(function (response) {
                  if (response.data.success) {
                  }
                })
                .catch(function (response) {
                  //console.log('error')
                });
              formData = new FormData()
              formData.append('idTransaction', id)
              formData.append('seatNumber', general[index])
              formData.append('seatType', 1)
              formData.append('ownerName', customName != '' && customName != '1' && customName != '2' && customName != '3' ? customName : `${user.name} ${user.lastName}`)
              formData.append('ownerEmail', customEmail != '' ? customEmail : user.email)
              formData.append('ownerMobile', customPhone != '' ? customPhone : user.phone)
              axios({
                method: 'post',
                url: `${PRODUCTION}/api/booking/insert.php`,
                data: formData,
                config: { headers: { 'Content-Type': 'multipart/form-data' } }
              })
                .then(function (response) {
                  if (response.data.success) {
                  }
                })
                .catch(function (response) {
                  //console.log('error')
                });
              indexCustomUser = indexCustomUser + 1
            }
          }



          setTimeout(() => {
            axios({
              method: 'get',
              url: `${PRODUCTION}/api/pdf/read.php?idTransaction=${id}`,
            })
              .then(function (response) {
                //console.log("Correo enviado");
              })
              .catch(function (response) {
                //handle error
                //console.log(response);
              });
          }, 2000);
        }, 2000);
        
        if (withInvoice) {
          ReactPixel.track("Purchase", { value: amountIva, currency: 'MXN', method: "PayPal" });
        } else {
          ReactPixel.track("Purchase", { value: amount, currency: 'MXN', method: "PayPal" });
        }
        
        history.push("/success")
        //console.log('Pago confirmado')
      } else {
        //console.log('Tu pago no fue confirmado')
      }
      // `${PRODUCTION}/api/pdf/read.php?idTransaction=`,
      // `${PRODUCTION}/api/transactions/insert.php`,
      // `${PRODUCTION}/api/asientos/ocupar.php`,
    });
  }

  render() {
    let PayPalButton = paypal.Button.driver("react", { React, ReactDOM });
    return (
      <PayPalButton
        commit={this.state.commit}
        env={this.state.env}
        client={this.state.client}
        payment={(data, actions) => this.payment(data, actions)}
        onAuthorize={(data, actions) => this.onAuthorize(data, actions)}
        style={{ color: "silver", size: "medium" }}
      />
    );
  }
}

export default withRouter(Modal);