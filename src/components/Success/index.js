import React from "react";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { Grid } from "@material-ui/core";
import red from "@material-ui/core/colors/red";
import { ReactComponent as SvgIcon } from "../../icons/ticket.svg";

function Copyright({ classes }) {
  return (
    <Typography variant="body2" color="textSecondary" align="center" className={classes.copy} style={{color: "#fff"}}>
      {"CINEMA APP, Copyright © "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  copy: {
    color: "#fff",
    margin: theme.spacing(3)
  },
  layout: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  titulo: {
    color: "#fff",
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    [theme.breakpoints.down("xs")]: {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2)
    }
  },
  link: {
    textDecoration: "none",
    color: "#880e4f"
  },
  image: {
    width: "35vh"
    //marginTop: theme.spacing(2)
  },
}));

export default function Home() {
  const classes = useStyles();
  const history = useHistory();

  React.useEffect(() => {
    setTimeout(function () {
      history.push("/");
    }, 15000); //will call the function after 2 secs.
  }, []);

  return (
    <React.Fragment>
      <CssBaseline />
      <Grid
        container
        align="center"
        justify="center"
        className={classes.layout}
      >
        <Grid item xs={12}>
          <SvgIcon style={{ fill: "#d50000", height: 144, marginTop: 20 }} />
          <Typography variant="h5" style={{color: "#fff"}}>
            CINEMA APP
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h3" className={classes.titulo}>
            Su transacción fue procesada exitosamente,{" "}
            <a
              href="/"
              className={classes.link}
            >
              volver a inicio
          </a>
            .
          <Typography variant="h6" className={classes.titulo} style={{padding: 20}}>Asegúrese de revisar su carpeta de spam en caso de no encontrar el correo con sus boletos.<br/>Cualquier duda favor de contactar al correo <a  style={{textDecoration: 'none', color: 'white', fontWeight: 'bolder'}}>15121185@tecmor.mx</a></Typography>
          <Typography
              variant="subtitle2"
              gutterBottom
              className={classes.titulo}
            >
              Hemos enviado su confirmación de compra y su entrada (código QR) por
              correo electrónico.
          </Typography>
            
          </Typography>
          <img
            className={classes.image}
            src="/boleto.png"
            alt="CINE"
          />
        </Grid>
        
        <Copyright classes={classes} />
      </Grid>
    </React.Fragment>
  );
}
