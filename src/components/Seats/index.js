import React, { Component } from "react";
import SeatPicker from "react-seat-picker";
import { Typography, Grid } from "@material-ui/core";
import "../../css/seat.css";
import * as ticketsActions from "../../actions/tickets.Actions";
import { connect } from 'react-redux'
import Spinner from "../General/Spinner.js";
import { store } from "react-notifications-component";
import RN from "../Notification";
import StopRoundedIcon from "@material-ui/icons/StopRounded";

const general = [
    [{ id: 1, number: 'A1' }, { id: 2, number: 'B1' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C1' }, { id: 4, number: 'D1' },],
    [{ id: 1, number: 'A2' }, { id: 2, number: 'B2' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C2' }, { id: 4, number: 'D2' },],
    [{ id: 1, number: 'A3' }, { id: 2, number: 'B3' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C3' }, { id: 4, number: 'D3' },],
    [{ id: 1, number: 'A4' }, { id: 2, number: 'B4' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C4' }, { id: 4, number: 'D4' },],
    [{ id: 1, number: 'A5' }, { id: 2, number: 'B5' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C5' }, { id: 4, number: 'D5' },],
    [{ id: 1, number: 'A6' }, { id: 2, number: 'B6' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C6' }, { id: 4, number: 'D6' },],
    [{ id: 1, number: 'A7' }, { id: 2, number: 'B7' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C7' }, { id: 4, number: 'D7' },],
    [{ id: 1, number: 'A8' }, { id: 2, number: 'B8' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C8' }, { id: 4, number: 'D8' },],
    [{ id: 1, number: 'A9' }, { id: 2, number: 'B9' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C9' }, { id: 4, number: 'D9' },],
    [{ id: 1, number: 'A10' }, { id: 2, number: 'B10' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C10' }, { id: 4, number: 'D10' },],
    [{ id: 1, number: 'A11' }, { id: 2, number: 'B11' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C11' }, { id: 4, number: 'D11' },],
    [{ id: 1, number: 'A12' }, { id: 2, number: 'B12' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C12' }, { id: 4, number: 'D12' },],
    [{ id: 1, number: 'A13' }, { id: 2, number: 'B13' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C13' }, { id: 4, number: 'D13' },],
    [{ id: 1, number: 'A14' }, { id: 2, number: 'B14' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C14' }, { id: 4, number: 'D14' },],
    [{ id: 1, number: 'A15' }, { id: 2, number: 'B15' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C15' }, { id: 4, number: 'D15' },],
    [{ id: 1, number: 'A16' }, { id: 2, number: 'B16' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C16' }, { id: 4, number: 'D16' },],
    [{ id: 1, number: 'A17' }, { id: 2, number: 'B17' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C17' }, { id: 4, number: 'D17' },],
    [{ id: 1, number: 'A18' }, { id: 2, number: 'B18' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C18' }, { id: 4, number: 'D18' },],
    [{ id: 1, number: 'A19' }, { id: 2, number: 'B19' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C19' }, { id: 4, number: 'D19' },],
    [{ id: 1, number: 'A20' }, { id: 2, number: 'B20' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C20' }, { id: 4, number: 'D20' },],
    [{ id: 1, number: 'A21' }, { id: 2, number: 'B21' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C21' }, { id: 4, number: 'D21' },],
    [{ id: 1, number: 'A22' }, { id: 2, number: 'B22' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C22' }, { id: 4, number: 'D22' },],
    [{ id: 1, number: 'A23' }, { id: 2, number: 'B23' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C23' }, { id: 4, number: 'D23' },],
    [{ id: 1, number: 'A24' }, { id: 2, number: 'B24' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C24' }, { id: 4, number: 'D24' },],
    [{ id: 1, number: 'A25' }, { id: 2, number: 'B25' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C25' }, { id: 4, number: 'D25' },],
    [{ id: 1, number: 'A26' }, { id: 2, number: 'B26' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C26' }, { id: 4, number: 'D26' },],
    [{ id: 1, number: 'A27' }, { id: 2, number: 'B27' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C27' }, { id: 4, number: 'D27' },],
    [{ id: 1, number: 'A28' }, { id: 2, number: 'B28' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C28' }, { id: 4, number: 'D28' },],
    [{ id: 1, number: 'A29' }, { id: 2, number: 'B29' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C29' }, { id: 4, number: 'D29' },],
    [{ id: 1, number: 'A30' }, { id: 2, number: 'B30' }, null, null, null, null, null, null, null, null, { id: 3, number: 'C30' }, { id: 4, number: 'D30' },],
]
const preferente = [
    [{ id: 1, number: 'A1' }, { id: 2, number: 'B1' }, { id: 2, number: 'C1' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D1' }, { id: 4, number: 'E1' }, { id: 5, number: 'F1' },],
    [{ id: 1, number: 'A2' }, { id: 2, number: 'B2' }, { id: 2, number: 'C2' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D2' }, { id: 4, number: 'E2' }, { id: 5, number: 'F2' },],
    [{ id: 1, number: 'A3' }, { id: 2, number: 'B3' }, { id: 2, number: 'C3' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D3' }, { id: 4, number: 'E3' }, { id: 5, number: 'F3' },],
    [{ id: 1, number: 'A4' }, { id: 2, number: 'B4' }, { id: 2, number: 'C4' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D4' }, { id: 4, number: 'E4' }, { id: 5, number: 'F4' },],
    [{ id: 1, number: 'A5' }, { id: 2, number: 'B5' }, { id: 2, number: 'C5' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D5' }, { id: 4, number: 'E5' }, { id: 5, number: 'F5' },],
    [{ id: 1, number: 'A6' }, { id: 2, number: 'B6' }, { id: 2, number: 'C6' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D6' }, { id: 4, number: 'E6' }, { id: 5, number: 'F6' },],
    [{ id: 1, number: 'A7' }, { id: 2, number: 'B7' }, { id: 2, number: 'C7' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D7' }, { id: 4, number: 'E7' }, { id: 5, number: 'F7' },],
    [{ id: 1, number: 'A8' }, { id: 2, number: 'B8' }, { id: 2, number: 'C8' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D8' }, { id: 4, number: 'E8' }, { id: 5, number: 'F8' },],
    [{ id: 1, number: 'A9' }, { id: 2, number: 'B9' }, { id: 2, number: 'C9' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D9' }, { id: 4, number: 'E9' }, { id: 5, number: 'F9' },],
    [{ id: 1, number: 'A10' }, { id: 2, number: 'B10' }, { id: 2, number: 'C10' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D10' }, { id: 4, number: 'E10' }, { id: 5, number: 'F10' },],
    [{ id: 1, number: 'A11' }, { id: 2, number: 'B11' }, { id: 2, number: 'C11' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D11' }, { id: 4, number: 'E11' }, { id: 5, number: 'F11' },],
    [{ id: 1, number: 'A12' }, { id: 2, number: 'B12' }, { id: 2, number: 'C12' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D12' }, { id: 4, number: 'E12' }, { id: 5, number: 'F12' },],
    [{ id: 1, number: 'A13' }, { id: 2, number: 'B13' }, { id: 2, number: 'C13' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D13' }, { id: 4, number: 'E13' }, { id: 5, number: 'F13' },],
    [{ id: 1, number: 'A14' }, { id: 2, number: 'B14' }, { id: 2, number: 'C14' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D14' }, { id: 4, number: 'E14' }, { id: 5, number: 'F14' },],
    [{ id: 1, number: 'A15' }, { id: 2, number: 'B15' }, { id: 2, number: 'C15' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D15' }, { id: 4, number: 'E15' }, { id: 5, number: 'F15' },],
    [{ id: 1, number: 'A16' }, { id: 2, number: 'B16' }, { id: 2, number: 'C16' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D16' }, { id: 4, number: 'E16' }, { id: 5, number: 'F16' },],
    [{ id: 1, number: 'A17' }, { id: 2, number: 'B17' }, { id: 2, number: 'C17' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D17' }, { id: 4, number: 'E17' }, { id: 5, number: 'F17' },],
    [{ id: 1, number: 'A18' }, { id: 2, number: 'B18' }, { id: 2, number: 'C18' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D18' }, { id: 4, number: 'E18' }, { id: 5, number: 'F18' },],
    [{ id: 1, number: 'A19' }, { id: 2, number: 'B19' }, { id: 2, number: 'C19' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D19' }, { id: 4, number: 'E19' }, { id: 5, number: 'F19' },],
    [{ id: 1, number: 'A20' }, { id: 2, number: 'B20' }, { id: 2, number: 'C20' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D20' }, { id: 4, number: 'E20' }, { id: 5, number: 'F20' },],
    [{ id: 1, number: 'A21' }, { id: 2, number: 'B21' }, { id: 2, number: 'C21' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D21' }, { id: 4, number: 'E21' }, { id: 5, number: 'F21' },],
    [{ id: 1, number: 'A22' }, { id: 2, number: 'B22' }, { id: 2, number: 'C22' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D22' }, { id: 4, number: 'E22' }, { id: 5, number: 'F22' },],
    [{ id: 1, number: 'A23' }, { id: 2, number: 'B23' }, { id: 2, number: 'C23' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D23' }, { id: 4, number: 'E23' }, { id: 5, number: 'F23' },],
    [{ id: 1, number: 'A24' }, { id: 2, number: 'B24' }, { id: 2, number: 'C24' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D24' }, { id: 4, number: 'E24' }, { id: 5, number: 'F24' },],
    [{ id: 1, number: 'A25' }, { id: 2, number: 'B25' }, { id: 2, number: 'C25' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D25' }, { id: 4, number: 'E25' }, { id: 5, number: 'F25' },],
    [{ id: 1, number: 'A26' }, { id: 2, number: 'B26' }, { id: 2, number: 'C26' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D26' }, { id: 4, number: 'E26' }, { id: 5, number: 'F26' },],
    [{ id: 1, number: 'A27' }, { id: 2, number: 'B27' }, { id: 2, number: 'C27' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D27' }, { id: 4, number: 'E27' }, { id: 5, number: 'F27' },],
    [{ id: 1, number: 'A28' }, { id: 2, number: 'B28' }, { id: 2, number: 'C28' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D28' }, { id: 4, number: 'E28' }, { id: 5, number: 'F28' },],
    [{ id: 1, number: 'A29' }, { id: 2, number: 'B29' }, { id: 2, number: 'C29' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D29' }, { id: 4, number: 'E29' }, { id: 5, number: 'F29' },],
    [{ id: 1, number: 'A30' }, { id: 2, number: 'B30' }, { id: 2, number: 'C30' }, null, null, null, null, null, null, null, null, { id: 3, number: 'D30' }, { id: 4, number: 'E30' }, { id: 5, number: 'F30' },],
]
const vip = [
    [{ id: 1, number: 'A1' }, { id: 2, number: 'B1' }, { id: 2, number: 'C1' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D1' }, { id: 4, number: 'E1' }, { id: 5, number: 'F1' },],
    [{ id: 1, number: 'A2' }, { id: 2, number: 'B2' }, { id: 2, number: 'C2' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D2' }, { id: 4, number: 'E2' }, { id: 5, number: 'F2' },],
    [{ id: 1, number: 'A3' }, { id: 2, number: 'B3' }, { id: 2, number: 'C3' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D3' }, { id: 4, number: 'E3' }, { id: 5, number: 'F3' },],
    [{ id: 1, number: 'A4' }, { id: 2, number: 'B4' }, { id: 2, number: 'C4' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D4' }, { id: 4, number: 'E4' }, { id: 5, number: 'F4' },],
    [{ id: 1, number: 'A5' }, { id: 2, number: 'B5' }, { id: 2, number: 'C5' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D5' }, { id: 4, number: 'E5' }, { id: 5, number: 'F5' },],
    [{ id: 1, number: 'A6' }, { id: 2, number: 'B6' }, { id: 2, number: 'C6' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D6' }, { id: 4, number: 'E6' }, { id: 5, number: 'F6' },],
    [{ id: 1, number: 'A7' }, { id: 2, number: 'B7' }, { id: 2, number: 'C7' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D7' }, { id: 4, number: 'E7' }, { id: 5, number: 'F7' },],
    [{ id: 1, number: 'A8' }, { id: 2, number: 'B8' }, { id: 2, number: 'C8' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D8' }, { id: 4, number: 'E8' }, { id: 5, number: 'F8' },],
    [{ id: 1, number: 'A9' }, { id: 2, number: 'B9' }, { id: 2, number: 'C9' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D9' }, { id: 4, number: 'E9' }, { id: 5, number: 'F9' },],
    [{ id: 1, number: 'A10' }, { id: 2, number: 'B10' }, { id: 2, number: 'C10' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D10' }, { id: 4, number: 'E10' }, { id: 5, number: 'F10' },],
    [{ id: 1, number: 'A11' }, { id: 2, number: 'B11' }, { id: 2, number: 'C11' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D11' }, { id: 4, number: 'E11' }, { id: 5, number: 'F11' },],
    [{ id: 1, number: 'A12' }, { id: 2, number: 'B12' }, { id: 2, number: 'C12' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D12' }, { id: 4, number: 'E12' }, { id: 5, number: 'F12' },],
    [{ id: 1, number: 'A13' }, { id: 2, number: 'B13' }, { id: 2, number: 'C13' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D13' }, { id: 4, number: 'E13' }, { id: 5, number: 'F13' },],
    [{ id: 1, number: 'A14' }, { id: 2, number: 'B14' }, { id: 2, number: 'C14' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D14' }, { id: 4, number: 'E14' }, { id: 5, number: 'F14' },],
    [{ id: 1, number: 'A15' }, { id: 2, number: 'B15' }, { id: 2, number: 'C15' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D15' }, { id: 4, number: 'E15' }, { id: 5, number: 'F15' },],
    [{ id: 1, number: 'A16' }, { id: 2, number: 'B16' }, { id: 2, number: 'C16' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D16' }, { id: 4, number: 'E16' }, { id: 5, number: 'F16' },],
    [{ id: 1, number: 'A17' }, { id: 2, number: 'B17' }, { id: 2, number: 'C17' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D17' }, { id: 4, number: 'E17' }, { id: 5, number: 'F17' },],
    [{ id: 1, number: 'A18' }, { id: 2, number: 'B18' }, { id: 2, number: 'C18' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D18' }, { id: 4, number: 'E18' }, { id: 5, number: 'F18' },],
    [{ id: 1, number: 'A19' }, { id: 2, number: 'B19' }, { id: 2, number: 'C19' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D19' }, { id: 4, number: 'E19' }, { id: 5, number: 'F19' },],
    [{ id: 1, number: 'A20' }, { id: 2, number: 'B20' }, { id: 2, number: 'C20' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D20' }, { id: 4, number: 'E20' }, { id: 5, number: 'F20' },],
    [{ id: 1, number: 'A21' }, { id: 2, number: 'B21' }, { id: 2, number: 'C21' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D21' }, { id: 4, number: 'E21' }, { id: 5, number: 'F21' },],
    [{ id: 1, number: 'A22' }, { id: 2, number: 'B22' }, { id: 2, number: 'C22' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D22' }, { id: 4, number: 'E22' }, { id: 5, number: 'F22' },],
    [null],
    [{ id: 1, number: 'G1' }, { id: 2, number: 'G2' }, { id: 3, number: 'G3' }, { id: 4, number: 'G4' }, { id: 5, number: 'G5' }, { id: 6, number: 'G6' }, { id: 7, number: 'G7' }, { id: 8, number: 'G8' }, { id: 9, number: 'G9' }, { id: 10, number: 'G10' }, { id: 11, number: 'G11' }, { id: 12, number: 'G12' }, { id: 13, number: 'G13' }, { id: 14, number: 'G14' }, { id: 15, number: 'G15' }, { id: 16, number: 'G16' }, { id: 17, number: 'G17' }, null, { id: 18, number: 'G18' }, { id: 19, number: 'G19' }, { id: 20, number: 'G20' }, { id: 21, number: 'G21' }, { id: 22, number: 'G22' }, { id: 23, number: 'G23' }, { id: 24, number: 'G24' }, { id: 25, number: 'G25' }, { id: 26, number: 'G26' }, { id: 27, number: 'G27' }, { id: 28, number: 'G28' }, { id: 29, number: 'G29' }, { id: 30, number: 'G30' }, { id: 31, number: 'G31' }, { id: 32, number: 'G32' }, { id: 33, number: 'G33' }, { id: 34, number: 'G34' },],
    [{ id: 1, number: 'H1' }, { id: 2, number: 'H2' }, { id: 3, number: 'H3' }, { id: 4, number: 'H4' }, { id: 5, number: 'H5' }, { id: 6, number: 'H6' }, { id: 7, number: 'H7' }, { id: 8, number: 'H8' }, { id: 9, number: 'H9' }, { id: 10, number: 'H10' }, { id: 11, number: 'H11' }, { id: 12, number: 'H12' }, { id: 13, number: 'H13' }, { id: 14, number: 'H14' }, { id: 15, number: 'H15' }, { id: 16, number: 'H16' }, { id: 17, number: 'H17' }, null, { id: 18, number: 'H18' }, { id: 19, number: 'H19' }, { id: 20, number: 'H20' }, { id: 21, number: 'H21' }, { id: 22, number: 'H22' }, { id: 23, number: 'H23' }, { id: 24, number: 'H24' }, { id: 25, number: 'H25' }, { id: 26, number: 'H26' }, { id: 27, number: 'H27' }, { id: 28, number: 'H28' }, { id: 29, number: 'H29' }, { id: 30, number: 'H30' }, { id: 31, number: 'H31' }, { id: 32, number: 'H32' }, { id: 33, number: 'H33' }, { id: 34, number: 'H34' },],
    [{ id: 1, number: 'I1' }, { id: 2, number: 'I2' }, { id: 3, number: 'I3' }, { id: 4, number: 'I4' }, { id: 5, number: 'I5' }, { id: 6, number: 'I6' }, { id: 7, number: 'I7' }, { id: 8, number: 'I8' }, { id: 9, number: 'I9' }, { id: 10, number: 'I10' }, { id: 11, number: 'I11' }, { id: 12, number: 'I12' }, { id: 13, number: 'I13' }, { id: 14, number: 'I14' }, { id: 15, number: 'I15' }, { id: 16, number: 'I16' }, { id: 17, number: 'I17' }, null, { id: 18, number: 'I18' }, { id: 19, number: 'I19' }, { id: 20, number: 'I20' }, { id: 21, number: 'I21' }, { id: 22, number: 'I22' }, { id: 23, number: 'I23' }, { id: 24, number: 'I24' }, { id: 25, number: 'I25' }, { id: 26, number: 'I26' }, { id: 27, number: 'I27' }, { id: 28, number: 'I28' }, { id: 29, number: 'I29' }, { id: 30, number: 'I30' }, { id: 31, number: 'I31' }, { id: 32, number: 'I32' }, { id: 33, number: 'I33' }, { id: 34, number: 'I34' },],
    [{ id: 1, number: 'J1' }, { id: 2, number: 'J2' }, { id: 3, number: 'J3' }, { id: 4, number: 'J4' }, { id: 5, number: 'J5' }, { id: 6, number: 'J6' }, { id: 7, number: 'J7' }, { id: 8, number: 'J8' }, { id: 9, number: 'J9' }, { id: 10, number: 'J10' }, { id: 11, number: 'J11' }, { id: 12, number: 'J12' }, { id: 13, number: 'J13' }, { id: 14, number: 'J14' }, { id: 15, number: 'J15' }, { id: 16, number: 'J16' }, { id: 17, number: 'J17' }, null, { id: 18, number: 'J18' }, { id: 19, number: 'J19' }, { id: 20, number: 'J20' }, { id: 21, number: 'J21' }, { id: 22, number: 'J22' }, { id: 23, number: 'J23' }, { id: 24, number: 'J24' }, { id: 25, number: 'J25' }, { id: 26, number: 'J26' }, { id: 27, number: 'J27' }, { id: 28, number: 'J28' }, { id: 29, number: 'J29' }, { id: 30, number: 'J30' }, { id: 31, number: 'J31' }, { id: 32, number: 'J32' }, { id: 33, number: 'J33' }, { id: 34, number: 'J34' },],
]
const priority = [
    [{ id: 1, number: 'A1' }, { id: 2, number: 'B1' }, { id: 2, number: 'C1' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D1' }, { id: 4, number: 'E1' }, { id: 5, number: 'F1' },],
    [{ id: 1, number: 'A2' }, { id: 2, number: 'B2' }, { id: 2, number: 'C2' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D2' }, { id: 4, number: 'E2' }, { id: 5, number: 'F2' },],
    [{ id: 1, number: 'A3' }, { id: 2, number: 'B3' }, { id: 2, number: 'C3' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D3' }, { id: 4, number: 'E3' }, { id: 5, number: 'F3' },],
    [{ id: 1, number: 'A4' }, { id: 2, number: 'B4' }, { id: 2, number: 'C4' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D4' }, { id: 4, number: 'E4' }, { id: 5, number: 'F4' },],
    [{ id: 1, number: 'A5' }, { id: 2, number: 'B5' }, { id: 2, number: 'C5' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D5' }, { id: 4, number: 'E5' }, { id: 5, number: 'F5' },],
    [{ id: 1, number: 'A6' }, { id: 2, number: 'B6' }, { id: 2, number: 'C6' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D6' }, { id: 4, number: 'E6' }, { id: 5, number: 'F6' },],
    [{ id: 1, number: 'A7' }, { id: 2, number: 'B7' }, { id: 2, number: 'C7' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D7' }, { id: 4, number: 'E7' }, { id: 5, number: 'F7' },],
    [{ id: 1, number: 'A8' }, { id: 2, number: 'B8' }, { id: 2, number: 'C8' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D8' }, { id: 4, number: 'E8' }, { id: 5, number: 'F8' },],
    [{ id: 1, number: 'A9' }, { id: 2, number: 'B9' }, { id: 2, number: 'C9' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D9' }, { id: 4, number: 'E9' }, { id: 5, number: 'F9' },],
    [{ id: 1, number: 'A10' }, { id: 2, number: 'B10' }, { id: 2, number: 'C10' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D10' }, { id: 4, number: 'E10' }, { id: 5, number: 'F10' },],
    [{ id: 1, number: 'A11' }, { id: 2, number: 'B11' }, { id: 2, number: 'C11' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D11' }, { id: 4, number: 'E11' }, { id: 5, number: 'F11' },],
    [{ id: 1, number: 'A12' }, { id: 2, number: 'B12' }, { id: 2, number: 'C12' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D12' }, { id: 4, number: 'E12' }, { id: 5, number: 'F12' },],
    [{ id: 1, number: 'A13' }, { id: 2, number: 'B13' }, { id: 2, number: 'C13' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D13' }, { id: 4, number: 'E13' }, { id: 5, number: 'F13' },],
    [{ id: 1, number: 'A14' }, { id: 2, number: 'B14' }, { id: 2, number: 'C14' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D14' }, { id: 4, number: 'E14' }, { id: 5, number: 'F14' },], [null],
    [{ id: 1, number: 'G1' }, { id: 2, number: 'G2' }, { id: 3, number: 'G3' }, { id: 4, number: 'G4' }, { id: 5, number: 'G5' }, { id: 6, number: 'G6' }, { id: 7, number: 'G7' }, { id: 8, number: 'G8' }, { id: 9, number: 'G9' }, { id: 10, number: 'G10' }, { id: 11, number: 'G11' }, { id: 12, number: 'G12' }, { id: 13, number: 'G13' }, { id: 14, number: 'G14' }, { id: 15, number: 'G15' }, { id: 16, number: 'G16' }, { id: 17, number: 'G17' }, { id: 18, number: 'G18' }, { id: 19, number: 'G19' }, { id: 20, number: 'G20' }, { id: 21, number: 'G21' }, { id: 22, number: 'G22' },],
    [{ id: 1, number: 'H1' }, { id: 2, number: 'H2' }, { id: 3, number: 'H3' }, { id: 4, number: 'H4' }, { id: 5, number: 'H5' }, { id: 6, number: 'H6' }, { id: 7, number: 'H7' }, { id: 8, number: 'H8' }, { id: 9, number: 'H9' }, { id: 10, number: 'H10' }, { id: 11, number: 'H11' }, { id: 12, number: 'H12' }, { id: 13, number: 'H13' }, { id: 14, number: 'H14' }, { id: 15, number: 'H15' }, { id: 16, number: 'H16' }, { id: 17, number: 'H17' }, { id: 18, number: 'H18' }, { id: 19, number: 'H19' }, { id: 20, number: 'H20' }, { id: 21, number: 'H21' }, { id: 22, number: 'H22' },],
    [{ id: 1, number: 'I1' }, { id: 2, number: 'I2' }, { id: 3, number: 'I3' }, { id: 4, number: 'I4' }, { id: 5, number: 'I5' }, { id: 6, number: 'I6' }, { id: 7, number: 'I7' }, { id: 8, number: 'I8' }, { id: 9, number: 'I9' }, { id: 10, number: 'I10' }, { id: 11, number: 'I11' }, { id: 12, number: 'I12' }, { id: 13, number: 'I13' }, { id: 14, number: 'I14' }, { id: 15, number: 'I15' }, { id: 16, number: 'I16' }, { id: 17, number: 'I17' }, { id: 18, number: 'I18' }, { id: 19, number: 'I19' }, { id: 20, number: 'I20' }, { id: 21, number: 'I21' }, { id: 22, number: 'I22' },],
]
const priorityPlus = [
    [null, null, { id: 2, number: 'C1' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D1' }, null, null,],
    [null, null, { id: 2, number: 'C2' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D2' }, null, null,],
    [null, null, { id: 2, number: 'C3' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D3' }, null, null,],
    [null, null, { id: 2, number: 'C4' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D4' },null, null,],
    [null, null, { id: 2, number: 'C5' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D5' }, null, null,],
    [null, null, { id: 2, number: 'C6' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D6' }, null, null,],
    [null, null, { id: 2, number: 'C7' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D7' }, null, null,],
    [null, null, { id: 2, number: 'C8' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D8' }, null, null,],
    [null, null, { id: 2, number: 'C9' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D9' }, null, null,],
    [null, null, { id: 2, number: 'C10' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D10' }, null, null,],
    [null, null, { id: 2, number: 'C11' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D11' }, null, null,],
    [null, null, { id: 2, number: 'C12' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D12' }, null, null,],
    [null, null, { id: 2, number: 'C13' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D13' }, null, null,],
    [null, null, { id: 2, number: 'C14' }, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, { id: 3, number: 'D14' }, null, null,], [null],

    [{ id: 1, number: 'G1' }, { id: 2, number: 'G2' }, { id: 3, number: 'G3' }, { id: 4, number: 'G4' }, { id: 5, number: 'G5' }, { id: 6, number: 'G6' }, { id: 7, number: 'G7' }, { id: 8, number: 'G8' }, { id: 9, number: 'G9' }, { id: 10, number: 'G10' }, { id: 11, number: 'G11' }, { id: 12, number: 'G12' }, { id: 13, number: 'G13' }, { id: 14, number: 'G14' }, { id: 15, number: 'G15' }, { id: 16, number: 'G16' }, { id: 17, number: 'G17' }, { id: 18, number: 'G18' }, { id: 19, number: 'G19' }, { id: 20, number: 'G20' }, { id: 21, number: 'G21' }, { id: 22, number: 'G22' },],
    [null],
    
]
class App extends Component {
    componentDidMount() {
        //this.props.readSeats();
    }

    constructor(props) {
        super(props);
        // this.state = props.seats
        if(props.isPlus){
            this.state = {
                loading: false,
                showModal: true,
                seleccionadosGeneral: [],
                ocupadosGeneral: [],
                seleccionadosPreferente: [],
                ocupadosPreferente: [],
                seleccionadosVip: [],
                ocupadosVip: [],
                seleccionadosPriority: [],
                ocupadosPriority: [],
                mostrandoAsientos: props.zona === 1 ? priorityPlus : props.zona === 2 ? vip : props.zona === 3 ? preferente : general,
                amount: props.amount,
                isPlus: true,
                seleccionadosPriorityPlus: [],
            };
        }else{
            this.state = {
                loading: false,
                showModal: true,
                seleccionadosGeneral: [],
                ocupadosGeneral: [],
                seleccionadosPreferente: [],
                ocupadosPreferente: [],
                seleccionadosVip: [],
                ocupadosVip: [],
                seleccionadosPriority: [],
                ocupadosPriority: [],
                mostrandoAsientos: props.zona === 1 ? priority : props.zona === 2 ? vip : props.zona === 3 ? preferente : general,
                amount: props.amount,
                isPlus: false,
                seleccionadosPriorityPlus: [],
            };
        }
        //useSelector(store => store.ticketsReducer)
    }


    addSeatCallback = ({ row, number, id }, addCb) => {
        if (this.state.isPlus){
            var seats = this.props.seats;
            this.setState({
                loading: true
            }, async () => {
                await new Promise(resolve => setTimeout(resolve, 100))
                const newTooltip = `Presiona para remover`
                let idType = 0
                switch (this.state.mostrandoAsientos) {
                    case general:
                        idType = 1
                        break
                    case vip:
                        idType = 3
                        break
                    case preferente:
                        idType = 2
                        break
                    case priorityPlus:
                        idType = 4
                        break
                }
                let formData = new FormData();
                formData.append('seatNumber', number)
                formData.append('idType', idType)
                formData.append('isPlus', true)
                let stateTemp = this.state;
                let propsTemp = this.props;
                addCb(row, number, id, newTooltip);
                            switch (stateTemp.mostrandoAsientos) {
                                case general:
                                    stateTemp.seleccionadosGeneral.push(number)
                                    break
                                case vip:
                                    stateTemp.seleccionadosVip.push(number)
                                    break
                                case preferente:
                                    stateTemp.seleccionadosPreferente.push(number)
                                    break
                                case priorityPlus:
                                    stateTemp.seleccionadosPriorityPlus.push(number)
                                    break
                            }

                            store.addNotification({
                                content: (
                                    <RN
                                        title="Asiento apartado"
                                        message="Recuerda que tu asiento será reservado una vez concretadao tu pago."
                                        type="Success"
                                    />
                                ),
                                insert: "top",
                                container: "top-right",
                                animationIn: ["animated", "fadeIn"],
                                animationOut: ["animated", "fadeOut"],
                                dismiss: {
                                    duration: 3000,
                                    onScreen: false
                                }
                            });

                            propsTemp.reservarTemp(number, idType, false, stateTemp, seats, false) //ADD

                this.setState({ loading: false })
                this.obtenerSeleccionados();
            });

        }else{
            var seats = this.props.seats;
            this.setState({
                loading: true
            }, async () => {
                await new Promise(resolve => setTimeout(resolve, 100))
                const newTooltip = `Presiona para remover`
                let idType = 0
                switch (this.state.mostrandoAsientos) {
                    case general:
                        idType = 1
                        break
                    case vip:
                        idType = 3
                        break
                    case preferente:
                        idType = 2
                        break
                    case priority:
                        idType = 4
                        break
                }
                let formData = new FormData();
                formData.append('seatNumber', number)
                formData.append('idType', idType)
                let stateTemp = this.state;
                let propsTemp = this.props;
                addCb(row, number, id, newTooltip);
                            switch (stateTemp.mostrandoAsientos) {
                                case general:
                                    stateTemp.seleccionadosGeneral.push(number)
                                    break
                                case vip:
                                    stateTemp.seleccionadosVip.push(number)
                                    break
                                case preferente:
                                    stateTemp.seleccionadosPreferente.push(number)
                                    break
                                case priority:
                                    stateTemp.seleccionadosPriority.push(number)
                                    break
                            }

                            store.addNotification({
                                content: (
                                    <RN
                                        title="Asiento apartado"
                                        message="Recuerda que tu asiento será reservado una vez concretadao tu pago."
                                        type="Success"
                                    />
                                ),
                                insert: "top",
                                container: "top-right",
                                animationIn: ["animated", "fadeIn"],
                                animationOut: ["animated", "fadeOut"],
                                dismiss: {
                                    duration: 3000,
                                    onScreen: false
                                }
                            });
                            propsTemp.reservarTemp(number, idType, false, stateTemp, seats, false) //ADD
                

                this.setState({ loading: false })
                this.obtenerSeleccionados();
            });
        }
    }

    removeSeatCallback = ({ row, number, id }, removeCb) => {
        if (this.state.isPlus) {
            var seats = this.props.seats;
            this.setState({
                loading: true
            }, async () => {
                await new Promise(resolve => setTimeout(resolve, 100))
                removeCb(row, number)
                let idType = 0

                const stateTemp = this.state;
                const propsTemp = this.props;
                switch (stateTemp.mostrandoAsientos) {
                    case general:
                        idType = 1
                        for (var index in stateTemp.seleccionadosGeneral) {
                            const item = stateTemp.seleccionadosGeneral[index]
                            if (item === number) {
                                stateTemp.seleccionadosGeneral.splice(index, 1)
                                break
                            }
                        }
                        break
                    case vip:
                        idType = 3
                        for (var index in stateTemp.seleccionadosVip) {
                            const item = stateTemp.seleccionadosVip[index]
                            if (item === number) {
                                stateTemp.seleccionadosVip.splice(index, 1)
                                break
                            }
                        }
                        break
                    case preferente:
                        idType = 2
                        for (var index in stateTemp.seleccionadosPreferente) {
                            const item = stateTemp.seleccionadosPreferente[index]
                            if (item === number) {
                                stateTemp.seleccionadosPreferente.splice(index, 1)
                                break
                            }
                        }
                        break
                    case priorityPlus:
                        idType = 4
                        for (var index in stateTemp.seleccionadosPriorityPlus) {
                            const item = stateTemp.seleccionadosPriorityPlus[index]
                            if (item === number) {
                                stateTemp.seleccionadosPriorityPlus.splice(index, 1)
                                break
                            }
                        }
                        break
                }
                propsTemp.reservarTemp(number, idType, true, stateTemp, seats, true); //REMOVE number, idType, false, stateTemp, seats
                this.setState({ loading: false })
                var ocupados = []
                switch (this.state.mostrandoAsientos) {
                    case general:
                        ocupados = this.state.ocupadosGeneral
                        break
                    case vip:
                        ocupados = this.state.ocupadosVip
                        break
                    case preferente:
                        ocupados = this.state.ocupadosPreferente
                        break
                    case priorityPlus:
                        ocupados = this.state.ocupadosPriority
                        break
                }
                this.marcarOcupados(ocupados, this.state.mostrandoAsientos)
            })
        }else{
            var seats = this.props.seats;
            this.setState({
                loading: true
            }, async () => {
                await new Promise(resolve => setTimeout(resolve, 100))
                removeCb(row, number)
                let idType = 0

                const stateTemp = this.state;
                const propsTemp = this.props;
                switch (stateTemp.mostrandoAsientos) {
                    case general:
                        idType = 1
                        for (var index in stateTemp.seleccionadosGeneral) {
                            const item = stateTemp.seleccionadosGeneral[index]
                            if (item === number) {
                                stateTemp.seleccionadosGeneral.splice(index, 1)
                                break
                            }
                        }
                        break
                    case vip:
                        idType = 3
                        for (var index in stateTemp.seleccionadosVip) {
                            const item = stateTemp.seleccionadosVip[index]
                            if (item === number) {
                                stateTemp.seleccionadosVip.splice(index, 1)
                                break
                            }
                        }
                        break
                    case preferente:
                        idType = 2
                        for (var index in stateTemp.seleccionadosPreferente) {
                            const item = stateTemp.seleccionadosPreferente[index]
                            if (item === number) {
                                stateTemp.seleccionadosPreferente.splice(index, 1)
                                break
                            }
                        }
                        break
                    case priority:
                        idType = 4
                        for (var index in stateTemp.seleccionadosPriority) {
                            const item = stateTemp.seleccionadosPriority[index]
                            if (item === number) {
                                stateTemp.seleccionadosPriority.splice(index, 1)
                                break
                            }
                        }
                        break
                }
                propsTemp.reservarTemp(number, idType, true, stateTemp, seats, true); //REMOVE number, idType, false, stateTemp, seats
                this.setState({ loading: false })
                var ocupados = []
                switch (this.state.mostrandoAsientos) {
                    case general:
                        ocupados = this.state.ocupadosGeneral
                        break
                    case vip:
                        ocupados = this.state.ocupadosVip
                        break
                    case preferente:
                        ocupados = this.state.ocupadosPreferente
                        break
                    case priority:
                        ocupados = this.state.ocupadosPriority
                        break
                }
                this.marcarOcupados(ocupados, this.state.mostrandoAsientos)
            })
        }
        
    }

    marcarOcupados = (ocupados, elementos) => {
        for (var key in ocupados) {
            const elem = ocupados[key]
            for (var index in elementos) {
                var array = elementos[index]
                if (array != null) {
                    for (var i in array) {
                        const l = array[i]
                        if (l != null && l.number === elem) {
                            l.isReserved = true
                        }
                    }
                }
            }
        }
    }

    obtenerSeleccionados = () => {
    }

    render() {
        var ocupados = []
        switch (this.state.mostrandoAsientos) {
            case general:
                ocupados = this.state.ocupadosGeneral
                break
            case vip:
                ocupados = this.state.ocupadosVip
                break
            case preferente:
                ocupados = this.state.ocupadosPreferente
                break
            case priority:
                ocupados = this.state.ocupadosPriority
                break
        }
        this.marcarOcupados(ocupados, this.state.mostrandoAsientos)
        

        if (this.props.read_seats.success) {
            
            if(this.state.isPlus){
                for (var index in this.props.read_seats.ocupados_plus) {
                    const item = this.props.read_seats.ocupados_plus[index];
                    if (item.idType === "1") {
                        this.state.ocupadosGeneral.push(item.seatNumber);
                    }
                    if (item.idType === "2") {
                        this.state.ocupadosPreferente.push(item.seatNumber);
                    }
                    if (item.idType === "3") {
                        this.state.ocupadosVip.push(item.seatNumber);
                    }
                    if (item.idType === "4") {
                        this.state.ocupadosPriority.push(item.seatNumber);
                    }
                }
                switch (this.state.mostrandoAsientos) {
                    case general:
                        ocupados = this.state.ocupadosGeneral
                        break
                    case vip:
                        ocupados = this.state.ocupadosVip
                        break
                    case preferente:
                        ocupados = this.state.ocupadosPreferente
                        break
                    case priorityPlus:
                        ocupados = this.state.ocupadosPriority
                        break
                }
            }else{
                for (var index in this.props.read_seats.ocupados) {
                    const item = this.props.read_seats.ocupados[index];
                    if (item.idType === "1") {
                        this.state.ocupadosGeneral.push(item.seatNumber);
                    }
                    if (item.idType === "2") {
                        this.state.ocupadosPreferente.push(item.seatNumber);
                    }
                    if (item.idType === "3") {
                        this.state.ocupadosVip.push(item.seatNumber);
                    }
                    if (item.idType === "4") {
                        this.state.ocupadosPriority.push(item.seatNumber);
                    }
                }
                switch (this.state.mostrandoAsientos) {
                    case general:
                        ocupados = this.state.ocupadosGeneral
                        break
                    case vip:
                        ocupados = this.state.ocupadosVip
                        break
                    case preferente:
                        ocupados = this.state.ocupadosPreferente
                        break
                    case priority:
                        ocupados = this.state.ocupadosPriority
                        break
                }
            }
            
            
            this.marcarOcupados(ocupados, this.state.mostrandoAsientos)
            if (this.props.seats.lenght > 0) {
                //console.log(this.props.seats)
                this.state = this.props.seats
            }
            
        }

        if (this.props.read_loading_seats) {
            return <Spinner/>
        }

        

        return (
            <div>
                <SeatPicker
                    addSeatCallback={this.addSeatCallback}
                    removeSeatCallback={this.removeSeatCallback}
                    rows={this.state.mostrandoAsientos}
                    maxReservableSeats={this.state.amount}
                    alpha
                    loading={this.state.loading}
                    tooltipProps={{ multiline: true }} 
                />
                <div style={{ marginLeft: 20, width: "100%", position: 'absolute', bottom: 30 }}>
                    <Grid container alignItems="center" justify="center" spacing={2}>
                                                <Grid item>
                                                        <Grid container alignItems="center" justify="center">
                                                                <Grid item>
                                                                        <StopRoundedIcon style={{ color: "#4FC3F7" }} fontSize="large" />
                                                                    </Grid>
                                                                <Grid item>
                                                                        <Typography variant="caption" display="inline">
                                                                                Asientos disponibles.
                                    </Typography>
                                                                    </Grid>
                                                            </Grid>
                                                    </Grid>
                                                <Grid item>
                                                        <Grid container alignItems="center" justify="center">
                                                                <Grid item>
                                                                        <StopRoundedIcon style={{ color: "#757575" }} fontSize="large" />
                                                                    </Grid>
                                                                <Grid item>
                                                                        <Typography variant="caption" display="inline">
                                                                                Asientos ocupados.
                                </Typography>
                                                                    </Grid>
                                                            </Grid>
                                                    </Grid>
                                            </Grid>
                    <Typography variant="caption">
                        Mapa de asientos: Muevete por la pantalla para ver todos los
                        lugares disponibles
                    </Typography>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (reducers) => {
    return reducers.ticketsReducer
}

export default connect(mapStateToProps, ticketsActions)(App)