import React from "react";
import {
  CssBaseline,
  Grid,
  Typography,
  TextField,
  AppBar,
  Toolbar,
  Stepper,
  Step,
  StepLabel,
  Paper,
  Button,
  makeStyles,
  Hidden,
} from "@material-ui/core";
import useWindowSize from "../WindowSize";
import Spinner from "../General/Spinner";
import Fatal from "../General/Fatal";
import { ReactComponent as SvgIcon } from "../../icons/ticket.svg";
import { useSelector, useDispatch } from "react-redux";
import * as usersActions from "../../actions/users.Actions";
import { useHistory } from "react-router-dom";
import { store } from "react-notifications-component";
import RN from "../Notification";
import MovieBanner from "./MovieBanner";
function Copyright({ classes }) {
  return (
    <Typography
      variant="body2"
      color="textSecondary"
      align="center"
      className={classes.copy}
    >
      {"Copyright © "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
  },
  icon: {
    height: 48,
  },
  formControl: {
    width: "100%",
  },
  layout: {
    //width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      //width: 600,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end",
  },
  button: {
    fontFamily: "Gotham-Book",
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
  marginQr: {
    margin: theme.spacing(3),
  },
  copy: {
    color: "#fff",
    margin: theme.spacing(3),
  },
  layoutContent: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  ajl: {
    fontFamily: "Gotham-Ultra",
    marginLeft: theme.spacing(2),
  },
  event: {
    fontFamily: "Gotham-Medium",
  },
  buttonsDown: {
    display: "flex",
    justifyContent: "center",
    marginBottom: theme.spacing(2),
  },
  image: {
    width: 100,
  },
}));

const steps = [
  "Selecciona tus boletos",
  "Datos del cliente",
  "Reserva",
  "Revise su compra",
];
const movies = [
  {
    _id: "5d8bd201a56968003607e6a5",
    title: "avengers: endgame",
    image:
      "https://image.tmdb.org/t/p/original/7RyHsO4yDXtBv1zUU3mTpHeQ0d5.jpg",
    genre: "adventure",
    language: "english",
    duration: 120,
    description: "spiderman the awesome movie",
    director: "chris evans",
    cast: "joe russo, anthony russo",
    __v: 0,
    endDate: "2022-03-31T16:18:00.000Z",
    releaseDate: "2019-11-04T17:18:00.000Z",
  },
  {
    _id: "5da1aa0e211c0c98e849a7cf",
    title: "joker",
    image:
      "https://image.tmdb.org/t/p/original/n6bUvigpRFqSwmPp1m2YADdbRBc.jpg",
    genre: "thriller,action,adventure,drama",
    language: "english",
    duration: 122,
    description:
      "during the 1980s, a failed stand-up comedian is driven insane and turns to a life of crime and chaos in gotham city while becoming an infamous psychopathic crime figure.",
    director: "todd phillips",
    cast: "joaquin phoenix, robert de niro, zazie beetz, frances conroy",
    __v: 0,
    endDate: "2022-05-29T11:03:00.000Z",
    releaseDate: "2019-11-09T12:03:00.000Z",
  },
  {
    _id: "5da1aa6c211c0c98e849a7d0",
    title: "spider-man: into the spider-verse",
    image:
      "https://image.tmdb.org/t/p/original/uUiId6cG32JSRI6RyBQSvQtLjz2.jpg",
    genre: "comedy",
    language: "english",
    duration: 117,
    description:
      'miles morales is juggling his life between being a high school student and being a spider-man. when wilson "kingpin" fisk uses a super collider, others from across the spider-verse are transported to this dimension.',
    director: "bob persichetti, peter ramsey, rodney rothman",
    cast: "shameik moore, jake johnson, hailee steinfeld",
    __v: 0,
    endDate: "2022-04-30T11:08:00.000Z",
    releaseDate: "2021-04-30T11:08:00.000Z",
  },
  {
    _id: "5db0afc35c747983076a3a04",
    title: "the lion king",
    image:
      "https://image.tmdb.org/t/p/original/nRXO2SnOA75OsWhNhXstHB8ZmI3.jpg",
    genre: "adventure",
    language: "english",
    duration: 120,
    description:
      "simba idolises his father, king mufasa, and takes to heart his own royal destiny. but not everyone in the kingdom celebrates the new cub's arrival. scar, mufasa's brother—and former heir to the throne—has plans of his own. the battle for pride rock is ravaged with betrayal, tragedy and drama, ultimately resulting in simba's exile. with help from a curious pair of newfound friends, simba will have to figure out how to grow up and take back what is rightfully his.",
    director: "jon favreau",
    cast: "donald glover, beyoncé knowles, james earl jones",
    __v: 0,
    endDate: "2022-05-31T11:07:00.000Z",
    releaseDate: "2019-11-09T12:07:00.000Z",
  },
  {
    _id: "5db0b0465c747983076a3a07",
    title: "john wick: chapter 3 - parabellum",
    image:
      "https://image.tmdb.org/t/p/original/stemLQMLDrlpfIlZ5OjllOPT8QX.jpg",
    genre: "action,adventure",
    language: "english",
    duration: 131,
    description:
      "super-assassin john wick returns with a $14 million price tag on his head and an army of bounty-hunting killers on his trail. after killing a member of the shadowy international assassin’s guild, the high table, john wick is excommunicado, but the world’s most ruthless hit men and women await his every turn...",
    director: "chad stahelski",
    cast: "keanu reeves, halle berry, ian mcssane",
    releaseDate: "2021-08-31T19:54:00.000Z",
    endDate: "2021-02-28T20:54:00.000Z",
    __v: 0,
  },
  {
    _id: "5db0b12b5c747983076a3a0b",
    title: "shazam",
    image:
      "https://image.tmdb.org/t/p/w370_and_h556_bestv2/xnopI5Xtky18MPhK40cZAGAOVeV.jpg",
    genre: "fantasy",
    language: "english",
    duration: 131,
    description:
      "a boy is given the ability to become an adult superhero in times of need with a single magic word.",
    director: "david f. sandberg",
    cast: "asher angel, mark strong, zachary levi",
    __v: 0,
    endDate: "2022-03-27T16:38:00.000Z",
    releaseDate: "2021-07-31T16:38:00.000Z",
  },
  {
    _id: "5db0b0995c747983076a3a08",
    title: "maleficent: mistress of evil",
    image:
      "https://image.tmdb.org/t/p/original/skvI4rYFrKXS73BJxWGH54Omlvv.jpg",
    genre: "fantasy",
    language: "english",
    duration: 131,
    description:
      "maleficent and her goddaughter aurora begin to question the complex family ties that bind them as they are pulled in different directions by impending nuptials, unexpected allies, and dark new forces at play.",
    director: "joachim rønning",
    cast: "angelina jolie, elle fanning,  sam riley",
    __v: 0,
    endDate: "2022-11-30T17:19:00.000Z",
    releaseDate: "2019-11-06T17:19:00.000Z",
  },
  {
    _id: "5db0b0e85c747983076a3a0a",
    title: "aladdin",
    image:
      "https://image.tmdb.org/t/p/w370_and_h556_bestv2/3iYQTLGoy7QnjcUYRJy4YrAgGvp.jpg",
    genre: "fantasy",
    language: "english",
    duration: 131,
    description:
      "a kindhearted street urchin named aladdin embarks on a magical adventure after finding a lamp that releases a wisecracking genie while a power-hungry grand vizier vies for the same lamp that has the power to make their deepest wishes come true.",
    director: "guy ritchie",
    cast: "will smith, naomi scott, mena massoud",
    releaseDate: "2021-08-31T19:54:00.000Z",
    endDate: "2022-10-23T19:54:15.155Z",
    __v: 0,
  },
  {
    _id: "5dc6aaa054ec69380c88a0fc",
    title: "watchmen",
    image:
      "https://image.tmdb.org/t/p/original/gFeaXBnOO14aOQhMQrr5tbyhMTw.jpg",
    genre: "drama",
    language: "english",
    duration: 60,
    description:
      "set in an alternate history where “superheroes” are treated as outlaws, “watchmen” embraces the nostalgia of the original groundbreaking graphic novel while attempting to break new ground of its own.",
    director: "damon lindelof",
    cast: "regina king, jeremy irons, tim blake nelson",
    releaseDate: "2019-11-09T11:59:00.000Z",
    endDate: "2022-07-31T10:59:00.000Z",
    __v: 0,
  },
  {
    _id: "5dc6abd654ec69380c88a0ff",
    title: "zombieland: double tap",
    image: "https://image.tmdb.org/t/p/original/jCCdt0e8Xe9ttvevD4S3TSMNdH.jpg",
    genre: "action",
    language: "english",
    duration: 120,
    description:
      "columbus, tallahassee, wichita, and little rock move to the american heartland as they face off against evolved zombies, fellow survivors, and the growing pains of the snarky makeshift family.",
    director: "ruben fleischer",
    cast: "woody harrelson, jesse eisenberg, emma stone",
    releaseDate: "2019-11-09T12:05:00.000Z",
    endDate: "2022-05-30T11:05:00.000Z",
    __v: 0,
  },
  {
    _id: "5dc6ac1754ec69380c88a100",
    title: "the king",
    image:
      "https://image.tmdb.org/t/p/original/r0AWsZ9dBvC2No3kND9nxv3iRbb.jpg",
    genre: "drama",
    language: "english",
    duration: 120,
    description:
      "england, 15th century. hal, a capricious prince who lives among the populace far from court, is forced by circumstances to reluctantly accept the throne and become henry v.",
    director: "david michôd",
    cast: "sean harris, joel edgerton, tomothee chalamet",
    releaseDate: "2019-11-09T12:05:00.000Z",
    endDate: "2022-08-31T11:05:00.000Z",
    __v: 0,
  },
  {
    _id: "5dc6ad0c54ec69380c88a101",
    title: "terminator: dark fate",
    image:
      "https://image.tmdb.org/t/p/original/rtf4vjjLZLalpOzDUi0Qd2GTUqq.jpg",
    genre: "adventure",
    language: "english",
    duration: 128,
    description:
      "more than two decades have passed since sarah connor prevented judgment day, changed the future, and re-wrote the fate of the human race. dani ramos is living a simple life in mexico city with her brother and father when a highly advanced and deadly new terminator – a rev-9 – travels back through time to hunt and kill her. dani's survival depends on her joining forces with two warriors: grace, an enhanced super-soldier from the future, and a battle-hardened sarah connor. as the rev-9 ruthlessly destroys everything and everyone in its path on the hunt for dani, the three are led to a t-800 from sarah’s past that may be their last best hope.",
    director: "tim miller",
    cast: "arnold schwarzenegger, natalia reyes",
    releaseDate: "2019-12-31T12:10:00.000Z",
    endDate: "2022-05-31T11:10:00.000Z",
    __v: 0,
  },
  {
    _id: "5dc6ad8354ec69380c88a102",
    title: "black and blue",
    image:
      "https://image.tmdb.org/t/p/original/zBAoNL50oFRCAJvEEQEKD8M48pV.jpg",
    genre: "drama",
    language: "english",
    duration: 128,
    description:
      "exposure follows a rookie detroit african-american female cop who stumbles upon corrupt officers who are murdering a drug dealer, an incident captured by her body cam. they pursue her through the night in an attempt to destroy the footage, but to make matters worse, they've tipped off a criminal gang that she's responsible for the dealer's death.",
    director: "deon taylor",
    cast: "frank grillo,  naomie harris",
    releaseDate: "2021-04-30T11:10:00.000Z",
    endDate: "2022-09-30T11:10:00.000Z",
    __v: 0,
  },
];

export default function UserForm() {
  const classes = useStyles();
  const userSelector = useSelector((store) => store.usersReducer);
  const dispatch = useDispatch();
  const history = useHistory();
  const size = useWindowSize();

  React.useEffect(() => {
    const { user } = userSelector;
    //console.log(user);
    if (Object.keys(user).length !== 0) {
      setState({
        ...state,
        name: user.name,
        lastName: user.lastName,
        email: user.email,
        phone: user.phone,
      });
    }
  }, []);
  const [state, setState] = React.useState({
    activeStep: 1,
    name: "",
    lastName: "",
    email: "",
    phone: "",
    validatedEmail: false,
    id: "",
  });
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const handleChange = (prop) => (event) => {
    setState({ ...state, [prop]: event.target.value });
  };
  const handleNext = () => {
    const { user, loading, error } = userSelector;
    if (loading) {
      return <Spinner />;
    }
    if (error) {
      return <Fatal mensaje={error} />;
    }
    if (user.length !== 0) {
      if (
        state.name === "" ||
        state.lastName === "" ||
        state.email === "" ||
        state.phone === ""
      ) {
        store.addNotification({
          content: (
            <RN
              title="Campos requeridos vacíos"
              message="Por favor ingresa tus datos para continuar."
              type="Error"
            />
          ),
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 1000,
            onScreen: false,
          },
        });
      } else if (!state.email.match(re)) {
        store.addNotification({
          content: (
            <RN
              title="Email inválido"
              message="Recuerda que una vez finalizado tu compra se te enviarán tus entradas al evento vía correo electrónico."
              type="Error"
            />
          ),
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 3000,
            onScreen: false,
          },
        });
      } else {
        dispatch(usersActions.enviar(state));
        history.push("/booking");
      }
    }
  };

  const handleBack = () => {
    history.push("/");
  };
  const sizeEscenario = () => {
    if (size.height > size.width) {
      return "auto";
    }
    return "75%";
  };

  const activeStepClass = () => {
    if (state.activeStep !== 1) {
      return 600;
    } else {
      return sizeEscenario();
    }
  };
  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="absolute" color="default" className={classes.appBar}>
        <Toolbar>
          <SvgIcon style={{ fill: "#d50000", width: 48, height: 48 }} />
          <Typography
            variant="h6"
            color="inherit"
            noWrap
            className={classes.ajl}
          >
            CINEMA APP
          </Typography>
        </Toolbar>
      </AppBar>
      <MovieBanner/>
      </React.Fragment>
  );
}
