import React from "react";
import { Rating } from "@material-ui/lab";
import {
  Box,
  Typography,
  Button,
  makeStyles,
  withStyles,
} from "@material-ui/core";
import { textTruncate } from "../../utils/utils";
import { Link } from "react-router-dom";
import ArrowRightAlt from "@material-ui/icons/ArrowRightAlt";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import styles from "./styles";
import { useSelector } from "react-redux";

import { useHistory } from "react-router-dom";
const useStyles = makeStyles(styles);

const StyledRating = withStyles({
  iconFilled: {
    color: "#fff",
  },
  iconEmpty: {
    color: "#fff",
  },
})(Rating);
function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
}
function MovieBanner(props) {
  const classes = useStyles(props);
  const history = useHistory();
  const ticketsSelector = useSelector((store) => store.ticketsReducer);
  const { select_movie } = ticketsSelector;

  if (isEmpty(select_movie)) {
    history.push("/");
    return <div></div>;
  }

  return (
    <div className={classes.movieHero}>
      <div className={classes.infoSection}>
        <header className={classes.movieHeader}>
          <Box mb={3} display="flex" alignItems="center" flexWrap="wrap">
            {select_movie.genre.split(",").map((genre, index) => (
              <Typography
                key={`${genre}-${index}`}
                className={classes.tag}
                variant="body1"
                color="inherit"
              >
                {genre}
              </Typography>
            ))}

            <StyledRating
              value={4}
              readOnly
              size="small"
              emptyIcon={<StarBorderIcon fontSize="inherit" />}
            />
          </Box>
          <Typography
            className={classes.movieTitle}
            variant="h1"
            color="inherit"
          >
            {select_movie.title}
          </Typography>
          <Typography
            className={classes.descriptionText}
            variant="body1"
            color="inherit"
          >
            {textTruncate(select_movie.description, 450)}
          </Typography>
          <Typography className={classes.director} variant="h4" color="inherit">
            By: {select_movie.director}
          </Typography>
          <Typography
            className={classes.duration}
            variant="body1"
            color="inherit"
          >
            {select_movie.duration} min.
          </Typography>
          <Typography className={classes.genre} variant="body1" color="inherit">
            {select_movie.genre}
          </Typography>
        </header>
      </div>
      <div
        className={classes.blurBackground}
        style={{
          backgroundImage: `url(${select_movie.image})`,
        }}
      />
      <div className={classes.movieActions}>
        <Link to={`/tickets`} style={{ textDecoration: "none" }}>
          <Button variant="contained" className={classes.button}>
            Comprar Boletos
            <ArrowRightAlt className={classes.buttonIcon} />
          </Button>
        </Link>
      </div>
    </div>
  );
}

export default MovieBanner;
