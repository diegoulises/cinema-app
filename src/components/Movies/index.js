import React from "react";
import {
  CssBaseline,
  Grid,
  Typography,
  TextField,
  AppBar,
  Toolbar,
  Stepper,
  Step,
  StepLabel,
  Paper,
  Button,
  makeStyles,
  Hidden,
} from "@material-ui/core";
import useWindowSize from "../WindowSize";
import Spinner from "../General/Spinner";
import Fatal from "../General/Fatal";
import { ReactComponent as SvgIcon } from "../../icons/ticket.svg";
import { useSelector, useDispatch } from "react-redux";
import * as usersActions from "../../actions/users.Actions";
import { useHistory } from "react-router-dom";
import { store } from "react-notifications-component";
import RN from "../Notification";
import ResponsiveMovieCard from "../ResponsiveMovieCard/ResponsiveMovieCard";
function Copyright({ classes }) {
  return (
    <Typography
      variant="body2"
      color="textSecondary"
      align="center"
      className={classes.copy}
    >
      {"Copyright © "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
  },
  icon: {
    height: 48,
  },
  formControl: {
    width: "100%",
  },
  layout: {
    //width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      //width: 600,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end",
  },
  button: {
    fontFamily: "Gotham-Book",
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
  marginQr: {
    margin: theme.spacing(3),
  },
  copy: {
    color: "#fff",
    margin: theme.spacing(3),
  },
  layoutContent: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  ajl: {
    fontFamily: "Gotham-Ultra",
    marginLeft: theme.spacing(2),
  },
  event: {
    fontFamily: "Gotham-Medium",
  },
  buttonsDown: {
    display: "flex",
    justifyContent: "center",
    marginBottom: theme.spacing(2),
  },
  image: {
    width: 100,
  },
}));

const steps = [
  "Selecciona tus boletos",
  "Datos del cliente",
  "Reserva",
  "Revise su compra",
];
const movies = [
  {
    _id: "5d8bd201a56968003607e6a5",
    title: "Avengers: endgame",
    image:
      "https://image.tmdb.org/t/p/original/7RyHsO4yDXtBv1zUU3mTpHeQ0d5.jpg",
    genre: "Aventura",
    language: "english",
    duration: 120,
    description:
      "Los Vengadores restantes deben encontrar una manera de recuperar a sus aliados para un enfrentamiento épico con Thanos, el malvado que diezmó el planeta y el universo.",
    director: "Joe Russo, Anthony Russo",
    cast:
      "Robert Downey Jr., Scarlett Johansson (Viuda Negra), Chris Hemsworth (Thor), Chris Evans (Capitán América), Mark Ruffalo (Bruce Banner)",
    __v: 0,
    endDate: "2022-03-31T16:18:00.000Z",
    releaseDate: "2019-11-04T17:18:00.000Z",
  },
  {
    _id: "5da1aa0e211c0c98e849a7cf",
    title: "Joker",
    image:
      "https://image.tmdb.org/t/p/original/n6bUvigpRFqSwmPp1m2YADdbRBc.jpg",
    genre: "Suspenso, Acción, Aventura, Drama",
    language: "english",
    duration: 122,
    description:
      "Arthur Fleck adora hacer reír a la gente, pero su carrera como comediante es un fracaso. El repudio social, la marginación y una serie de trágicos acontecimientos lo conducen por el sendero de la locura y, finalmente, cae en el mundo del crimen.",
    director: "Todd Phillips",
    cast: "Joaquin Phoenix, Robert de Niro, Zazie Beetz, Frances Conroy",
    __v: 0,
    endDate: "2022-05-29T11:03:00.000Z",
    releaseDate: "2019-11-09T12:03:00.000Z",
  },
  {
    _id: "5da1aa6c211c0c98e849a7d0",
    title: "Spider-Man: Into The Spider-Verse",
    image:
      "https://image.tmdb.org/t/p/original/uUiId6cG32JSRI6RyBQSvQtLjz2.jpg",
    genre: "Comedia",
    language: "english",
    duration: 117,
    description:
      "Luego de ser mordido por una araña radioactiva, el joven Miles Morales desarrolla misteriosos poderes que lo transforman en el Hombre Araña. Ahora deberá usar sus nuevas habilidades ante el malvado Kingpin, un enorme demente que puede abrir portales hacia otros universos.",
    director: "Bob Persichetti, Peter Ramsey, Rodney Rothman",
    cast: "Shameik Moore, Jake Johnson, Hailee Steinfeld",
    __v: 0,
    endDate: "2022-04-30T11:08:00.000Z",
    releaseDate: "2021-04-30T11:08:00.000Z",
  },
  {
    _id: "5db0afc35c747983076a3a04",
    title: "The Lion King",
    image:
      "https://image.tmdb.org/t/p/original/nRXO2SnOA75OsWhNhXstHB8ZmI3.jpg",
    genre: "Aventura",
    language: "english",
    duration: 120,
    description:
      "Tras el asesinato de su padre, Simba, un joven león es apartado su reino y tendrá que descubrir con ayuda de amigos de la sabana africana el significado de la responsabilidad y la valentía. Más tarde volverá para recuperar el control de su reino.",
    director: "Jon Favreau",
    cast: "Donald Glover, Beyoncé Knowles, James Earl Jones",
    __v: 0,
    endDate: "2022-05-31T11:07:00.000Z",
    releaseDate: "2019-11-09T12:07:00.000Z",
  },
  {
    _id: "5db0b0465c747983076a3a07",
    title: "John Wick: Chapter 3 - Parabellum",
    image:
      "https://image.tmdb.org/t/p/original/stemLQMLDrlpfIlZ5OjllOPT8QX.jpg",
    genre: "Acción, Aventura",
    language: "english",
    duration: 131,
    description:
      "John Wick regresa de nuevo pero con una recompensa sobre su cabeza que persigue unos mercenarios. Tras asesinar a uno de los miembros de su gremio, Wick es expulsado y se convierte en el foco de atención de todos los sicarios de la organización.",
    director: "Chad Stahelski",
    cast: "Keanu Reeves, Halle Berry, Ian Mcssane",
    releaseDate: "2021-08-31T19:54:00.000Z",
    endDate: "2021-02-28T20:54:00.000Z",
    __v: 0,
  },
  {
    _id: "5db0b12b5c747983076a3a0b",
    title: "Shazam",
    image:
      "https://image.tmdb.org/t/p/w370_and_h556_bestv2/xnopI5Xtky18MPhK40cZAGAOVeV.jpg",
    genre: "Fantasía",
    language: "english",
    duration: 131,
    description:
      "Billy Batson, un astuto joven de 14 años, se transforma en el superhéroe Shazam, pero sus poderes son puestos a prueba cuando se enfrenta al mal.",
    director: "David F. Sandberg",
    cast: "Asher Angel, Mark Strong, Zachary Levi",
    __v: 0,
    endDate: "2022-03-27T16:38:00.000Z",
    releaseDate: "2021-07-31T16:38:00.000Z",
  },
  {
    _id: "5db0b0995c747983076a3a08",
    title: "Maleficent: Mistress of Evil",
    image:
      "https://image.tmdb.org/t/p/original/skvI4rYFrKXS73BJxWGH54Omlvv.jpg",
    genre: "Fantasía",
    language: "english",
    duration: 131,
    description:
      "Maléfica acude a un viejo castillo para asistir a la boda de Aurora y el príncipe Phillip. Mientras está allí, descubre que la futura suegra de Aurora ha urdido un plan con el que pretende destruir la tierra de las hadas. ",
    director: "Joachim Rønning",
    cast: "Angelina Jolie, Elle Fanning,  Sam Riley",
    __v: 0,
    endDate: "2022-11-30T17:19:00.000Z",
    releaseDate: "2019-11-06T17:19:00.000Z",
  },
  {
    _id: "5db0b0e85c747983076a3a0a",
    title: "Aladdin",
    image:
      "https://image.tmdb.org/t/p/w370_and_h556_bestv2/3iYQTLGoy7QnjcUYRJy4YrAgGvp.jpg",
    genre: "Fantasía",
    language: "english",
    duration: 131,
    description:
      "Aladdin es un ladronzuelo que se enamora de la hija del Sultán, la princesa Jasmine. Para poder conquistarla aceptará un desafío de Jafar.",
    director: "Guy Ritchie",
    cast: "Will Smith, Naomi Scott, Mena Massoud",
    releaseDate: "2021-08-31T19:54:00.000Z",
    endDate: "2022-10-23T19:54:15.155Z",
    __v: 0,
  },
  {
    _id: "5dc6aaa054ec69380c88a0fc",
    title: "Watchmen",
    image:
      "https://image.tmdb.org/t/p/original/gFeaXBnOO14aOQhMQrr5tbyhMTw.jpg",
    genre: "Drama",
    language: "english",
    duration: 60,
    description:
      "En el marco de una historia paralela, los superhéroes son tratados como delincuentes.",
    director: "Damon Lindelof",
    cast: "Regina King, Jeremy Irons, Tim Blake Nelson",
    releaseDate: "2019-11-09T11:59:00.000Z",
    endDate: "2022-07-31T10:59:00.000Z",
    __v: 0,
  },
  {
    _id: "5dc6abd654ec69380c88a0ff",
    title: "Zombieland: Double Tap",
    image: "https://image.tmdb.org/t/p/original/jCCdt0e8Xe9ttvevD4S3TSMNdH.jpg",
    genre: "Acción",
    language: "english",
    duration: 120,
    description:
      "Los cazadores de zombis viajan desde la Casa Blanca hasta el corazón de los Estados Unidos, donde tendrán que defenderse de nuevas clases de muertos vivientes que han evolucionado. ",
    director: "Ruben Fleischer",
    cast: "Woody Harrelson, Jesse Eisenberg, Emma Stone",
    releaseDate: "2019-11-09T12:05:00.000Z",
    endDate: "2022-05-30T11:05:00.000Z",
    __v: 0,
  },
  {
    _id: "5dc6ac1754ec69380c88a100",
    title: "The King",
    image:
      "https://image.tmdb.org/t/p/original/r0AWsZ9dBvC2No3kND9nxv3iRbb.jpg",
    genre: "Drama",
    language: "english",
    duration: 120,
    description:
      "Hal, un príncipe caprichoso y sin interés por ejercer su derecho al trono de Inglaterra, ha abandonado las responsabilidades reales para vivir en libertad entre la plebe.",
    director: "David Michôd",
    cast: "Sean Harris, Joel Edgerton, Tomothee Chalamet",
    releaseDate: "2019-11-09T12:05:00.000Z",
    endDate: "2022-08-31T11:05:00.000Z",
    __v: 0,
  },
  {
    _id: "5dc6ad8354ec69380c88a102",
    title: "Black and Blue",
    image:
      "https://image.tmdb.org/t/p/original/zBAoNL50oFRCAJvEEQEKD8M48pV.jpg",
    genre: "Drama",
    language: "english",
    duration: 128,
    description:
      "Una policía novata de Nueva Orleans graba a sus compañeros asesinando a un narcotraficante con la cámara que lleva acoplada al cuerpo.",
    director: "Deon Taylor",
    cast: "Frank Grillo,  Naomie Harris",
    releaseDate: "2021-04-30T11:10:00.000Z",
    endDate: "2022-09-30T11:10:00.000Z",
    __v: 0,
  },
];

export default function UserForm() {
  const classes = useStyles();
  const userSelector = useSelector((store) => store.usersReducer);
  const dispatch = useDispatch();
  const history = useHistory();
  const size = useWindowSize();

  React.useEffect(() => {
    const { user } = userSelector;
    //console.log(user);
    if (Object.keys(user).length !== 0) {
      setState({
        ...state,
        name: user.name,
        lastName: user.lastName,
        email: user.email,
        phone: user.phone,
      });
    }
  }, []);
  const [state, setState] = React.useState({
    activeStep: 1,
    name: "",
    lastName: "",
    email: "",
    phone: "",
    validatedEmail: false,
    id: "",
  });
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const handleChange = (prop) => (event) => {
    setState({ ...state, [prop]: event.target.value });
  };
  const handleNext = () => {
    const { user, loading, error } = userSelector;
    if (loading) {
      return <Spinner />;
    }
    if (error) {
      return <Fatal mensaje={error} />;
    }
    if (user.length !== 0) {
      if (
        state.name === "" ||
        state.lastName === "" ||
        state.email === "" ||
        state.phone === ""
      ) {
        store.addNotification({
          content: (
            <RN
              title="Campos requeridos vacíos"
              message="Por favor ingresa tus datos para continuar."
              type="Error"
            />
          ),
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 1000,
            onScreen: false,
          },
        });
      } else if (!state.email.match(re)) {
        store.addNotification({
          content: (
            <RN
              title="Email inválido"
              message="Recuerda que una vez finalizado tu compra se te enviarán tus entradas al evento vía correo electrónico."
              type="Error"
            />
          ),
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 3000,
            onScreen: false,
          },
        });
      } else {
        dispatch(usersActions.enviar(state));
        history.push("/booking");
      }
    }
  };

  const handleBack = () => {
    history.push("/");
  };
  const sizeEscenario = () => {
    if (size.height > size.width) {
      return "auto";
    }
    return "75%";
  };

  const activeStepClass = () => {
    if (state.activeStep !== 1) {
      return 600;
    } else {
      return sizeEscenario();
    }
  };
  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="absolute" color="default" className={classes.appBar}>
        <Toolbar>
          <SvgIcon style={{ fill: "#d50000", width: 48, height: 48 }} />
          <Typography
            variant="h6"
            color="inherit"
            noWrap
            className={classes.ajl}
          >
            CINEMA APP
          </Typography>
        </Toolbar>
      </AppBar>
      <Grid container alignItems="center" justify="center" direction="row">
        <Grid item>
          <Hidden smDown>
            <img className={classes.image} src="/ticketLogo.png" alt="CINEMA" />
          </Hidden>
        </Grid>
        <Grid item xs={12}>
          <main>
            <Grid
              container
              align="center"
              justify="center"
              direction="row"
              className={classes.layoutContent}
            >
              <Grid
                container
                item
                xs={12}
                direction="column"
                alignItems="center"
                justify="center"
                spacing={0}
              >
                {movies.map((movie) => (
                  <Grid
                    key={movie._id}
                    item
                    xs={12}
                    className={classes.fullWidth}
                  >
                    <ResponsiveMovieCard movie={movie} />
                  </Grid>
                ))}
              </Grid>
              <Copyright classes={classes} />
            </Grid>
          </main>
        </Grid>

        <Grid item>
          <Hidden smDown>
            <img className={classes.image} src="/ticketLogo.png" alt="APP" />
          </Hidden>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
