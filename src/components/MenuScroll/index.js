import React, { Component } from "react";
import ScrollMenu from "react-horizontal-scrolling-menu";
import ArrowBackIcon from '@material-ui/icons/ArrowBack'; 
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import "./styles.css";


const MenuItem = ({ text, selected }) => {
  return text;
};


export const Menu = list =>
  list.map((item, index) => {
    const { ticket } = item;

    return <MenuItem text={ticket} key={index} />;
  });


export const ArrowLeft = <ArrowBackIcon/>;
export const ArrowRight = <ArrowForwardIcon/>;

class MenuScroll extends Component {
  state = {
    alignCenter: true,
    clickWhenDrag: false,
    dragging: true,
    hideArrows: true,
    hideSingleArrow: true,
    itemsCount: this.props.list.length,
    selected: 0,
    translate: 0,
    transition: 0.9,
    wheel: true
  };

  constructor(props) {
    super(props);
    this.menu = null;
    this.menuItems = Menu(props.list.slice(0, props.list.length), this.state.selected);
  }

  onUpdate = ({ translate }) => {
    //console.log(`onUpdate: translate: ${translate}`);
    this.setState({ translate });
  };

  onSelect = key => {
    //console.log(`onSelect: ${key}`);
    this.setState({ selected: key });
  };

  componentDidUpdate(prevProps, prevState) {
    const { alignCenter } = prevState;
    const { alignCenter: alignCenterNew } = this.state;
    if (alignCenter !== alignCenterNew) {
      this.menu.setInitial();
    }
  }

  setItemsCount = ev => {
    const { itemsCount = this.props.list.length, selected } = this.state;
    const val = +ev.target.value;
    const itemsCountNew =
      !isNaN(val) && val <= this.props.list.length && val >= 0
        ? +ev.target.value
        : this.props.list.length;
    const itemsCountChanged = itemsCount !== itemsCountNew;

    if (itemsCountChanged) {
      this.menuItems = Menu(this.props.list.slice(0, itemsCountNew), selected);
      this.setState({
        itemsCount: itemsCountNew
      });
    }
  };

  setSelected = ev => {
    const { value } = ev.target;
    this.setState({ selected: String(value) });
  };



  render() {
    const {
      clickWhenDrag,
      hideArrows,
      dragging,
      hideSingleArrow,
      selected,
      translate,
      transition,
      wheel
    } = this.state;

    const menu = this.menuItems;
    return (
      <div className="App">
        <ScrollMenu
          ref={el => (this.menu = el)}
          data={menu}
          arrowLeft={ArrowLeft}
          arrowRight={ArrowRight}
          hideArrows={hideArrows}
          hideSingleArrow={hideSingleArrow}
          transition={+transition}
          onUpdate={this.onUpdate}
          onSelect={this.onSelect}
          selected={selected}
          translate={translate}
          alignCenter={true}
          dragging={dragging}
          clickWhenDrag={clickWhenDrag}
          wheel={wheel}
        />
        <hr />
      </div>
    );
  }
}

export default MenuScroll;
