import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import LocalAtmRoundedIcon from "@material-ui/icons/LocalAtmRounded";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import axios from 'axios'
import { useHistory, withRouter } from 'react-router-dom'
import { PRODUCTION } from "../../../constants/StringsAPI";

const useStyles = makeStyles(theme => ({
  button: {
    backgroundColor: "#fafafa",
    color: "#212121",
    margin: theme.spacing(5),
    paddingLeft: theme.spacing(6),
    paddingRight: theme.spacing(6),
    fontFamily: "Gotham-Medium"
  }
}));

function CashPayment(props) {
  const classes = useStyles();
  const history = useHistory();
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const getLenghtPlus = type => {
    let size = 0
    for (var index in props.seats) {
      const item = props.seats[index]
      if (item.idType == type) {
        if(item.isPlus){
          size = size + 1
        }
        
      }
    }
    return size
  }
  const getLenght = type => {
    let size = 0
    for (var index in props.seats) {
      const item = props.seats[index]
      if (item.idType == type) {
        if (item.isPlus) {
          
        }else{
          size = size + 1
        }
      }
    }
    return size
  }

  const getAsientosPlus = seats => {
    var asientosPlus = [];
    seats.map((item, index) => {
      if (item.idType === 4 && item.isPlus){
        asientosPlus.push(item.seatNumber)
      }
    })
    
    return asientosPlus;
  }

  const handleAccept = () => {
    const general = props.asientosGeneral
    const generalSize = getLenght(1)
    const preferencial = props.asientosPreferencial
    const preferencialSize = getLenght(2)
    const vip = props.asientosVip
    const vipSize = getLenght(3)
    const priority = props.asientosPriority
    const prioritySize = getLenght(4)
    const priorityPlus = getAsientosPlus(props.seats)
    const priorityPlusSize = getLenghtPlus(4)
    const amount = props.amount
    const user = props.user
    const email = user.email
    const { history } = props;
    const self = props.state
    var id = '_' + Math.random().toString(36);
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < 13; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    id = result;
    const withInvoice = self.invoice
    const username = localStorage.getItem("username");
    
    if (true) {
      var formData = new FormData()
      formData.append('idTransaction', result)
      formData.append('paymentAmount', amount)
      formData.append('email', email)
      formData.append('method', 'cash')
      if (withInvoice) {
        formData.append('requiresInvoice', true)
      }
      formData.append('user', username)
      axios({
        method: 'post',
        url: `${PRODUCTION}/api/transactions/insert.php`,
        data: formData,
        config: { headers: { 'Content-Type': 'multipart/form-data' } }
      })
        .then(function (response) {
          if (response.data.success) {
          }
        })
        .catch(function (response) {
        });
      setTimeout(() => {
        var indexCustomUser = 0;

        if (typeof general !== 'undefined') {
          for (var index = 0; index < generalSize; index++) {
            let customUser = self
            let customName = customUser.name[indexCustomUser]
            let customEmail = customUser.email[indexCustomUser]
            let customPhone = customUser.phone[indexCustomUser]
            var formData = new FormData()
            formData.append('seatNumber', general[index])
            formData.append('idType', 1)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/asientos/ocupar.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {
                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            formData = new FormData()
            formData.append('idTransaction', id)
            formData.append('seatNumber', general[index])
            formData.append('seatType', 1)
            formData.append('ownerName', customName != '' && customName != '1' && customName != '2' && customName != '3' ? customName : `${user.name} ${user.lastName}`)
            formData.append('ownerEmail', customEmail != '' ? customEmail : user.email)
            formData.append('ownerMobile', customPhone != '' ? customPhone : user.phone)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/booking/insert.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {
                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            indexCustomUser = indexCustomUser + 1
          }
        }
        if (typeof preferencial !== 'undefined') {
          for (var index = 0; index < preferencialSize; index++) {
            let customUser = self
            let customName = customUser.name[indexCustomUser]
            let customEmail = customUser.email[indexCustomUser]
            let customPhone = customUser.phone[indexCustomUser]
            var formData = new FormData()
            formData.append('seatNumber', preferencial[index])
            formData.append('idType', 2)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/asientos/ocupar.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {
                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            formData = new FormData()
            formData.append('idTransaction', id)
            formData.append('seatNumber', preferencial[index])
            formData.append('seatType', 2)
            formData.append('ownerName', customName != '' && customName != '1' && customName != '2' && customName != '3' ? customName : `${user.name} ${user.lastName}`)
            formData.append('ownerEmail', customEmail != '' ? customEmail : user.email)
            formData.append('ownerMobile', customPhone != '' ? customPhone : user.phone)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/booking/insert.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {
                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            indexCustomUser = indexCustomUser + 1
          }
        }
        if (typeof vip !== 'undefined') {
          for (var index = 0; index < vipSize; index++) {
            let customUser = self
            let customName = customUser.name[indexCustomUser]
            let customEmail = customUser.email[indexCustomUser]
            let customPhone = customUser.phone[indexCustomUser]
            var formData = new FormData()
            formData.append('seatNumber', vip[index])
            formData.append('idType', 3)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/asientos/ocupar.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {
                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            formData = new FormData()
            formData.append('idTransaction', id)
            formData.append('seatNumber', vip[index])
            formData.append('seatType', 3)
            formData.append('ownerName', customName != '' && customName != '1' && customName != '2' && customName != '3' ? customName : `${user.name} ${user.lastName}`)
            formData.append('ownerEmail', customEmail != '' ? customEmail : user.email)
            formData.append('ownerMobile', customPhone != '' ? customPhone : user.phone)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/booking/insert.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {
                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            indexCustomUser = indexCustomUser + 1
          }
        }
        if (typeof priority !== 'undefined') {
          for (var index = 0; index < prioritySize; index++) {
            let customUser = self
            let customName = customUser.name[indexCustomUser]
            let customEmail = customUser.email[indexCustomUser]
            let customPhone = customUser.phone[indexCustomUser]
            var formData = new FormData()
            formData.append('seatNumber', priority[index])
            formData.append('idType', 4)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/asientos/ocupar.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {

                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            formData = new FormData()
            formData.append('idTransaction', id)
            formData.append('seatNumber', priority[index])
            formData.append('seatType', 4)
            formData.append('ownerName', customName != '' && customName != '1' && customName != '2' && customName != '3' ? customName : `${user.name} ${user.lastName}`)
            formData.append('ownerEmail', customEmail != '' ? customEmail : user.email)
            formData.append('ownerMobile', customPhone != '' ? customPhone : user.phone)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/booking/insert.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {
                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            indexCustomUser = indexCustomUser + 1
          }
        }
        if (typeof priorityPlus !== 'undefined') {
          for (var index = 0; index < priorityPlusSize; index++) {
            let customUser = self
            let customName = customUser.name[indexCustomUser]
            let customEmail = customUser.email[indexCustomUser]
            let customPhone = customUser.phone[indexCustomUser]
            var formData = new FormData()
            formData.append('seatNumber', priority[index])
            formData.append('idType', 4)
            formData.append('isPlus', true)
            axios({
              method: 'post',
              url: `${PRODUCTION}/api/asientos/ocupar.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {

                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            formData = new FormData()
            formData.append('idTransaction', id)
            formData.append('seatNumber', priorityPlus[index])
            formData.append('seatType', 4)
            formData.append('ownerName', customName != '' && customName != '1' && customName != '2' && customName != '3' ? customName : `${user.name} ${user.lastName}`)
            formData.append('ownerEmail', customEmail != '' ? customEmail : user.email)
            formData.append('ownerMobile', customPhone != '' ? customPhone : user.phone)
            //formData.append('isPlus', true)

            axios({
              method: 'post',
              url: `${PRODUCTION}/api/booking/insert.php`,
              data: formData,
              config: { headers: { 'Content-Type': 'multipart/form-data' } }
            })
              .then(function (response) {
                if (response.data.success) {
                }
              })
              .catch(function (response) {
                //console.log('error')
              });
            indexCustomUser = indexCustomUser + 1
          }
        }

        setTimeout(() => {
          axios({
            method: 'get',
            url: `${PRODUCTION}/api/pdf/read.php?idTransaction=${id}`,
          })
            .then(function (response) {
              //console.log("Correo enviado");
            })
            .catch(function (response) {
              //handle error
              //console.log(response);
            });
        }, 2000);
      }, 2000);
      //console.log('Pago confirmado')
    }
    history.push("/login/success")
  };
  return (
    <div>
      <Button
        variant="contained"
        disableElevation
        className={classes.button}
        style={{ borderRadius: 8 }}
        endIcon={<LocalAtmRoundedIcon />}
        onClick={handleClickOpen}
      >
        {props.text}
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Confirmación de pago"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Precione Aceptar si ha recibido el efectivo y todos los datos sean
            correctos.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={handleAccept} color="primary" autoFocus>
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default withRouter(CashPayment);
