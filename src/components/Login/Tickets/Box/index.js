import React from "react";
import posed from "react-pose";
import { makeStyles, Typography, Grid } from "@material-ui/core";
import useWindowSize from "../../../WindowSize";

const useStyles = makeStyles(theme => ({
  general: {
    backgroundColor: "#006f9b",
    width: 150,
    height: 150,
    padding: theme.spacing(3, 2),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  preferencial: {
    backgroundColor: "#bf0811",
    width: 150,
    height: 150,
    padding: theme.spacing(3, 2),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  vip: {
    backgroundColor: "#545454",
    width: 150,
    height: 150,
    padding: theme.spacing(3, 2),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  priority: {
    backgroundColor: "#FBDF23",
    width: 150,
    height: 150,
    padding: theme.spacing(3, 2),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  box: {
    background: "#ffffff",
    width: "100%"
  },
  text: {
    color: "#000000",
    [theme.breakpoints.up("lg")]: {
      fontSize: 16
    },
    [theme.breakpoints.up("md")]: {
      fontSize: 10
    },
    [theme.breakpoints.up("sm")]: {
      fontSize: 8
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: 6
    }
  },
  layout: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
}));

const Box = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 0.5
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});

const BoxGeneral = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 0.5,
    background: "#000000"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#006F9B"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});
const BoxGeneralEnable = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 0.5,
    background: "#006F9B"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#006F9B"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});

const BoxPreferencial = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 0.5,
    background: "#000000"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#bf0811"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});
const BoxPreferencialEnable = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 0.5,
    background: "#bf0811"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#bf0811"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});

const BoxVip = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 0.5,
    background: "#000000"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#607d8b"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});
const BoxVipEnable = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 0.5,
    background: "#607d8b"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#607d8b"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});

const BoxPriority = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 0.5,
    background: "#000000"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#FBDF23"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});
const BoxPriorityEnable = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 0.5,
    background: "#FBDF23"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#FBDF23"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});

const BoxPriorityPlus = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 0.5,
    background: "#000000"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#f5f5f5"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});
const BoxPriorityPlusEnable = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    //boxShadow: "0px 0px 0px rgba(0,0,0,0)",
    opacity: 0.5,
    background: "#f5f5f5"
  },
  hover: {
    scale: 1.0,
    //boxShadow: "0px 5px 10px rgba(0,0,0,0.2)",
    opacity: 1,
    background: "#f5f5f5"
  },
  press: {
    scale: 1.005,
    boxShadow: "0px 2px 5px rgba(0,0,0,0.1)"
  }
});

const BoxP = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 0.9,
    opacity: 1
  },
  hover: {
    scale: 0.9,
    opacity: 1
  },
  press: {
    scale: 0.9
  }
});

export default function Zona({ color, largo, enableColor, text }) {
  const classes = useStyles();
  const size = useWindowSize();

  const sizeEscenario = () => {
    if (size.height > size.width) {
      return (size.height / 300) * largo;
    }
    return (size.height / 300) * largo;
  };
  if (color === "#ffffff") {
    return (
      <BoxP
        className={classes.box}
        style={{ background: color, height: sizeEscenario() }}
      >
        <Grid
          container
          align="center"
          justify="center"
          direction="column"
          className={classes.layout}
        >
          <Typography className={classes.text} align="center">
            {text}
          </Typography>
        </Grid>
      </BoxP>
    );
  }

  if (color === "#006f9b") {
    if (!enableColor) {
      return (
        <BoxGeneralEnable
          className={classes.box}
          style={{ background: color, height: sizeEscenario() }}
        />
      );
    }
    return (
      <BoxGeneral
        className={classes.box}
        style={{ background: color, height: sizeEscenario() }}
      />
    );
  }

  if (color === "#bf0811") {
    if (!enableColor) {
      return (
        <BoxPreferencialEnable
          className={classes.box}
          style={{ background: color, height: sizeEscenario() }}
        />
      );
    }
    return (
      <BoxPreferencial
        className={classes.box}
        style={{ background: color, height: sizeEscenario() }}
      />
    );
  }

  if (color === "#607d8b") {
    if (!enableColor) {
      return (
        <BoxVipEnable
          className={classes.box}
          style={{ background: color, height: sizeEscenario() }}
        />
      );
    }
    return (
      <BoxVip
        className={classes.box}
        style={{ background: color, height: sizeEscenario() }}
      />
    );
  }

  if (color === "#FBDF23") {
    if (!enableColor) {
      return (
        <BoxPriorityEnable
          className={classes.box}
          style={{ background: color, height: sizeEscenario() }}
        />
      );
    }
    return (
      <BoxPriority
        className={classes.box}
        style={{ background: color, height: sizeEscenario() }}
      />
    );
  }

  if (color === "#f5f5f5") {
    if (!enableColor) {
      return (
        <BoxPriorityPlusEnable
          className={classes.box}
          style={{ background: color, height: sizeEscenario() }}
        />
      );
    }
    return (
      <BoxPriorityPlus
        className={classes.box}
        style={{ background: color, height: sizeEscenario() }}
      />
    );
  }


  return (
    <Box
      className={classes.box}
      style={{
        background: enableColor ? "#000000" : color,
        height: sizeEscenario()
      }}
    />
  );
}
