import React from "react";
import {
  IconButton,
  CssBaseline,
  Grid,
  Typography,
  AppBar,
  Toolbar,
  Stepper,
  Step,
  StepLabel,
  Paper,
  Button,
  makeStyles,
  Hidden
} from "@material-ui/core";

import useWindowSize from "../../WindowSize";
import Add from "@material-ui/icons/AddRounded";
import Remove from "@material-ui/icons/RemoveRounded";
import Spinner from "../../General/Spinner";
import Fatal from "../../General/Fatal";
import Box from "./Box";
import { ReactComponent as SvgIcon } from "../../../icons/ticket.svg";
import { useSelector, useDispatch } from "react-redux";
import * as ticketsActions from "../../../actions/tickets.Actions";
import { useHistory } from "react-router-dom";
import { store } from "react-notifications-component";
import RN from "../../Notification";
import * as sessionActions from "../../../actions/session.Actions";

function Copyright({ classes }) {
  return (
    <Typography
      variant="body2"
      color="textSecondary"
      align="center"
      className={classes.copy}
    >
      {"Copyright © "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  general: {
    backgroundColor: "#006f9b",
    [theme.breakpoints.up("sm")]: {
      width: 150
    },
    [theme.breakpoints.down("xs")]: {
      width: 150
    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  preferencial: {
    backgroundColor: "#bf0811",
    [theme.breakpoints.up("sm")]: {
      width: 150
    },
    [theme.breakpoints.down("xs")]: {
      width: 150
    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  vip: {
    backgroundColor: "#607d8b",
    [theme.breakpoints.up("sm")]: {
      width: 150
    },
    [theme.breakpoints.down("xs")]: {
      width: 150
    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  priority: {
    backgroundColor: "#FBDF23",
    [theme.breakpoints.up("sm")]: {
      width: 150
    },
    [theme.breakpoints.down("xs")]: {
      width: 150
    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  priorityPlus: {
    backgroundColor: "#f5f5f5",
    [theme.breakpoints.up("sm")]: {
      width: 150
    },
    [theme.breakpoints.down("xs")]: {
      width: 150
    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  icon: {
    color: "#FFFFFF"
  },
  appBar: {
    position: "relative"
  },
  formControl: {
    width: "100%"
  },
  layout: {
    //width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      //width: 600,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3)
    }
  },
  stepper: {
    padding: theme.spacing(3, 0, 5)
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  buttonsDown: {
    display: "flex",
    justifyContent: "center",
    marginBottom: theme.spacing(2)
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  },
  marginQr: {
    margin: theme.spacing(3)
  },
  copy: {
    margin: theme.spacing(3)
  },
  layoutContent: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontFamily: "Gotham-Medium",
    fontSize: 15
  },
  number: {
    fontFamily: "Gotham-Medium"
  },
  numberBlack: {
    fontFamily: "Gotham-Medium",
    color: "#212121"
  },
  info: {
    fontFamily: "Gotham-Book"
  },
  price: {
    fontFamily: "Gotham-Book",
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    textDecoration: "underline"
  },
  ajl: {
    fontFamily: "GothamNarrow-Black",
    flexGrow: 1
  }
}));

const steps = [
  "Selecciona tus boletos",
  "Datos del cliente",
  "Reserva",
  "Revise su compra"
];

export default function Tickets() {
  const classes = useStyles();
  const session = useSelector(store => store.sessionReducer);
  const usersSelector = useSelector(store => store.usersReducer);
  const ticketsSelector = useSelector(store => store.ticketsReducer);
  const dispatch = useDispatch();

  const history = useHistory();

  const size = useWindowSize();

  React.useEffect(() => {
    const { tickets } = ticketsSelector;
    if (Object.keys(tickets).length !== 0) {
      setValues({
        ...values,
        priority: tickets.priority,
        vip: tickets.vip,
        preferencial: tickets.preferencial,
        general: tickets.general,
        priorityPlus: tickets.priority
      });
    }
  }, []);

  const [values, setValues] = React.useState({
    activeStep: 0,
    general: 0,
    preferencial: 0,
    vip: 0,
    priority: 0,
    priorityPlus: 0
  });
  const handleNext = () => {
    dispatch(ticketsActions.enviarTickets(values));
    dispatch(ticketsActions.getPrices());
    dispatch(ticketsActions.getDescuentos());
    if (
      values.general === 0 &&
      values.vip === 0 &&
      values.preferencial === 0 &&
      values.priority === 0 &&
      values.priorityPlus === 0
    ) {
      store.addNotification({
        content: (
          <RN
            title="Error"
            message="Selecciona tu o tus boletos según la zona de tu prefencia para continuar."
            type="Error"
          />
        ),
        insert: "top",
        container: "top-right",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 1000,
          onScreen: false
        }
      });
    }
    dispatch(ticketsActions.vaciarSeats());
    history.push("/login/client");
  };
  const sizeEscenario = () => {
    if (size.height > size.width) {
      return "auto";
    }
    return "80%";
  };
  const activeStepClass = () => {
    return sizeEscenario();
  };
  const cargarEscenario = () => {
    const { user, loading, error } = usersSelector;
    if (loading) {
      return <Spinner />;
    }
    if (error) {
      return <Fatal mensaje={error} />;
    }
    const sessionInit = localStorage.getItem("login");
    if (!sessionInit) {
      dispatch(sessionActions.logOut());
      history.push("/login");
    } else {
      return (
        <Grid container justify="center">
          <Grid item xs={12} sm={10} container justify="center">
            <Grid item xs={6}>
              <Box
                enableColor={values.general === 0}
                color="#ffffff"
                largo={10}
                text="ESCENARIO"
              />
            </Grid>
            {/**ESCENARIO */}
            <Grid item xs={12} sm={8} container justify="center">
              <Grid item xs={2} container justify="center">
                <Grid item xs={5}>
                  <Box
                    enableColor={values.general === 0}
                    color="#006f9b"
                    largo={100}
                  />
                </Grid>
                <Grid item xs={7}>
                  <Box
                    enableColor={values.preferencial === 0}
                    color="#bf0811"
                    largo={100}
                  />
                </Grid>
              </Grid>

              <Grid item xs={8} container>
                <Grid item xs={12} container>
                  <Grid item xs={2}>
                    <Box
                      enableColor={values.vip === 0}
                      color="#607d8b"
                      largo={70}
                    />
                  </Grid>

                  <Grid item xs={8} container justify="center">
                    <Grid item xs={12} container justify="center">
                      <Grid item xs={3}>
                        <Box
                          enableColor={values.priority === 0}
                          color="#FBDF23"
                          largo={50}
                        />
                      </Grid>
                      <Grid item xs={1}>
                        <Box
                          enableColor={values.priorityPlus === 0}
                          color="#f5f5f5"
                          largo={50}
                        />
                      </Grid>
                      <Grid item xs={4}>
                        <Box
                          enableColor={values.general === 0}
                          color="#ffffff"
                          largo={50}
                          text="PASARELA"
                        />
                      </Grid>
                      <Grid item xs={1}>
                        <Box
                          enableColor={values.priorityPlus === 0}
                          color="#f5f5f5"
                          largo={50}
                        />
                      </Grid>
                      <Grid item xs={3}>
                        <Box
                          enableColor={values.priority === 0}
                          color="#FBDF23"
                          largo={50}
                        />
                      </Grid>
                    </Grid>
                    <Grid item xs={12}>
                      <Box
                        enableColor={values.priorityPlus === 0}
                        color="#f5f5f5"
                        largo={6}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Box
                        enableColor={values.priority === 0}
                        color="#FBDF23"
                        largo={15}
                      />
                    </Grid>
                  </Grid>

                  <Grid item xs={2}>
                    <Box
                      enableColor={values.vip === 0}
                      color="#607d8b"
                      largo={70}
                    />
                  </Grid>
                </Grid>

                <Grid item xs={12} container justify="center">
                  <Grid item xs={6}>
                    <Box
                      enableColor={values.vip === 0}
                      color="#607d8b"
                      largo={25}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Box
                      enableColor={values.vip === 0}
                      color="#607d8b"
                      largo={25}
                    />
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={2} container justify="center">
                <Grid item xs={7}>
                  <Box
                    enableColor={values.preferencial === 0}
                    color="#bf0811"
                    largo={100}
                  />
                </Grid>
                <Grid item xs={5}>
                  <Box
                    enableColor={values.general === 0}
                    color="#006f9b"
                    largo={100}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Typography variant="h6" gutterBottom>
              Selecciona tus boletos
            </Typography>
            {/** BOLETOS */}

            <Grid container spacing={3} justify="center">
              <Grid item xs={12} sm={3}>
                <Grid item container justify="center" alignItems="center">
                  <Grid item xs={12}>
                    <Typography align="center" className={classes.text}>
                      PRIORITY
                    </Typography>
                  </Grid>
                  <Grid item>
                    <IconButton
                      size="small"
                      className={classes.icon}
                      aria-label="Eliminar 1 boleto"
                      onClick={() =>
                        setValues({
                          ...values,
                          priority: values.priority - 1
                        })
                      }
                      disabled={values.priority === 0}
                    >
                      <Remove />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <Paper elevation={3} className={classes.priority}>
                      <Grid item xs={12}>
                        <Typography
                          align="center"
                          variant="body2"
                          className={classes.numberBlack}
                        >
                          {values.priority}
                        </Typography>
                      </Grid>
                    </Paper>
                  </Grid>
                  <Grid item>
                    <IconButton
                      size="small"
                      className={classes.icon}
                      aria-label="Añadir 1 boleto"
                      onClick={() =>
                        setValues({
                          ...values,
                          priority: values.priority + 1
                        })
                      }
                    >
                      <Add />
                    </IconButton>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography
                      align="center"
                      variant="body1"
                      className={classes.price}
                    >
                      $2,799 MXN
                    </Typography>
                    <Typography align="center" variant="body2">
                      Boleto Zona 1, acceso prioritario, barra libre, acceso al
                      evento de Networking con empresarios.
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Grid item container justify="center" alignItems="center">
                  <Grid item xs={12}>
                    <Typography align="center" className={classes.text}>
                      VIP
                    </Typography>
                  </Grid>
                  <Grid item>
                    <IconButton
                      size="small"
                      className={classes.icon}
                      aria-label="Eliminar 1 boleto"
                      onClick={() =>
                        setValues({ ...values, vip: values.vip - 1 })
                      }
                      disabled={values.vip === 0}
                    >
                      <Remove />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <Paper elevation={3} className={classes.vip}>
                      <Grid item xs={12}>
                        <Typography
                          align="center"
                          variant="body2"
                          className={classes.number}
                        >
                          {values.vip}
                        </Typography>
                      </Grid>
                    </Paper>
                  </Grid>
                  <Grid item>
                    <IconButton
                      size="small"
                      className={classes.icon}
                      aria-label="Añadir 1 boleto"
                      onClick={() =>
                        setValues({ ...values, vip: values.vip + 1 })
                      }
                    >
                      <Add />
                    </IconButton>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography
                      align="center"
                      variant="body1"
                      className={classes.price}
                    >
                      $1,749 MXN
                    </Typography>
                    <Typography align="center" variant="body2">
                      Boleto Zona 2, barra libre, meseros, acceso al evento de
                      Networking con empresarios.
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Grid item container justify="center" alignItems="center">
                  <Grid item xs={12}>
                    <Typography align="center" className={classes.text}>
                      PREFERENCIAL
                    </Typography>
                  </Grid>
                  <Grid item>
                    <IconButton
                      size="small"
                      className={classes.icon}
                      aria-label="Eliminar 1 boleto"
                      onClick={() =>
                        setValues({
                          ...values,
                          preferencial: values.preferencial - 1
                        })
                      }
                      disabled={values.preferencial === 0}
                    >
                      <Remove />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <Paper elevation={3} className={classes.preferencial}>
                      <Grid>
                        <Typography
                          align="center"
                          variant="body2"
                          className={classes.number}
                        >
                          {values.preferencial}
                        </Typography>
                      </Grid>
                    </Paper>
                  </Grid>
                  <Grid item>
                    <IconButton
                      size="small"
                      className={classes.icon}
                      aria-label="Añadir 1 boleto"
                      onClick={() =>
                        setValues({
                          ...values,
                          preferencial: values.preferencial + 1
                        })
                      }
                    >
                      <Add />
                    </IconButton>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography
                      align="center"
                      variant="body1"
                      className={classes.price}
                    >
                      $1,199 MXN
                    </Typography>
                    <Typography align="center" variant="body2">
                      Boleto Zona 3.
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12} sm={3}>
                <Grid
                  item
                  xs={12}
                  container
                  justify="center"
                  alignItems="center"
                >
                  <Grid item xs={12}>
                    <Typography align="center" className={classes.text}>
                      GENERAL
                    </Typography>
                  </Grid>
                  <Grid item>
                    <IconButton
                      size="small"
                      className={classes.icon}
                      aria-label="Eliminar 1 boleto"
                      onClick={() =>
                        setValues({
                          ...values,
                          general: values.general - 1
                        })
                      }
                      disabled={values.general === 0}
                    >
                      <Remove />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <Paper elevation={3} className={classes.general}>
                      <Typography
                        align="center"
                        variant="body2"
                        className={classes.number}
                      >
                        {values.general}
                      </Typography>
                    </Paper>
                  </Grid>
                  <Grid item>
                    <IconButton
                      size="small"
                      className={classes.icon}
                      aria-label="Añadir 1 boleto"
                      onClick={() =>
                        setValues({
                          ...values,
                          general: values.general + 1
                        })
                      }
                    >
                      <Add />
                    </IconButton>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography
                      align="center"
                      variant="body1"
                      className={classes.price}
                    >
                      $849 MXN
                    </Typography>
                    <Typography align="center" variant="body2">
                      Boleto Zona 4.
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Grid item container justify="center" alignItems="center">
                  <Grid item xs={12}>
                    <Typography align="center" className={classes.text}>
                      PRIORITY PLUS
                    </Typography>
                  </Grid>
                  <Grid item>
                    <IconButton
                      size="small"
                      className={classes.icon}
                      aria-label="Eliminar 1 boleto"
                      onClick={() =>
                        setValues({
                          ...values,
                          priorityPlus: values.priorityPlus - 1
                        })
                      }
                      disabled={values.priorityPlus === 0}
                    >
                      <Remove />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <Paper elevation={3} className={classes.priorityPlus}>
                      <Grid item xs={12}>
                        <Typography
                          align="center"
                          variant="body2"
                          className={classes.numberBlack}
                        >
                          {values.priorityPlus}
                        </Typography>
                      </Grid>
                    </Paper>
                  </Grid>
                  <Grid item>
                    <IconButton
                      size="small"
                      className={classes.icon}
                      aria-label="Añadir 1 boleto"
                      onClick={() =>
                        setValues({
                          ...values,
                          priorityPlus: values.priorityPlus + 1
                        })
                      }
                    >
                      <Add />
                    </IconButton>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography
                      align="center"
                      variant="body1"
                      className={classes.price}
                    >
                      $3,500 MXN
                    </Typography>
                    <Typography align="center" variant="body2">
                      Boleto Zona 1, acceso prioritario, barra libre, acceso al
                      evento de Networking con empresarios.
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      );
    }
  };
  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="absolute" color="default" className={classes.appBar}>
        <Toolbar>
          <SvgIcon style={{ fill: "#d50000", height: 48 }} />
          <Typography
            variant="h6"
            color="inherit"
            noWrap
            className={classes.ajl}
            align="justify"
          >
            AJL Boletos - Carlos Muñoz en Morelia
          </Typography>
          <Button
            color="inherit"
            onClick={() => {
              history.push("/login");
              dispatch(sessionActions.logOut());
            }}
          >
            Cerrar sesión
          </Button>
        </Toolbar>
      </AppBar>
      <main className={classes.layout} style={{ maxWidth: activeStepClass() }}>
        <Grid
          container
          align="center"
          justify="center"
          direction="column"
          className={classes.layoutContent}
        >
          <Paper className={classes.paper}>
            <Typography component="h1" variant="h4" align="center"></Typography>
            <Stepper
              activeStep={values.activeStep}
              className={classes.stepper}
              alternativeLabel
            >
              {steps.map(label => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
            <React.Fragment>
              <div className={classes.buttons}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleNext}
                  className={classes.button}
                >
                  SIGUIENTE
                </Button>
              </div>

              {cargarEscenario()}
            </React.Fragment>
          </Paper>
        </Grid>
        <Copyright classes={classes} />
      </main>
    </React.Fragment>
  );
}
