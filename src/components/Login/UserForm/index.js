import React from "react";
import {
  CssBaseline,
  Grid,
  Typography,
  TextField,
  AppBar,
  Toolbar,
  Stepper,
  Step,
  StepLabel,
  Paper,
  Button,
  makeStyles,
  Hidden
} from "@material-ui/core";
import useWindowSize from "../../WindowSize";
import Spinner from "../../General/Spinner";
import Fatal from "../../General/Fatal";
import { ReactComponent as SvgIcon } from "../../../icons/ticket.svg";
import { useSelector, useDispatch } from "react-redux";
import * as usersActions from "../../../actions/users.Actions";
import * as sessionActions from "../../../actions/session.Actions";
import { useHistory } from "react-router-dom";
import { store } from "react-notifications-component";
import RN from "../../Notification";

function Copyright({ classes }) {
  return (
    <Typography
      variant="body2"
      color="textSecondary"
      align="center"
      className={classes.copy}
    >
      {"Copyright © "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  appBar: {
    position: "relative"
  },
  icon: {
    height: 48
  },
  formControl: {
    width: "100%"
  },
  layout: {
    //width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      //width: 600,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3)
    }
  },
  stepper: {
    padding: theme.spacing(3, 0, 5)
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  buttonsDown: {
    display: "flex",
    justifyContent: "center",
    marginBottom: theme.spacing(2)
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  },
  marginQr: {
    margin: theme.spacing(3)
  },
  copy: {
    margin: theme.spacing(3)
  },
  layoutContent: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  ajl: {
    fontFamily: "GothamNarrow-Black",
    flexGrow: 1,
  },
  event: {
    fontFamily: "Gotham-Medium"
  },
}));

const steps = [
  "Selecciona tus boletos",
  "Datos del cliente",
  "Reserva",
  "Revise su compra"
];


export default function UserForm() {
  const classes = useStyles();
  const userSelector = useSelector(store => store.usersReducer);
  const dispatch = useDispatch();
  const history = useHistory();
  const size = useWindowSize();

  React.useEffect(() => {
    const { user } = userSelector;
    //console.log(user);
    if (Object.keys(user).length !== 0) {
      setState({
        ...state,
        name: user.name,
        lastName: user.lastName,
        email: user.email,
        phone: user.phone
      });
    }
  }, []);
  const [state, setState] = React.useState({
    activeStep: 1,
    name: "",
    lastName: "",
    email: "",
    phone: "",
    validatedEmail: false,
    id: ""
  });
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const handleChange = prop => event => {
    setState({ ...state, [prop]: event.target.value });
  };
  const handleNext = () => {
    const { user, loading, error } = userSelector;
    if (loading) {
      return <Spinner />;
    }
    if (error) {
      return <Fatal mensaje={error} />;
    }
    if (user.length !== 0) {
      if (
        state.name === "" ||
        state.lastName === "" ||
        state.email === "" ||
        state.phone === ""
      ) {
        store.addNotification({
          content: (
            <RN
              title="Campos requeridos vacíos"
              message="Por favor ingresa tus datos para continuar."
              type="Error"
            />
          ),
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 1000,
            onScreen: false
          }
        });
      } else if (!state.email.match(re)) {
        store.addNotification({
          content: (
            <RN
              title="Email inválido"
              message="Recuerda que una vez finalizado tu compra se te enviarán tus entradas al evento vía correo electrónico."
              type="Error"
            />
          ),
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 3000,
            onScreen: false
          }
        });
      } else {
        dispatch(usersActions.enviar(state));
        history.push("/login/booking");
      }
    }
  };
  const sizeEscenario = () => {
    if (size.height > size.width) {
      return "auto";
    }
    return "75%";
  };

  const activeStepClass = () => {
    if (state.activeStep !== 1) {
      return 600;
    } else {
      return sizeEscenario();
    }
  };
  const handleBack = () => {
    history.push("/login/tickets");
  };
  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="absolute" color="default" className={classes.appBar}>
        <Toolbar>
          <SvgIcon style={{ fill: "#d50000", height: 48 }} />
          <Typography variant="h6" color="inherit" noWrap className={classes.ajl}
            align="justify">
            AJL Boletos - Carlos Muñoz en Morelia
          </Typography>
          <Button color="inherit" onClick={() => {
            history.push("/login")
            dispatch(sessionActions.logOut());
          }} >Cerrar sesión</Button>
        </Toolbar>
      </AppBar>
      <Grid
        container
        alignItems="center"
        justify="space-evenly"
        direction="row"
      >
        <Grid item>
          <Hidden smDown>
            <img className={classes.image} src="/BN 1.png" alt="CARLOS MUÑOZ" />
          </Hidden>
        </Grid>
        <Grid item>
          <main
            className={classes.layout}
            style={{ maxWidth: activeStepClass() }}
          >
            <Grid
              container
              align="center"
              justify="center"
              direction="row"
              className={classes.layoutContent}
            >
              <Paper className={classes.paper}>
                <Typography
                  component="h1"
                  variant="h4"
                  align="center"
                ></Typography>
                <Stepper
                  activeStep={state.activeStep}
                  className={classes.stepper}
                  alternativeLabel
                >
                  {steps.map(label => (
                    <Step key={label}>
                      <StepLabel>{label}</StepLabel>
                    </Step>
                  ))}
                </Stepper>
                <React.Fragment>
                  <Typography variant="h6" gutterBottom>
                    Datos del cliente
                  </Typography>
                  <Grid container spacing={3} justify="center">
                    <Grid item xs={12} sm={12}>
                      <TextField
                        required
                        id="name"
                        name="name"
                        label="Nombre (s)"
                        value={state.name}
                        onChange={handleChange("name")}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <TextField
                        required
                        id="lastName"
                        name="lastName"
                        label="Apellidos"
                        value={state.lastName}
                        onChange={handleChange("lastName")}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <TextField
                        required
                        id="email"
                        name="email"
                        label="Correo Electrónico"
                        value={state.email}
                        onChange={handleChange("email")}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <TextField
                        required
                        id="phone"
                        name="phone"
                        label="Telefono Móvil"
                        value={state.phone}
                        onChange={handleChange("phone")}
                        fullWidth
                      />
                    </Grid>
                  </Grid>
                  
                  <Hidden xsDown>
                    <div className={classes.buttons}>
                      <Button onClick={handleBack} className={classes.button}>
                        REGRESAR
                  </Button>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={handleNext}
                        className={classes.button}
                      >
                        SIGUIENTE
                  </Button>
                    </div>
                  </Hidden>
                  <Hidden smUp>
                    <div className={classes.buttonsDown}>
                      <Button onClick={handleBack} className={classes.button}>
                        REGRESAR
                  </Button>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={handleNext}
                        className={classes.button}
                      >
                        SIGUIENTE
                  </Button>
                    </div>
                  </Hidden>
                </React.Fragment>
              </Paper>
              <Copyright classes={classes} />
            </Grid>
          </main>
        </Grid>

        <Grid item>
          <Hidden smDown>
            <img className={classes.image} src="/BN 2.png" alt="CARLOS MUÑOZ" />
          </Hidden>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
