import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { Grid } from "@material-ui/core";
import red from "@material-ui/core/colors/red";
import { ReactComponent as SvgIcon } from "../../../icons/ticket.svg";
import { useHistory } from 'react-router-dom'

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"AJL Boletos, Copyright © "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  layout: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  titulo: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    [theme.breakpoints.down("xs")]: {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2)
    }
  },
  link: {
    textDecoration: "none",
    color: red[700]
  },
  image: {
    width: "35vh"
    //marginTop: theme.spacing(2)
  },
}));

export default function Home() {
  const classes = useStyles();
  const history = useHistory();

  React.useEffect(() => {
    setTimeout(function () {
      history.push("/login/tickets")
    }, 10000); //will call the function after 2 secs.
  }, []);

  return (
    <React.Fragment>
      <CssBaseline />
      <Grid
        container
        align="center"
        justify="center"
        className={classes.layout}
      >
        <Grid item xs={12}>
          <SvgIcon style={{ fill: "#d50000", height: 144 }} />
          <Typography variant="h5">
            AJL Boletos
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h3" className={classes.titulo}>
            Su transacción fue procesada exitosamente,{" "}
            <Link to="/login/tickets" className={classes.link}>
              volver a inicio
          </Link>
            .
          <Typography
              variant="subtitle2"
              gutterBottom
              className={classes.titulo}
            >
              Hemos enviado su confirmación de compra y su entrada (código QR) por
              correo electrónico.
          </Typography>
            
          </Typography>
          <img
            className={classes.image}
            src="/image.png"
            alt="CARLOS MUÑOZ"
          />
        </Grid>
        
        <Copyright />
      </Grid>
    </React.Fragment>
  );
}
