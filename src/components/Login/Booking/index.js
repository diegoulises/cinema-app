import React from "react";
import {
  CssBaseline,
  Grid,
  Typography,
  AppBar,
  Toolbar,
  Stepper,
  Step,
  StepLabel,
  Paper,
  Button,
  makeStyles,
  ListItemText,
  List,
  ListItem,
  Hidden,
} from "@material-ui/core";

import useWindowSize from "../../WindowSize";
import { ReactComponent as SvgIcon } from "../../../icons/ticket.svg";
import Spinner from "../../General/Spinner";
import Fatal from "../../General/Fatal";
import Box from "./Box";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { store } from "react-notifications-component";
import RN from "../../Notification";
import * as sessionActions from "../../../actions/session.Actions";

function Copyright({ classes }) {
  return (
    <Typography
      variant="body2"
      color="textSecondary"
      align="center"
      className={classes.copy}
    >
      {"Copyright © "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  general: {
    backgroundColor: "#006f9b",
    [theme.breakpoints.up("sm")]: {
      width: 150,

    },
    [theme.breakpoints.down("xs")]: {
      width: 150,

    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1),
  },
  preferencial: {
    backgroundColor: "#bf0811",
    [theme.breakpoints.up("sm")]: {
      width: 150,

    },
    [theme.breakpoints.down("xs")]: {
      width: 150,

    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  vip: {
    backgroundColor: "#607d8b",
    [theme.breakpoints.up("sm")]: {
      width: 150,

    },
    [theme.breakpoints.down("xs")]: {
      width: 150,

    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  priority: {
    backgroundColor: "#FBDF23",
    [theme.breakpoints.up("sm")]: {
      width: 150,

    },
    [theme.breakpoints.down("xs")]: {
      width: 150,

    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  priorityPlus: {
    backgroundColor: "#f5f5f5",
    [theme.breakpoints.up("sm")]: {
      width: 150
    },
    [theme.breakpoints.down("xs")]: {
      width: 150
    },
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(1)
  },
  icon: {
    color: "#FFFFFF"
  },
  text: {
    fontFamily: "Gotham-Medium",
    fontSize: 15,
    paddingTop: theme.spacing(1)
  },
  appBar: {
    position: "relative"
  },
  formControl: {
    width: "100%"
  },
  layout: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      //width: 600,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3)
    },
  },
  stepper: {
    padding: theme.spacing(3, 0, 5)
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  buttonsDown: {
    display: "flex",
    justifyContent: "center",
    marginBottom: theme.spacing(2)
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  },
  marginQr: {
    margin: theme.spacing(3)
  },
  copy: {
    margin: theme.spacing(3)
  },
  layoutContent: {
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  numberBlack: {
    fontFamily: "Gotham-Medium",
    color: "#212121"
  },
  info: {
    fontFamily: "Gotham-Book",
  },
  price: {
    fontFamily: "Gotham-Book",
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    textDecoration: "underline"
  },
  number: {
    fontFamily: "Gotham-Medium",
  },
  ajl: {
    fontFamily: "GothamNarrow-Black",
    flexGrow: 1,
  },
}));

const steps = [
  "Selecciona tus boletos",
  "Datos del cliente",
  "Reserva",
  "Revise su compra"
];

export default function Tickets() {
  const classes = useStyles();
  const history = useHistory();
  const size = useWindowSize();
  const ticketsSelector = useSelector(store => store.ticketsReducer);
  const sessionInit = localStorage.getItem("login")
  const dispatch = useDispatch();
  const cargarZonas = () => {
    const { tickets, loading, error } = ticketsSelector;
    if (loading) {
      return <Spinner />;
    }
    if (error) {
      return <Fatal mensaje={error} />;
    }
    
    if ((tickets.general === 0 && tickets.vip === 0 && tickets.preferencial === 0 && tickets.priority === 0 && tickets.priorityPlus === 0) || Object.keys(tickets).length === 0 || !sessionInit) {
      history.push("/login/tickets")
    } else {
      return (
        <Grid container justify="center">
          <Grid item xs={12} sm={10} container justify="center">
            <Grid item xs={6} className={classes.boxWhite}>
              <Box
                color="#ffffff"
                largo={10}
                text="ESCENARIO"
              />
            </Grid>
            {/**ESCENARIO */}
            <Grid item container justify="center">
              <Grid item xs={2} container justify="center">
                <Grid item xs={5}>
                  <Box
                    enableColor={tickets.general === 0}
                    amount={tickets.general}
                    color="#006f9b"
                    largo={100}
                  />
                </Grid>
                <Grid item xs={7}>
                  <Box
                    enableColor={tickets.preferencial === 0}
                    amount={tickets.preferencial}
                    color="#bf0811"
                    largo={100}
                  />
                </Grid>
              </Grid>

              <Grid item xs={8} container>
                <Grid item xs={12} container>
                  <Grid item xs={2}>
                    <Box
                      enableColor={tickets.vip === 0}
                      amount={tickets.vip}
                      color="#607d8b"
                      largo={70}
                    />
                  </Grid>

                  <Grid item xs={8} container justify="center">
                    <Grid item xs={12} container justify="center">
                      <Grid item xs={3}>
                        <Box
                          enableColor={tickets.priority === 0}
                          amount={tickets.priority}
                          color="#FBDF23"
                          largo={50}
                        />
                      </Grid>
                      <Grid item xs={1}>
                        <Box
                          enableColor={tickets.priorityPlus === 0}
                          amount={tickets.priorityPlus}
                          color="#f5f5f5"
                          largo={50}
                        />
                      </Grid>
                      <Grid item xs={4}>
                        <Box
                          enableColor={tickets.general === 0}
                          amount={tickets.general}
                          color="#ffffff"
                          largo={50}
                          text="PASARELA"
                        />
                      </Grid>
                      <Grid item xs={1}>
                        <Box
                          enableColor={tickets.priorityPlus === 0}
                          amount={tickets.priorityPlus}
                          color="#f5f5f5"
                          largo={50}
                        />
                      </Grid>
                      <Grid item xs={3}>
                        <Box
                          enableColor={tickets.priority === 0}
                          amount={tickets.priority}
                          color="#FBDF23"
                          largo={50}
                        />
                      </Grid>
                    </Grid>
                    <Grid item xs={12}>
                      <Box
                        enableColor={tickets.priorityPlus === 0}
                        amount={tickets.priorityPlus}
                        color="#f5f5f5"
                        largo={6}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Box
                        enableColor={tickets.priority === 0}
                        amount={tickets.priority}
                        color="#FBDF23"
                        largo={15}
                      />
                    </Grid>
                  </Grid>

                  <Grid item xs={2}>
                    <Box
                      enableColor={tickets.vip === 0}
                      amount={tickets.vip}
                      color="#607d8b"
                      largo={70}
                    />
                  </Grid>
                </Grid>

                <Grid item xs={12} container justify="center">
                  <Grid item xs={6}>
                    <Box
                      enableColor={tickets.vip === 0}
                      amount={tickets.vip}
                      color="#607d8b"
                      largo={25}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Box
                      enableColor={tickets.vip === 0}
                      amount={tickets.vip}
                      color="#607d8b"
                      largo={25}
                    />
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={2} container justify="center">
                <Grid item xs={7}>
                  <Box
                    enableColor={tickets.preferencial === 0}
                    amount={tickets.preferencial}
                    color="#bf0811"
                    largo={100}
                  />
                </Grid>
                <Grid item xs={5}>
                  <Box
                    enableColor={tickets.general === 0}
                    amount={tickets.general}
                    color="#006f9b"
                    largo={100}
                  />
                </Grid>
              </Grid>
            </Grid></Grid>

        </Grid>
      );
    }
  };
  const cargarZonas2 = () => {
    const {
      tickets,
      loading,
      error,
      seats,
      loading_seats,
      error_seats
    } = ticketsSelector;
    if (loading) {
      return <Spinner />;
    }
    if (error) {
      return <Fatal mensaje={error} />;
    }
    if ((tickets.general === 0 && tickets.vip === 0 && tickets.preferencial === 0 && tickets.priority === 0 && tickets.priorityPlus === 0) || Object.keys(tickets).length === 0 || !sessionInit) {
      history.push("/login/tickets")
    } else {
      if (loading_seats) {
        return <Spinner />;
      }
      if (error_seats) {
        return <Fatal mensaje={error} />;
      }
      let ticketsData = [];
      let priceGeneral = 0;
      let priceVip = 0;
      let pricePriority = 0;
      let pricePreferencial = 0;
      let pricePriorityPlus = 0;

      if (tickets.priority > 0) {
        pricePriority = tickets.priority * 2799;
        let seatsPriority = "";
        
        for (let k in seats) {
          if (seats[k].idType === 4) {
            if (seats[k].isPlus) {
              
            } else {
              seatsPriority += seats[k].seatNumber + " ";

            }
          }
        }
        ticketsData.push({
          amount: tickets.priority,
          name: "Priority",
          desc:
            "Boleto Zona 1, acceso prioritario, barra libre, acceso al evento de Networking con empresarios.",
          price: `$${pricePriority} MXN`,
          seats: seatsPriority
        });
      }
      if (tickets.vip > 0) {
        priceVip = tickets.vip * 1749;
        let seatsVip = "";
        for (let k in seats) {
          if (seats[k].idType === 3) {
            seatsVip += seats[k].seatNumber + " ";
          }
        }
        ticketsData.push({
          amount: tickets.vip,
          name: "VIP",
          desc:
            "Boleto Zona 2, barra libre, meseros, acceso al evento de Networking con empresarios.",
          price: `$${priceVip} MXN`,
          seats: seatsVip
        });
      }
      if (tickets.preferencial > 0) {
        pricePreferencial = tickets.preferencial * 1199;
        let seatsPreferencial = "";
        for (let k in seats) {
          if (seats[k].idType === 2) {
            seatsPreferencial += seats[k].seatNumber + " ";
          }
        }
        ticketsData.push({
          amount: tickets.preferencial,
          name: "Preferencial",
          desc: "Boleto Zona 3",
          price: `$${pricePreferencial} MXN`,
          seats: seatsPreferencial
        });
      }
      if (tickets.general > 0) {
        priceGeneral = tickets.general * 849;
        let seatsGeneral = "";
        for (let k in seats) {
          if (seats[k].idType === 1) {
            seatsGeneral += seats[k].seatNumber + " ";
          }
        }
        ticketsData.push({
          amount: tickets.general,
          name: "General",
          desc: "Boleto Zona 4",
          price: `$${priceGeneral} MXN`,
          seats: seatsGeneral
        });
      }
      if (tickets.priorityPlus > 0) {
        pricePriorityPlus = tickets.priorityPlus * 3500;
        let seatsPriorityPlus = "";
        for (let k in seats) {
          if (seats[k].idType === 4) {
            //console.log(seats[k]);
            if (seats[k].isPlus) {
              seatsPriorityPlus += seats[k].seatNumber + " ";
            }
            
          }
        }
        ticketsData.push({
          amount: tickets.priorityPlus,
          name: "Priority Plus",
          desc:
            "Boleto Zona 1, acceso prioritario, barra libre, acceso al evento de Networking con empresarios.",
          price: `$${pricePriorityPlus} MXN`,
          seats: seatsPriorityPlus
        });
      }

      return (
        <Grid container justify="center">
          <Grid item container justify="center" xs={12} spacing={3}>
            {/**INFORMACIÓN DE BOLETOS SELECCIONADOS */}
            {ticketsData.map(product => (
              <Grid item xs={12} sm={3} key={product.name}>
                <Grid item container justify="center" alignItems="center">
                  <Grid item xs={12}>
                    <Typography align="center" className={classes.text}>
                      {`${product.name} (x${product.amount})`}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Paper elevation={3} className={classessTicket(product.name)}>
                      <Grid item xs={12}>
                        <Typography align="center" variant="body2" className={product.name === "Priority" ? classes.numberBlack : product.name === "Priority Plus" ? classes.numberBlack : classes.number}>
                          Asientos
                        </Typography>
                      </Grid>
                    </Paper>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography align="center" variant="body1" className={seats.length === 0 ? classes.text : classes.price}>
                      {seatText(product.seats)}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            ))}
          </Grid>
        </Grid>
      );
    }
  };
  const classessTicket = (ticket) => {
    if (ticket==="Priority") {
      return classes.priority
    }
    if (ticket === "General") {
      return classes.general
    }
    if (ticket === "Preferencial") {
      return classes.preferencial
    }
    if (ticket === "VIP") {
      return classes.vip
    }
    if (ticket === "Priority Plus") {
      return classes.priorityPlus
    }
  }
  const seatText = (seats) => {
    if (seats.length===0){
      return "Da click sobre el mapa para elegir tus asientos"
    }
    return seats
  }
  const handleNext = () => {
    const {
      tickets,
      loading,
      error,
      seats,
      loading_seats,
      error_seats
    } = ticketsSelector;
    if (loading || loading_seats) {
      return <Spinner />;
    }
    if (error) {
      return <Fatal mensaje={error} />;
    } 
    if (error_seats) {
      return <Fatal mensaje={error_seats} />;
    }
    var sum = tickets.general + tickets.vip + tickets.preferencial + tickets.priority + tickets.priorityPlus;
    //console.log(sum);
    //console.log(seats.length)
    
    if (sum === seats.length) {
      history.push("/login/review");
    }else{
      store.addNotification({
        content: (
          <RN
            title="Elige todos tus asientos"
            message="Termina de elegir tus asientos para continuar."
            type="Warning"
          />
        ),
        insert: "top",
        container: "top-right",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 1000,
          onScreen: false
        }
      });
    }
    
  };
  const handleBack = () => {
    history.push("/login/client");
  };
  const sizeEscenario = () => {
    if (size.height > size.width) {
      return "auto";
    }
    return "80%";
  };
  const activeStepClass = () => {
    return sizeEscenario();
  };
  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="absolute" color="default" className={classes.appBar}>
        <Toolbar>
          <SvgIcon style={{ fill: "#d50000", height: 48 }} />
          <Typography variant="h6" color="inherit" noWrap className={classes.ajl}
            align="justify">
            AJL Boletos - Carlos Muñoz en Morelia
          </Typography>
          <Button color="inherit" onClick={() => {
            history.push("/login")
            dispatch(sessionActions.logOut());
          }} >Cerrar sesión</Button>
        </Toolbar>
      </AppBar>
      <main className={classes.layout}>
        <Grid
          container
          align="center"
          justify="center"
          direction="column"
          className={classes.layoutContent}
        >
          <Paper className={classes.paper} style={{ width: activeStepClass()}}>
            <Typography component="h1" variant="h4" align="center"></Typography>
            <Stepper activeStep={2} className={classes.stepper} alternativeLabel>
              {steps.map(label => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
            <React.Fragment>
              <Hidden xsDown>
                <div className={classes.buttons}>
                  <Button onClick={handleBack} className={classes.button}>
                    REGRESAR
                </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    SIGUIENTE
                </Button>
                </div>
              </Hidden>
              <Hidden smUp>
                <div className={classes.buttonsDown}>
                  <Button onClick={handleBack} className={classes.button}>
                    REGRESAR
                </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    SIGUIENTE
                </Button>
                </div>
              </Hidden>

              {cargarZonas()}
              {cargarZonas2()}
              
            </React.Fragment>
          </Paper>
        </Grid>
        <Copyright classes={classes} />
      </main>
    </React.Fragment>
  );
}
