import React, { useRef } from "react";
import {
  CssBaseline,
  Grid,
  Typography,
  AppBar,
  Toolbar,
  Stepper,
  Step,
  StepLabel,
  Paper,
  Button,
  makeStyles,
  ListItemText,
  List,
  ListItem,
  TextField,
  Checkbox,
  Hidden,
  FormControlLabel,
  Select,
  InputLabel,
  FormControl
} from "@material-ui/core";
import useWindowSize from "../../WindowSize";
import { Image } from "react";
import Spinner from "../../General/Spinner";
import Fatal from "../../General/Fatal";
import { ReactComponent as SvgIcon } from "../../../icons/ticket.svg";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import * as ticketsActions from "../../../actions/tickets.Actions";
import * as sessionActions from "../../../actions/session.Actions";
import axios from "axios";
import { CountdownCircleTimer } from "react-countdown-circle-timer";
import CashPayment from "../CashPayment";
import { PRODUCTION } from "../../../constants/StringsAPI";

var seleccionadosGeneral = [];
var seleccionadosPreferente = [];
var seleccionadosPriority = [];
var seleccionadosVip = [];
var seleccionadosPriorityPlus = [];

function Copyright({ classes }) {
  return (
    <Typography
      variant="body2"
      color="textSecondary"
      align="center"
      className={classes.copy}
    >
      {"Copyright © "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const renderTime = value => {
  if (value === 0) {
    return (
      <div style={{ fontSize: 12, color: "white", fontFamily: "Gotham-Book" }}>
        Asientos liberados
      </div>
    );
  }
  if (value % 60 < 10) {
    return (
      <div className="timer">
        <div
          style={{ fontSize: 22, color: "white", fontFamily: "Gotham-Ultra" }}
        >
          {Math.trunc(value / 60)}:0{value % 60}
        </div>
        <div
          style={{ fontSize: 10, color: "white", fontFamily: "Gotham-Book" }}
        >
          tiempo
          <br />
          restante
        </div>
      </div>
    );
  }
  return (
    <div className="timer">
      <div style={{ fontSize: 22, color: "white", fontFamily: "Gotham-Ultra" }}>
        {Math.trunc(value / 60)}:{value % 60}
      </div>
      <div style={{ fontSize: 10, color: "white", fontFamily: "Gotham-Book" }}>
        tiempo
        <br />
        restante
      </div>
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  icon: {
    color: "#FFFFFF"
  },
  text: {
    fontSize: 15
  },
  appBar: {
    position: "relative"
  },
  formControl: {
    width: "100%"
  },
  layout: {
    //width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      //width: 600,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3)
    }
  },
  stepper: {
    padding: theme.spacing(3, 0, 5)
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  },
  buttonOpenPay: {
    backgroundColor: "white",
    width: 200,
    borderRadius: 100,
    color: "black"
  },
  marginQr: {
    margin: theme.spacing(3)
  },
  copy: {
    margin: theme.spacing(3)
  },
  layoutContent: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  listItem: {
    padding: theme.spacing(1, 0)
  },
  total: {
    fontWeight: "700"
  },
  title: {
    marginTop: theme.spacing(2)
  },
  add: {
    fontFamily: "Gotham-Book"
  },
  dataReview: {
    fontFamily: "Gotham-Book",
    textDecoration: "underline"
  },
  dataReview2: {
    fontFamily: "Gotham-Book"
  },
  ajl: {
    fontFamily: "GothamNarrow-Black",
    flexGrow: 1
  }
}));

const steps = [
  "Selecciona tus boletos",
  "Datos del cliente",
  "Reserva",
  "Revise su compra"
];

const addresses = [
  "Negrito Poeta #84",
  "col. Santa María de Guido",
  "58090",
  "Morelia",
  "México"
];
const checks = [
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false
];
const init = [
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  ""
];
export default function Review() {
  const classes = useStyles();
  const history = useHistory();
  const size = useWindowSize();
  const ticketsSelector = useSelector(store => store.ticketsReducer);
  const userSelector = useSelector(store => store.usersReducer);
  const dispatch = useDispatch();
  const sessionInit = localStorage.getItem("login");

  const [state, setState] = React.useState({
    checked: [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false
    ],
    name: [
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      ""
    ],
    email: [
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      ""
    ],
    phone: [
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      ""
    ],
    amountSeats: 0,
    invoice: false,
    descuento: 0
  });
  var openPayWindow = null;
  
  const freeUp = seats => {
    //Agregar elementos al array de seleccionados para el pago con paypal
    for (var k in seats) {
      if (seats[k].idType === 1) {
        seleccionadosGeneral.push(seats[k].seatNumber);
      }
      if (seats[k].idType === 2) {
        seleccionadosPreferente.push(seats[k].seatNumber);
      }
      if (seats[k].idType === 3) {
        seleccionadosVip.push(seats[k].seatNumber);
      }
      if (seats[k].idType === 4) {
        seleccionadosPriority.push(seats[k].seatNumber);
      }
    }
    for (var key in seleccionadosGeneral) {
      window.onbeforeunload = e => {
        alert("Adios");
      };
      var formData = new FormData();
      formData.append("seatNumber", seleccionadosGeneral[key]);
      formData.append("idType", 1);
      formData.append("free", true);
      axios({
        method: "post",
        url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
        data: formData,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(function(response) {
          if (response.data.success) {
            //console.log(`asiento ${seleccionadosGeneral[key]} liberado de general`)
          } else {
            //console.log(`asiento ${seleccionadosGeneral[key]} no se pudo liberar de general`)
          }
        })
        .catch(function(response) {
          //console.log('error')
        });
    }
    for (var key in seleccionadosPreferente) {
      window.onbeforeunload = e => {
        alert("Adios");
      };
      var formData = new FormData();
      formData.append("seatNumber", seleccionadosPreferente[key]);
      formData.append("idType", 2);
      formData.append("free", true);
      axios({
        method: "post",
        url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
        data: formData,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(function(response) {
          if (response.data.success) {
            //console.log(`asiento ${seleccionadosPreferente[key]} liberado de preferente`)
          } else {
            //console.log(`asiento ${seleccionadosPreferente[key]} no se pudo liberar de preferente`)
          }
        })
        .catch(function(response) {
          //console.log('error')
        });
    }
    for (var key in seleccionadosVip) {
      window.onbeforeunload = e => {
        alert("Adios");
      };
      var formData = new FormData();
      formData.append("seatNumber", seleccionadosVip[key]);
      formData.append("idType", 3);
      formData.append("free", true);
      axios({
        method: "post",
        url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
        data: formData,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(function(response) {
          if (response.data.success) {
            //console.log(`asiento ${seleccionadosVip[key]} liberado de vip`)
          } else {
            //console.log(`asiento ${seleccionadosVip[key]} no se pudo liberar de vip`)
          }
        })
        .catch(function(response) {
          //console.log('error')
        });
    }
    for (var key in seleccionadosPriority) {
      window.onbeforeunload = e => {
        alert("Adios");
      };
      var formData = new FormData();
      formData.append("seatNumber", seleccionadosPriority[key]);
      formData.append("idType", 4);
      formData.append("free", true);
      axios({
        method: "post",
        url: `${PRODUCTION}/api/asientos/reservarTemp.php`,
        data: formData,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(function(response) {
          if (response.data.success) {
            //console.log(`asiento ${seleccionadosPriority[key]} liberado de priority`)
          } else {
            //console.log(`asiento ${seleccionadosPriority[key]} no se pudo liberar de priority`)
          }
        })
        .catch(function(response) {
          //console.log('error')
        });
    }
  };
  const handleChangeName = index => event => {
    const { name } = state;
    const newState = name.slice(0); // Create a shallow copy of the name
    newState[index] = event.target.value; // Set the new value
    setState({ ...state, name: newState });
  };
  const handleChangeEmail = index => event => {
    
    const { email } = state;
    const newState = email.slice(0); // Create a shallow copy of the name
    newState[index] = event.target.value; // Set the new value
    setState({ ...state, email: newState });
  };
  const handleChangePhone = index => event => {
    const { phone } = state;
    const newState = phone.slice(0); // Create a shallow copy of the name
    newState[index] = event.target.value; // Set the new value
    setState({ ...state, phone: newState });
  };
  const handleChangeC = index => event => {
    const { user } = userSelector;
    const { checked } = state;
    const newState = checked.slice(0); // Create a shallow copy of the name
    newState.map((item, j) => {
      if (j === index) {
        newState[j] = event.target.checked; // Set the new value
        if (event.target.checked) {
          let helloName = Object.assign({}, state);
          helloName.name[j] = user.name;
          let helloEmail = Object.assign({}, state);
          helloEmail.email[j] = user.email;
          let helloPhone = Object.assign({}, state);
          helloPhone.phone[j] = user.phone;

          setState({ ...state, name: helloName });
          setState({ ...state, email: helloEmail });
          setState({ ...state, phone: helloPhone });
        }
      } else {
        newState[j] = !event.target.checked;
        let helloName = Object.assign({}, state);
        helloName.name[j] = "";
        let helloEmail = Object.assign({}, state);
        helloEmail.email[j] = "";
        let helloPhone = Object.assign({}, state);
        helloPhone.phone[j] = "";

        setState({ ...state, name: helloName });
        setState({ ...state, email: helloEmail });
        setState({ ...state, phone: helloPhone });
      }
    });
    setState({ ...state, checked: newState });
  };

  const handleBack = () => {
    dispatch(ticketsActions.vaciarSeats());
    const {
      tickets,
      loading,
      error,
      seats,
      loading_seats,
      error_seats
    } = ticketsSelector;
    freeUp(seats);
    history.push("/login/booking");
  };
  const handleOpenPay = amount => {
    //window.open('http://www.facebook.com/sharer.php?s=100&p[title]=Fb Share&p[summary]=Facebook share popup&p[url]=javascript:fbShare("http://jsfiddle.net/stichoza/EYxTJ/")&p[images][0]="http://goo.gl/dS52U"', 'sharer', 'toolbar=0,status=0,width=548,height=325');
    const { user } = userSelector;
    const name = user.name;
    const last_name = user.last_name;
    const phone_number = user.phone;
    const email = user.email;
    var formData = new FormData();
    formData.append("name", name);
    formData.append("last_name", last_name);
    formData.append("phone_number", phone_number);
    formData.append("email", email);
    formData.append("amount", amount);
    axios({
      method: "post",
      url: `${PRODUCTION}/api/openpay/createCharge.php`,
      data: formData,
      config: { headers: { "Content-Type": "multipart/form-data" } }
    })
      .then(function(response) {
        if (response.data.success) {
          const url = response.data.redirect_url;
          var win = window.open(
            url,
            "sharer",
            "toolbar=0,status=0,width=800,height=600"
          );
          openPayWindow = win;
        } else {
          alert("error");
        }
      })
      .catch(function(response) {});
  };
  const sizeEscenario = () => {
    if (size.height > size.width) {
      return "auto";
    }
    return 800;
  };

  const activeStepClass = () => {
    return sizeEscenario();
  };

  const getInfoUser = () => {
    const { user, loading, error } = userSelector;
    if (loading) {
      return <Spinner />;
    }
    if (error) {
      return <Fatal mensaje={error} />;
    }
    return (
      <Grid item xs={12}>
        <Typography variant="caption" gutterBottom>
          Hola!, verifica que el email{" "}
          <Typography
            variant="caption"
            display="inline"
            className={classes.dataReview}
          >
            {user.email}
          </Typography>{" "}
          de tu cliente sea correcto, recuerda que las entradas serán enviadas a ésta dirección.
        </Typography>
      </Grid>
    );
  };

  const handleCancel = seats => {
    freeUp(seats);
    if (openPayWindow != null) {
      openPayWindow.close();
    }
    history.push("/login");
    alert(
      "Se acabó tu tiempo. Hemos cancelado tu compra y liberado tus asientos"
    );
  };

  const handleChange = name => event => {
    setState({ ...state, [name]: event.target.checked });
  };

  const handleChangeDescuento = name => event => {
    setState({
      ...state,
      [name]: event.target.value,
    });

  };

  const getSummary = () => {
    const { user } = userSelector;
    const {
      tickets,
      loading,
      error,
      seats,
      loading_seats,
      error_seats,
      prices,
      loading_prices,
      error_prices,
      descuentos,
      loading_descuentos,
      error_descuentos
    } = ticketsSelector;

    //Agregar elementos al array de seleccionados para el pago con paypal
    for (let k in seats) {
      if (seats[k].idType === 1) {
        seleccionadosGeneral.push(seats[k].seatNumber);
      }
      if (seats[k].idType === 2) {
        seleccionadosPreferente.push(seats[k].seatNumber);
      }
      if (seats[k].idType === 3) {
        seleccionadosVip.push(seats[k].seatNumber);
      }
      if (seats[k].idType === 4) {
        if (seats[k].isPlus) {
          seleccionadosPriorityPlus.push(seats[k].seatNumber);
        }else{
          seleccionadosPriority.push(seats[k].seatNumber);
        }
        
      }
    }

    if (loading || loading_seats || loading_prices || loading_descuentos) {
      return <Spinner />;
    }
    if (error || error_seats || error_prices) {
      return <Fatal mensaje={error} />;
    }
    if (error_seats) {
      return <Fatal mensaje={error_seats} />;
    }
    if (error_prices) {
      return <Fatal mensaje={error_prices} />;
    }
    if (
      (tickets.general === 0 &&
        tickets.vip === 0 &&
        tickets.preferencial === 0 &&
        tickets.priority === 0 &&
        tickets.priorityPlus === 0) ||
      Object.keys(tickets).length === 0
    ) {
      history.push("/login/tickets");
    }

    var totalTickets =  tickets.general + tickets.vip + tickets.preferencial + tickets.priority + tickets.priorityPlus;
    
    let p = prices.prices;
    var descuentosSelect = []
    if(descuentos.success){
      descuentosSelect = descuentos.discounts;
    }

    var priceGeneralAPI;
    var priceVipAPI;
    var pricePriorityAPI;
    var pricePreferencialAPI;
    var pricePriorityPlusAPI;



for (let i in p) {
  if (p[i].name === "General") {
    priceGeneralAPI = p[i].price;
  }
  if (p[i].name === "VIP") {
    priceVipAPI = p[i].price;
  }
  if (p[i].name === "Priority") {
    pricePriorityAPI = p[i].price;
  }
  if (p[i].name === "Preferencial") {
    pricePreferencialAPI = p[i].price;
  }
  if (p[i].name === "Priority Plus") {
    pricePriorityPlusAPI = p[i].price;
  }
}
let ticketsData = [];
let priceGeneral = 0;
let priceVip = 0;
let pricePriority = 0;
let pricePreferencial = 0;
let pricePriorityPlus = 0;
let sum = 0;
if (tickets.priority > 0) {
  pricePriority = tickets.priority * pricePriorityAPI;
  let seatsPriority = "";
  for (let k in seats) {
    if (seats[k].idType === 4) {
      if (seats[k].isPlus) {
        
      }else{
        seatsPriority += seats[k].seatNumber + " ";
      }
      
    }
  }
  ticketsData.push({
    amount: tickets.priority,
    name: "Priority",
    desc:
      "Boleto Zona 1, acceso prioritario, barra libre, acceso al evento de Networking con empresarios.",
    price: `$${pricePriority} MXN`,
    seats: seatsPriority,
  });
}
if (tickets.vip > 0) {
  priceVip = tickets.vip * priceVipAPI;
  let seatsVip = "";
  for (let k in seats) {
    if (seats[k].idType === 3) {
      seatsVip += seats[k].seatNumber + " ";
    }
  }
  ticketsData.push({
    amount: tickets.vip,
    name: "VIP",
    desc:
      "Boleto Zona 2, barra libre, meseros, acceso al evento de Networking con empresarios.",
    price: `$${priceVip} MXN`,
    seats: seatsVip,
  });
}
if (tickets.preferencial > 0) {
  pricePreferencial = tickets.preferencial * pricePreferencialAPI;
  let seatsPreferencial = "";
  for (let k in seats) {
    if (seats[k].idType === 2) {
      seatsPreferencial += seats[k].seatNumber + " ";
    }
  }
  ticketsData.push({
    amount: tickets.preferencial,
    name: "Preferencial",
    desc: "Boleto Zona 3",
    price: `$${pricePreferencial} MXN`,
    seats: seatsPreferencial,
  });
}
if (tickets.general > 0) {
  priceGeneral = tickets.general * priceGeneralAPI;
  let seatsGeneral = "";
  for (let k in seats) {
    if (seats[k].idType === 1) {
      seatsGeneral += seats[k].seatNumber + " ";
    }
  }
  ticketsData.push({
    amount: tickets.general,
    name: "General",
    desc: "Boleto Zona 4",
    price: `$${priceGeneral} MXN`,
    seats: seatsGeneral,
  });
}
if (tickets.priorityPlus > 0) {
  pricePriorityPlus = tickets.priorityPlus * pricePriorityPlusAPI;
  let seatsPriorityPlus = "";
  for (let k in seats) {
    if (seats[k].idType === 4) {
      if(seats[k].isPlus){
        seatsPriorityPlus += seats[k].seatNumber + " ";
      }
      
    }
  }
  ticketsData.push({
    amount: tickets.priorityPlus,
    name: "Priority Plus",
    desc:
      "Boleto Zona 1, acceso prioritario, barra libre, acceso al evento de Networking con empresarios.",
    price: `$${pricePriorityPlus} MXN`,
    seats: seatsPriorityPlus,
  });
}
    sum = priceGeneral + pricePreferencial + pricePriority + priceVip + pricePriorityPlus;
    var iva = (sum * 0.16).toFixed(2);
    const cargo = (sum-(sum*state.descuento)).toFixed(2);
    const cargoMasIva = ((sum * 0.16 + sum) - ((sum * 0.16 + sum) * state.descuento)).toFixed(2);

    return (
      <div>
        <ListItem className={classes.listItem}>
          <FormControlLabel
            control={
              <Checkbox
                checked={state.invoice}
                onChange={handleChange("invoice")}
                value="invoice"
                color="primary"
              />
            }
            label="Requiere factura"
          />
        </ListItem>
        <List disablePadding>
          {ticketsData.map((product, indexProduct) => (
            <div key={indexProduct}>
              <ListItem className={classes.listItem} key={product.name}>
                <ListItemText
                  primary={
                    <Typography
                      variant="body1"
                      style={{ fontFamily: "Gotham-Book" }}
                    >{`${product.name} (x${product.amount})`}</Typography>
                  }
                  secondary={
                    <Typography
                      variant="body2"
                      style={{ fontFamily: "Gotham-Book" }}
                    >
                      {product.desc}
                    </Typography>
                  }
                />
                <Typography
                  variant="body2"
                  align="right"
                  style={{ fontFamily: "Gotham-Book" }}
                >
                  {product.price}
                </Typography>
              </ListItem>
              <Grid container>
                {seats.map((seat, index) => boletos(seat, product.name, index))}
              </Grid>
            </div>
          ))}

          <ListItem className={classes.listItem}>
            <ListItemText primary="Subtotal" />
            <Typography variant="subtitle1" className={classes.total}>
              ${sum.toFixed(2)}
            </Typography>
          </ListItem>
          <ListItem className={classes.listItem}>
            <ListItemText primary="IVA" />
            <Typography variant="subtitle1" className={classes.total}>
              ${state.invoice ? iva : 0}
            </Typography>
          </ListItem>
          <ListItem className={classes.listItem}>
            <ListItemText primary="Total" />
            <Typography variant="subtitle1" className={classes.total}>
              ${state.invoice ? cargoMasIva : cargo}
            </Typography>
          </ListItem>
          <ListItem className={classes.listItem}>
            <FormControl variant="filled" style={{ width: "100%" }}>
              <InputLabel htmlFor="filled-descuento-native-simple">Aplicar descuento</InputLabel>
              <Select
                native
                value={state.descuento}
                onChange={handleChangeDescuento('descuento')}
                inputProps={{
                  name: 'descuento',
                  id: 'filled-descuento-native-simple',
                }}
              >
                <option value="" />
                {descuentosSelect.map((discount, index) => (
                  <option key={index} value={discount*100/100}>{discount*100}% de descuento</option>
                ))}
              </Select>
            </FormControl>
          </ListItem>
        </List>
        <Grid container spacing={2} alignContent="center">
          <Grid item xs={12}>
            <Typography
              style={{
                width: "100%",
                marginTop: 0,
                marginBottom: 0,
                fontSize: 12,
                fontFamily: "Gotham-Ultra"
              }}
            >
              NOTA:
            </Typography>
            <Typography
              style={{
                width: "100%",
                marginTop: 10,
                marginBottom: 20,
                fontSize: 12
              }}
            >
              Tu compra será cancelada y tus asientos serán liberados al
              acabarse el tiempo
            </Typography>
          </Grid>
          <CountdownCircleTimer
            isPlaying
            size={100}
            renderTime={renderTime}
            strokeWidth={5}
            trailColor="#212121"
            ariaLabel={"2"}
            durationSeconds={600}
            onComplete={() => handleCancel(seats)}
            colors={[["#388e3c", 0.33], ["#FBDF23", 0.33], ["#880000"]]}
          />
          <Grid item xs={12}>
            <Typography variant="h6" gutterBottom className={classes.title}>
              Dirección del evento
            </Typography>
            <Typography variant="body2">Morelia, Michoacán</Typography>
            <Typography gutterBottom variant="body2">
              {addresses.join(", ")}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            {/*
            <PaypalBtn state={state} seats={seats} amount={sum} user={user} asientosGeneral={seleccionadosGeneral} asientosPreferencial={seleccionadosPreferente} asientosVip={seleccionadosVip} asientosPriority={seleccionadosPriority} />
              */}
            <CashPayment
              user={user}
              state={state}
              seats={seats}
              amount={state.invoice ? cargoMasIva : cargo}
              asientosGeneral={seleccionadosGeneral}
              asientosPreferencial={seleccionadosPreferente}
              asientosVip={seleccionadosVip}
              asientosPriority={seleccionadosPriority}
              text="CONFIRMACIÓN DE PAGO"
            />
          </Grid>
          <Grid item xs={12}>
            {/*
            <Button onClick={()=>handleOpenPay(sum)} className={classes.buttonOpenPay}>
              Pagar con tarjeta
            </Button>
            */}
          </Grid>
        </Grid>
      </div>
    );
  };

  const boletos = (seat, name, index) => {
    //GENERAL
    if (seat.idType === 1) {
      if (name === "General") {
        return (
          <Grid
            container
            item
            spacing={2}
            alignItems="center"
            justify="center"
            key={index}
          >
            <Grid item xs={1}>
              <Checkbox
                checked={state.checked[index]}
                onChange={handleChangeC(index)}
                value="secondary"
                color="primary"
                inputProps={{
                  "aria-label": "secondary checkbox"
                }}
              />
            </Grid>
            <Grid item xs={2}>
              <Typography>{seat.seatNumber}</Typography>
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="name"
                label="Nombre"
                value={state.name[index]}
                key={index}
                onChange={handleChangeName(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="email"
                label="Email"
                value={state.email[index]}
                key={index}
                onChange={handleChangeEmail(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="phone"
                label="Telefono"
                value={state.phone[index]}
                key={index}
                onChange={handleChangePhone(index)}
                fullWidth
              />
            </Grid>
          </Grid>
        );
      }
    }
    //Preferencial
    if (seat.idType === 2) {
      if (name === "Preferencial") {
        return (
          <Grid
            container
            item
            spacing={2}
            alignItems="center"
            justify="center"
            key={index}
          >
            <Grid item xs={1}>
              <Checkbox
                checked={state.checked[index]}
                onChange={handleChangeC(index)}
                value="secondary"
                color="primary"
                inputProps={{
                  "aria-label": "secondary checkbox"
                }}
              />
            </Grid>
            <Grid item xs={2}>
              <Typography>{seat.seatNumber}</Typography>
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="name"
                label="Nombre"
                value={state.name[index]}
                key={index}
                onChange={handleChangeName(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="email"
                label="Email"
                value={state.email[index]}
                key={index}
                onChange={handleChangeEmail(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="phone"
                label="Telefono"
                value={state.phone[index]}
                key={index}
                onChange={handleChangePhone(index)}
                fullWidth
              />
            </Grid>
          </Grid>
        );
      }
    }
    //Vip
    if (seat.idType === 3) {
      if (name === "VIP") {
        return (
          <Grid
            container
            item
            spacing={2}
            alignItems="center"
            justify="center"
            key={index}
          >
            <Grid item xs={1}>
              <Checkbox
                checked={state.checked[index]}
                onChange={handleChangeC(index)}
                value="secondary"
                color="primary"
                inputProps={{
                  "aria-label": "secondary checkbox"
                }}
              />
            </Grid>
            <Grid item xs={2}>
              <Typography>{seat.seatNumber}</Typography>
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="name"
                label="Nombre"
                value={state.name[index]}
                key={index}
                onChange={handleChangeName(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="email"
                label="Email"
                value={state.email[index]}
                key={index}
                onChange={handleChangeEmail(index)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                required
                name="phone"
                label="Telefono"
                value={state.phone[index]}
                key={index}
                onChange={handleChangePhone(index)}
                fullWidth
              />
            </Grid>
          </Grid>
        );
      }
    }
    //Priority
    if (seat.idType === 4) {
      if (name === "Priority") {
        if (seat.isPlus) {
          
        }else{
          return (
            <Grid
              container
              item
              spacing={2}
              alignItems="center"
              justify="center"
              key={index}
            >
              <Grid item xs={1}>
                <Checkbox
                  checked={state.checked[index]}
                  onChange={handleChangeC(index)}
                  value="secondary"
                  color="primary"
                  inputProps={{
                    "aria-label": "secondary checkbox"
                  }}
                />
              </Grid>
              <Grid item xs={2}>
                <Typography>{seat.seatNumber}</Typography>
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  name="name"
                  label="Nombre"
                  value={state.name[index]}
                  key={index}
                  onChange={handleChangeName(index)}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  name="email"
                  label="Email"
                  value={state.email[index]}
                  key={index}
                  onChange={handleChangeEmail(index)}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  name="phone"
                  label="Telefono"
                  value={state.phone[index]}
                  key={index}
                  onChange={handleChangePhone(index)}
                  fullWidth
                />
              </Grid>
            </Grid>
          );
        }
        
      }
      if (name === "Priority Plus") {
        if (seat.isPlus) {
          return (
            <Grid
              container
              item
              spacing={2}
              alignItems="center"
              justify="center"
              key={index}
            >
              <Grid item xs={1}>
                <Checkbox
                  checked={state.checked[index]}
                  onChange={handleChangeC(index)}
                  value="secondary"
                  color="primary"
                  inputProps={{
                    "aria-label": "secondary checkbox"
                  }}
                />
              </Grid>
              <Grid item xs={2}>
                <Typography>{seat.seatNumber}</Typography>
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  name="name"
                  label="Nombre"
                  value={state.name[index]}
                  key={index}
                  onChange={handleChangeName(index)}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  name="email"
                  label="Email"
                  value={state.email[index]}
                  key={index}
                  onChange={handleChangeEmail(index)}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  name="phone"
                  label="Telefono"
                  value={state.phone[index]}
                  key={index}
                  onChange={handleChangePhone(index)}
                  fullWidth
                />
              </Grid>
            </Grid>
          );
        }
        
      }
    }
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="absolute" color="default" className={classes.appBar}>
        <Toolbar>
          <SvgIcon style={{ fill: "#d50000", height: 48 }} />
          <Typography
            variant="h6"
            color="inherit"
            noWrap
            className={classes.ajl}
            align="justify"
          >
            AJL Boletos - Carlos Muñoz en Morelia
          </Typography>
          <Button
            color="inherit"
            onClick={() => {
              history.push("/login");
              dispatch(sessionActions.logOut());
            }}
          >
            Cerrar sesión
          </Button>
        </Toolbar>
      </AppBar>
      <Grid
        container
        alignItems="center"
        justify="space-evenly"
        direction="row"
      >
        <Grid item>
          <Hidden mdDown>
            <img className={classes.image} src="/BN 1.png" alt="CARLOS MUÑOZ" />
          </Hidden>
        </Grid>
        <Grid item>
          <main
            className={classes.layout}
            style={{ maxWidth: activeStepClass() }}
          >
            <Grid
              container
              align="center"
              justify="center"
              direction="column"
              className={classes.layoutContent}
            >
              <Paper className={classes.paper}>
                <Typography
                  component="h1"
                  variant="h4"
                  align="center"
                ></Typography>
                <Stepper
                  activeStep={4}
                  className={classes.stepper}
                  alternativeLabel
                >
                  {steps.map(label => (
                    <Step key={label}>
                      <StepLabel>{label}</StepLabel>
                    </Step>
                  ))}
                </Stepper>
                <React.Fragment>
                  <Typography variant="h6" gutterBottom>
                    Resumen de compra
                  </Typography>
                  {getInfoUser()}
                  {getSummary()}

                  <div className={classes.buttons}>
                    <Button onClick={handleBack} className={classes.button}>
                      REGRESAR
                    </Button>
                  </div>
                </React.Fragment>
              </Paper>
            </Grid>
            <Copyright classes={classes} />
          </main>
        </Grid>

        <Grid item>
          <Hidden mdDown>
            <img className={classes.image} src="/BN 2.png" alt="CARLOS MUÑOZ" />
          </Hidden>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
