import React from "react";
import {
  CssBaseline,
  Grid,
  Typography,
  TextField,
  AppBar,
  Toolbar,
  Stepper,
  Step,
  StepLabel,
  Paper,
  Button,
  makeStyles,
  Hidden,
  FormControl,
  IconButton,
  Input,
  InputLabel,
  InputAdornment
} from "@material-ui/core";
import useWindowSize from "../WindowSize";
import Spinner from "../General/Spinner";
import Fatal from "../General/Fatal";
import { ReactComponent as SvgIcon } from "../../icons/ticket.svg";
import { useSelector, useDispatch } from "react-redux";
import * as sessionActions from "../../actions/session.Actions";
import { useHistory } from "react-router-dom";
import { store } from "react-notifications-component";
import RN from "../Notification";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

function Copyright({ classes }) {
  return (
    <Grid container>
      <Grid item xs={12}>
        <Typography
          variant="body2"
          color="textSecondary"
          align="center"
          className={classes.copy}
        >
          {"Copyright © "}
          {new Date().getFullYear()}
          {"."}
        </Typography>
      </Grid>
    </Grid>
  );
}

const useStyles = makeStyles(theme => ({
  appBar: {
    position: "relative"
  },
  icon: {
    height: 48
  },
  formControl: {
    width: "100%"
  },
  layout: {
    //width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      //width: 600,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3)
    }

  },
  stepper: {
    padding: theme.spacing(3, 0, 5)
  },
  buttons: {
    display: "flex",
    justifyContent: "center"
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  },
  marginQr: {
    margin: theme.spacing(3)
  },
  copy: {
    margin: theme.spacing(3)
  },
  layoutContent: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  ajl: {
    fontFamily: "GothamNarrow-Black"
  },
  event: {
    fontFamily: "Gotham-Medium"
  },
  layout: {
    width: "auto",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
}));


export default function Login() {
  const classes = useStyles();
  const session = useSelector(store => store.sessionReducer);
  const dispatch = useDispatch();
  const history = useHistory();
  const size = useWindowSize();
  React.useEffect(() => {
    const { login } = session;
    if (login.success) {
       history.push("/login/tickets");
    }
  });
  const [values, setValues] = React.useState({
    password: "",
    email: "",
    showPassword: false,
  });
  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value });
    dispatch(sessionActions.logOut());
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };
  const signIn = () => {
    const { error, cargando } = session;
    if (cargando) {
      return <Spinner />;
    }
    if (error) {
      return <Fatal mensaje={error} />;
    }
  };
  const sizeEscenario = () => {
    if (size.height > size.width) {
      return "auto";
    }
    return "75%";
  };
  const activeStepClass = () => {
    sizeEscenario();
  };
  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="absolute" color="default" className={classes.appBar}>
        <Toolbar>
          <SvgIcon style={{ fill: "#d50000", height: 48 }} />
          <Typography
            variant="h6"
            color="inherit"
            noWrap
            className={classes.ajl}
          >
            AJL Boletos
          </Typography>
        </Toolbar>
      </AppBar>
      <Grid
        container
        alignItems="center"
        justify="space-evenly"
        direction="row"
      >
        <Grid item>
          <Hidden smDown>
            <img className={classes.image} src="/BN 1.png" alt="CARLOS MUÑOZ" />
          </Hidden>
        </Grid>
        <Grid item>
          <main
            className={classes.layout}
            style={{ maxWidth: activeStepClass() }}
          >
            <Grid
              container
              align="center"
              justify="center"
              direction="row"
              className={classes.layoutContent}
            >
              <Paper className={classes.paper}>
                <Typography
                  component="h1"
                  variant="h4"
                  align="center"
                ></Typography>
                <Grid
                  container
                  align="center"
                  justify="center"
                  direction="column"
                  className={classes.layout}
                >
                  <Grid item>
                    <SvgIcon style={{ fill: "#d50000", height: 144 }} />
                    <Typography variant="h5" gutterBottom>
                      Bienvenido
                    </Typography>
                  </Grid>
                </Grid>

                <React.Fragment>
                  <Grid container spacing={3} justify="center">
                    <Grid item xs={12} sm={12}>
                      <TextField
                        id="email"
                        label="Usuario"
                        type="text"
                        value={values.email}
                        onChange={handleChange("email")}
                        InputLabelProps={{
                          className: classes.label
                        }}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <FormControl fullWidth>
                        <InputLabel>Contraseña</InputLabel>
                        <Input
                          id="standard-adornment-password"
                          type={values.showPassword ? "text" : "password"}
                          value={values.password}
                          onChange={handleChange("password")}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                              >
                                {values.showPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          }
                        />
                      </FormControl>
                    </Grid>
                    {signIn()}
                  </Grid>

                  <div className={classes.buttons}>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => {
                        dispatch(
                          sessionActions.login(values.email, values.password)
                        );
                      }}
                      className={classes.button}
                    >
                      INGRESAR
                    </Button>
                  </div>
                </React.Fragment>
              </Paper>
              <Copyright classes={classes} />
            </Grid>
          </main>
        </Grid>

        <Grid item>
          <Hidden smDown>
            <img className={classes.image} src="/BN 2.png" alt="CARLOS MUÑOZ" />
          </Hidden>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
