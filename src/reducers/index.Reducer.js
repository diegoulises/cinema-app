import { combineReducers } from "redux";

import usersReducer from "./users.Reducer";
import ticketsReducer from "./tickets.Reducer";
import sessionReducer from "./session.Reducer";

export default combineReducers({
    usersReducer,
    ticketsReducer,
    sessionReducer
});