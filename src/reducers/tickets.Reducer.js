import {
    TICKETS,
    LOADING,
    ERROR,
    SEATS,
    LOADING_SEATS,
    ERROR_SEATS,
    READ_SEATS,
    READ_LOADING_SEATS,
    READ_ERROR_SEATS,
    SEATS_VIP,
    PRICES,
    LOADING_PRICES,
    ERROR_PRICES,
    DESCUENTOS,
    LOADING_DESCUENTOS,
    ERROR_DESCUENTOS,
    SELECT_MOVIE
} from "../constants/ticketsTypes";

const INITIAL_STATE = {
    tickets: {},
    loading: false,
    error: "",
    seats: [],
    loading_seats: false,
    error_seats: "",
    read_seats: {},
    read_seats_loading: false,
    read_seats_error: "",
    seats_vip: {},
    prices: {},
    loading_prices: false,
    error_prices: "",
    descuentos: {},
    loading_descuentos: false,
    error_descuentos: "",
    select_movie: ""
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TICKETS:
            return {
                ...state,
                tickets: action.payload,
                loading: false,
                error: ""
            };
        case LOADING:
            return {
                ...state,
                loading: true
            };
        case ERROR:
            return {
                ...state,
                error: action.payload,
                loading: false
            };
        case SEATS:
            //console.log(action.payload)
            return {
                ...state,
                seats: action.payload,
                loading_seats: false,
                error_seats: ""
            }
        case LOADING_SEATS:
            return {
                ...state,
                loading_seats: true
            }
        case ERROR_SEATS:
            return {
                ...state,
                error_seats: action.payload,
                loading_seats: false
            }
        case READ_SEATS:
            return {
                ...state,
                read_seats: action.payload,
                read_loading_seats: false,
                read_error_seats: ""
            }
        case READ_LOADING_SEATS:
            return {
                ...state,
                read_loading_seats: true
            }
        case READ_ERROR_SEATS:
            return {
                ...state,
                read_error_seats: action.payload,
                read_loading_seats: false
            }
        case SEATS_VIP:
            return {
                ...state,
                seats_vip: action.payload,
            }
        case PRICES:
            return {
                ...state,
                prices: action.payload,
                loading_prices: false,
                error_prices: ""
            };
        case LOADING_PRICES:
            return {
                ...state,
                loading_prices: true
            };
        case ERROR_PRICES:
            return {
                ...state,
                error_prices: action.payload,
                loading_prices: false
            };
        case DESCUENTOS:
            return {
                ...state,
                descuentos: action.payload,
                loading_descuentos: false,
                error_descuentos: ""
            };
        case LOADING_DESCUENTOS:
            return {
                ...state,
                loading_descuentos: true
            };
        case ERROR_DESCUENTOS:
            return {
                ...state,
                error_descuentos: action.payload,
                loading_descuentos: false
            };
        case SELECT_MOVIE:
            return {
                ...state,
                select_movie: action.payload,
            };
        default:
            return state;
    }
};
