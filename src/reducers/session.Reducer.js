import {
    LOGIN,
    CARGANDO,
    ERROR,
} from "../constants/sessionTypes";

const INITIAL_STATE = {
    login: {},
    cargando: false,
    error: "",
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                login: action.payload,
                cargando: false,
                error: ""
            };
        case CARGANDO:
            return {
                ...state,
                cargando: true
            };
        case ERROR:
            return {
                ...state,
                error: action.payload,
                cargando: false
            };
        default:
            return state;
    }
};
